﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MinutesOfMeeting.Models.DAL;
using MinutesOfMeeting.Models.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static MinutesOfMeeting.Models.Helpers.Admin;


namespace MinutesOfMeeting.Models.BAL
{
    public class BusinessLayer
    {

        SqlDataProviderBiz ObjProviderModel = new SqlDataProviderBiz();
        OutlookEWS objOutlookEWS = new OutlookEWS();

        public IConfiguration Configuration { get; }
        private static String _Errorlog;
        public IWebHostEnvironment _env { get; set; }

        public BusinessLayer()
        {

        }
        public BusinessLayer(IWebHostEnvironment _env)
        {
            if (_Errorlog == null)
            {
                _Errorlog = GetErrorLogPath();
            }
            ObjProviderModel._Errorlog = _env;
            objOutlookEWS._Errorlog = _env;
        }
        private string GetErrorLogPath()
        {
            var builder = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json");

            var connectionStringConfig = builder.Build();
            if (connectionStringConfig != null)
                return connectionStringConfig.GetConnectionString("ErrorLog");
            else
                return string.Empty;

        }

        #region User Login
        public Users GetUserInfo_ByUserID(Users objUsers)
        {
            DataTable dt = new DataTable();
            Users objUser = null;
            dt = ObjProviderModel.GetUserInfo_ByUserID(objUsers);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                objUser = new Users
                {
                    Id = Convert.ToInt32(dt.Rows[i]["ID"]),
                    UserName = Helper.isCheckNullOrEmtyAndEncrypt(dt.Rows[i]["NTLogin"]),
                    DisplayName = Helper.isCheckNullOrEmtyAndEncrypt(dt.Rows[i]["DisplayNameEn"]),
                    FirstName = Helper.isCheckNullOrEmtyAndEncrypt(dt.Rows[i]["FirstName"]),
                    LastName = Helper.isCheckNullOrEmtyAndEncrypt(dt.Rows[i]["LastName"]),
                    Designation = Helper.isCheckNullOrEmtyAndEncrypt(dt.Rows[i]["DesignationEn"]),
                    EmailId = Helper.isCheckNullOrEmtyAndEncrypt(dt.Rows[i]["EmailId"]),
                    UserPRnumber = Helper.isCheckNullOrEmtyAndEncrypt(dt.Rows[i]["PrNumber"]),
                    Division = Helper.isCheckNullOrEmtyAndEncrypt(dt.Rows[i]["DivisionName"]),
                    IsTBT = Convert.ToBoolean(dt.Rows[i]["IsTBT"]),
                    Picture = string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["Picture"])) ? "assets/img/user-default.png" : Convert.ToString(dt.Rows[i]["Picture"]),
                    Role = Convert.ToInt32(dt.Rows[i]["UserRole"])
                };
            }
            return objUser;
        }


        public DataTable GetEmailContactInfo_ByUserID(Users objUsers)
        {
            DataTable dt = new DataTable();
            Users objUser = new Users();
            dt = ObjProviderModel.GetEmailContactInfo_ByUserID(objUsers);

            return dt;
        }

        #endregion

        #region MEETING TYPE MASTER

        public List<MeetingType> GetMeetingType(MeetingType obj)
        {
            List<MeetingType> List_MeetingType = new List<MeetingType>();
            try
            {

                DataTable dt = new DataTable();
                dt = ObjProviderModel.GetMeetingType(obj);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MeetingType objKPI = new MeetingType();
                    objKPI.MeetingTypeUno = Convert.ToInt32(dt.Rows[i]["MeetingTypeUno"]);
                    objKPI.MeetingTypecode = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingTypecode"]);
                    objKPI.MeetingName = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingName"]);
                    List_MeetingType.Insert(i, objKPI);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return List_MeetingType;
        }

        public string InsertUpdateMeetingType(MeetingType obj)
        {
            string result = ObjProviderModel.MeetingType_Insert_Update(obj);
            return result;
        }

        public string DeleteMeetingType(MeetingType obj)
        {
            string result = ObjProviderModel.MeetingType_Delete(obj);
            return result;
        }

        #endregion

        #region Location Master

        public List<MeetingLocation> GetLocation(MeetingLocation obj)
        {
            List<MeetingLocation> List_Location = new List<MeetingLocation>();
            try
            {

                DataTable dt = new DataTable();
                dt = ObjProviderModel.GetLocation(obj);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MeetingLocation objKPI = new MeetingLocation();
                    objKPI.LocationUno = Convert.ToInt32(dt.Rows[i]["locationUno"]);
                    objKPI.LocationCode = Helper.isCheckNullOrEmty(dt.Rows[i]["locationCode"]);
                    objKPI.LocationName = Helper.isCheckNullOrEmty(dt.Rows[i]["locationName"]);
                    List_Location.Insert(i, objKPI);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return List_Location;
        }

        public string InsertUpdateLocation(MeetingLocation obj)
        {

            string result = ObjProviderModel.Location_Insert_Update(obj);
            return result;
        }

        //KPI Delete 
        public string DeleteLocation(MeetingLocation obj)
        {
            string result = ObjProviderModel.Location_Delete(obj);
            return result;
        }

        #endregion

        #region Meeting Room Master

        public List<MeetingRoom> GetMeetingRoom(MeetingRoom obj)
        {
            List<MeetingRoom> List_MeetingRoom = new List<MeetingRoom>();
            try
            {

                DataTable dt = new DataTable();
                dt = ObjProviderModel.GetMeetingRoom(obj);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MeetingRoom objKPI = new MeetingRoom();
                    objKPI.MeetingRoomUno = Convert.ToInt32(dt.Rows[i]["MeetingRoomUno"]);
                    objKPI.MeetingRoomCode = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingRoomCode"]);
                    objKPI.MeetingRoomName = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingRoomName"]);
                    objKPI.locationName = Helper.isCheckNullOrEmty(dt.Rows[i]["locationName"]);
                    objKPI.locationUno = Convert.ToInt32(dt.Rows[i]["locationUno"]);
                    List_MeetingRoom.Insert(i, objKPI);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return List_MeetingRoom;
        }

        public string InsertUpdateMeetingRoom(MeetingRoom obj)
        {

            string result = ObjProviderModel.MeetingRoom_Insert_Update(obj);
            return result;
        }

        public string DeleteMeetingRoom(MeetingRoom obj)
        {
            string result = ObjProviderModel.MeetingRoom_Delete(obj);
            return result;
        }

        #endregion

        #region User Master
        public List<UserMaster> GetUserMasters(UserMaster obj)
        {
            List<UserMaster> List_UserMaster = new List<UserMaster>();
            try
            {

                DataTable dt = new DataTable();
                dt = ObjProviderModel.GetUserMasters(obj);
                if (obj.Condition == 5)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        UserMaster objUserMaster = new UserMaster();
                        objUserMaster.ID = Helper.isnumberCheckNullOrEmty(dt.Rows[i]["ID"]);
                        objUserMaster.NTUsername = Helper.isCheckNullOrEmty(dt.Rows[i]["UserName"]);
                        objUserMaster.DisplayNameEn = Helper.isCheckNullOrEmty(dt.Rows[i]["DisplayNameEn"]);
                        objUserMaster.DisplayNameAr = Helper.isCheckNullOrEmty(dt.Rows[i]["DisplayNameAr"]);
                        objUserMaster.EmailId = Helper.isCheckNullOrEmty(dt.Rows[i]["EmailId"]);
                        objUserMaster.Firstname = Helper.isCheckNullOrEmty(dt.Rows[i]["Firstname"]);
                        objUserMaster.Lastname = Helper.isCheckNullOrEmty(dt.Rows[i]["LastName"]);
                        objUserMaster.DesignationEn = Helper.isCheckNullOrEmty(dt.Rows[i]["DesignationEn"]);
                        objUserMaster.DesignationAr = Helper.isCheckNullOrEmty(dt.Rows[i]["DesignationAr"]);
                        objUserMaster.PrNumber = Helper.isCheckNullOrEmty(dt.Rows[i]["PrNumber"]);
                        //objUserMaster.DivisionUno = Helper.isnumberCheckNullOrEmty(dt.Rows[i]["DivisionUno"]);
                        objUserMaster.DivisionNameEn = Helper.isCheckNullOrEmty(dt.Rows[i]["DivisionName"]);

                        List_UserMaster.Insert(i, objUserMaster);
                    }
                }
                else
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        UserMaster objUserMaster = new UserMaster();
                        objUserMaster.ID = Helper.isnumberCheckNullOrEmty(dt.Rows[i]["ID"]);
                        objUserMaster.NTUsername = Helper.isCheckNullOrEmty(dt.Rows[i]["UserName"]);
                        objUserMaster.DisplayNameEn = Helper.isCheckNullOrEmty(dt.Rows[i]["DisplayNameEn"]);
                        objUserMaster.DisplayNameAr = Helper.isCheckNullOrEmty(dt.Rows[i]["DisplayNameAr"]);
                        objUserMaster.EmailId = Helper.isCheckNullOrEmty(dt.Rows[i]["EmailId"]);
                        objUserMaster.Firstname = Helper.isCheckNullOrEmty(dt.Rows[i]["Firstname"]);
                        objUserMaster.Lastname = Helper.isCheckNullOrEmty(dt.Rows[i]["LastName"]);
                        objUserMaster.DesignationEn = Helper.isCheckNullOrEmty(dt.Rows[i]["DesignationEn"]);
                        objUserMaster.DesignationAr = Helper.isCheckNullOrEmty(dt.Rows[i]["DesignationAr"]);
                        objUserMaster.PrNumber = Helper.isCheckNullOrEmty(dt.Rows[i]["PrNumber"]);
                        objUserMaster.DivisionUno = Helper.isnumberCheckNullOrEmty(dt.Rows[i]["DivisionUno"]);
                        objUserMaster.DivisionNameEn = Helper.isCheckNullOrEmty(dt.Rows[i]["DivisionNameEn"]);
                        objUserMaster.IsTBT = Convert.ToBoolean(dt.Rows[i]["IsTBT"]);
                        objUserMaster.Picture = string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["Picture"])) ? "assets/img/user-default.png" : Convert.ToString(dt.Rows[i]["Picture"]);
                        objUserMaster.Role = Convert.ToInt32(dt.Rows[i]["UserRole"]);
                        List_UserMaster.Insert(i, objUserMaster);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return List_UserMaster;
        }


        public List<AttendeesAtd> getUserList(UserMaster obj)
        {
            List<AttendeesAtd> List_selUser = new List<AttendeesAtd>();
            try
            {

                DataTable dt = new DataTable();
                dt = ObjProviderModel.GetUserMasters(obj);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AttendeesAtd objSLA = new AttendeesAtd();
                    objSLA.FullAttendeeName = Convert.ToString(dt.Rows[i]["userselName"]);
                    objSLA.NameAtd = Convert.ToString(dt.Rows[i]["userselName"]).Split("-")[1];
                    objSLA.UserselUno = Convert.ToInt32(dt.Rows[i]["userselUno"]);
                    objSLA.emailAtd = Convert.ToString(dt.Rows[i]["userselEmail"]);
                    objSLA.PrNumberAtd = Convert.ToString(dt.Rows[i]["userselCode"]);
                    List_selUser.Insert(i, objSLA);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return List_selUser;
        }
        public string InsertUpdateUserMaster(UserMaster obj)
        {
            string result = ObjProviderModel.InsertUpdateUserMaster(obj);
            return result;
        }



        public string DeleteUserMaster(UserMaster obj)
        {
            string result = ObjProviderModel.DeleteUserMaster(obj);
            return result;
        }
        #endregion

        #region Dashboard
        public MOMDashboard GetMOMDashboard(MOMDashboard obj)
        {
            MOMDashboard List_cmtObject = new MOMDashboard();
            List<MOMDashboard_MyMeetings> List_MOMDashboard_MyMeetings = new List<MOMDashboard_MyMeetings>();
            List<MOMDashboard_MyMeetings> List_MOMDashboard_TodayMeetings = new List<MOMDashboard_MyMeetings>();
            List<MOMDashboard_PartofMeetings> List_MOMDashboard_PartofMeetings = new List<MOMDashboard_PartofMeetings>();
            List_cmtObject.NoOfMyMeetings = 0;
            List_cmtObject.NoOfMyAttendanceSheets = 0;
            List_cmtObject.NoOfPartMeetings = 0;
            List_cmtObject.NoOfMyTasks = 0;
            List_cmtObject.NoOfMyTBTSessions = 0;

            try
            {

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                ds = ObjProviderModel.GetMOMDashboard(obj);
                dt = ds.Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    List_cmtObject.NoOfMyMeetings = Convert.ToInt32(dt.Rows[i]["NoOfMyMeetings"]);
                    List_cmtObject.NoOfMyAttendanceSheets = Convert.ToInt32(dt.Rows[i]["NoOfMyAttendanceSheets"]);
                    List_cmtObject.NoOfPartMeetings = Convert.ToInt32(dt.Rows[i]["NoOfPartMeetings"]);
                    List_cmtObject.NoOfMyTasks = Convert.ToInt32(dt.Rows[i]["NoOfMyTasks"]);
                    List_cmtObject.NoOfMyTBTSessions = Convert.ToInt32(dt.Rows[i]["NoOfMyTBTSessions"]);
                    List_cmtObject.NoOfDraftMeeting = Math.Round(Convert.ToDouble(dt.Rows[i]["NoOfDraftMeeting"]), 2);
                    List_cmtObject.NoOfNotPublishMeeting = Math.Round(Convert.ToDouble(dt.Rows[i]["NoOfNotPublishMeeting"]), 2);
                    List_cmtObject.NoOfPublishedMeeting = Math.Round(Convert.ToDouble(dt.Rows[i]["NoOfPublishedMeeting"]), 2);
                }
                dt = new DataTable();
                dt = ds.Tables[1];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MOMDashboard_MyMeetings objMOMDashboard_MyMeetings = new MOMDashboard_MyMeetings();
                    objMOMDashboard_MyMeetings.GenInfoAtdUno = Convert.ToInt32(dt.Rows[i]["GenInfoMetUno"]);
                    objMOMDashboard_MyMeetings.MoMNoAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["MoMNoAtd"]);
                    objMOMDashboard_MyMeetings.SubjectAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["SubjectAtd"]);
                    objMOMDashboard_MyMeetings.MeetingDate = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingDate"]);
                    objMOMDashboard_MyMeetings.StartTime = Helper.isCheckNullOrEmty(dt.Rows[i]["StartTime"]);
                    objMOMDashboard_MyMeetings.EndTime = Helper.isCheckNullOrEmty(dt.Rows[i]["EndTime"]);
                    objMOMDashboard_MyMeetings.ApprovalStatus = Helper.isCheckNullOrEmty(dt.Rows[i]["ApprovalStatus"]);
                    objMOMDashboard_MyMeetings.MeetingName = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingName"]);
                    objMOMDashboard_MyMeetings.MeetingLocation = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingLoc"]);
                    objMOMDashboard_MyMeetings.MeetingRoom = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingRoom"]);
                    List_MOMDashboard_MyMeetings.Insert(i, objMOMDashboard_MyMeetings);
                }
                List_cmtObject.LstMOMDashboard_MyMeetings = List_MOMDashboard_MyMeetings;
                dt = new DataTable();
                dt = ds.Tables[2];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MOMDashboard_PartofMeetings objMOMDashboard_PartofMeetings = new MOMDashboard_PartofMeetings();
                    objMOMDashboard_PartofMeetings.GenInfoAtdUno = Convert.ToInt32(dt.Rows[i]["GenInfoMetUno"]);
                    objMOMDashboard_PartofMeetings.MoMNoAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["MoMNoAtd"]);
                    objMOMDashboard_PartofMeetings.SubjectAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["SubjectAtd"]);
                    objMOMDashboard_PartofMeetings.MeetingName = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingName"]);
                    objMOMDashboard_PartofMeetings.MeetingDate = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingDate"]);
                    objMOMDashboard_PartofMeetings.StartTime = Helper.isCheckNullOrEmty(dt.Rows[i]["StartTime"]);
                    objMOMDashboard_PartofMeetings.EndTime = Helper.isCheckNullOrEmty(dt.Rows[i]["EndTime"]);
                    objMOMDashboard_PartofMeetings.ApprovalStatus = Helper.isCheckNullOrEmty(dt.Rows[i]["ApprovalStatus"]);
                    objMOMDashboard_PartofMeetings.MeetingLocation = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingLoc"]);
                    objMOMDashboard_PartofMeetings.MeetingRoom = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingRoom"]);
                    List_MOMDashboard_PartofMeetings.Insert(i, objMOMDashboard_PartofMeetings);
                }
                List_cmtObject.LstMOMDashboard_PartofMeetings = List_MOMDashboard_PartofMeetings;

                dt = new DataTable();
                dt = ds.Tables[3];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MOMDashboard_MyMeetings objMOMDashboard_MyMeetings = new MOMDashboard_MyMeetings();
                    objMOMDashboard_MyMeetings.GenInfoAtdUno = Convert.ToInt32(dt.Rows[i]["GenInfoMetUno"]);
                    objMOMDashboard_MyMeetings.MoMNoAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["MoMNoAtd"]);
                    objMOMDashboard_MyMeetings.SubjectAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["SubjectAtd"]);
                    objMOMDashboard_MyMeetings.MeetingDate = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingDate"]);
                    objMOMDashboard_MyMeetings.StartTime = Helper.isCheckNullOrEmty(dt.Rows[i]["StartTime"]);
                    objMOMDashboard_MyMeetings.EndTime = Helper.isCheckNullOrEmty(dt.Rows[i]["EndTime"]);
                    objMOMDashboard_MyMeetings.ApprovalStatus = Helper.isCheckNullOrEmty(dt.Rows[i]["ApprovalStatus"]);
                    objMOMDashboard_MyMeetings.MeetingName = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingName"]);
                    objMOMDashboard_MyMeetings.MeetingLocation = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingLoc"]);
                    objMOMDashboard_MyMeetings.MeetingRoom = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingRoom"]);
                    List_MOMDashboard_TodayMeetings.Insert(i, objMOMDashboard_MyMeetings);
                }
                List_cmtObject.lstMOMDashboard_TodayMeetings = List_MOMDashboard_TodayMeetings;

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return List_cmtObject;
        }

        #endregion

        #region Get Search List && Report
        public MOMDashboard GetSearchList(MOMDashboard obj)
        {
            MOMDashboard List_cmtObject = new MOMDashboard();
            List<MOMDashboard_MyMeetings> List_MOMDashboard_MyMeetings = new List<MOMDashboard_MyMeetings>();
            try
            {
                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                ds = ObjProviderModel.GetSearchList(obj);
                dt = ds.Tables[0];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MOMDashboard_MyMeetings objMOMDashboard_MyMeetings = new MOMDashboard_MyMeetings();
                    objMOMDashboard_MyMeetings.GenInfoAtdUno = Convert.ToInt32(dt.Rows[i]["GenInfoUno"]);
                    objMOMDashboard_MyMeetings.MoMNoAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["MoMNoAtd"]);
                    objMOMDashboard_MyMeetings.SubjectAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["SubjectAtd"]);
                    objMOMDashboard_MyMeetings.MeetingDate = Convert.ToDateTime(dt.Rows[i]["MeetingDate"]).ToString("dddd, dd MMMM yyyy");
                    objMOMDashboard_MyMeetings.StartTime = Helper.isCheckNullOrEmty(dt.Rows[i]["StartTime"]);
                    objMOMDashboard_MyMeetings.EndTime = Helper.isCheckNullOrEmty(dt.Rows[i]["EndTime"]);
                    objMOMDashboard_MyMeetings.ApprovalStatus = Helper.isCheckNullOrEmty(dt.Rows[i]["ApprovalStatus"]);
                    objMOMDashboard_MyMeetings.MeetingName = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingName"]);
                    objMOMDashboard_MyMeetings.MeetingLocation = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingLoc"]);
                    objMOMDashboard_MyMeetings.MeetingRoom = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingRoom"]);
                    List_MOMDashboard_MyMeetings.Insert(i, objMOMDashboard_MyMeetings);
                }
                List_cmtObject.LstMOMDashboard_MyMeetings = List_MOMDashboard_MyMeetings;


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return List_cmtObject;
        }


        public List<DivisionReport> GetDivMeetingList(MOMDashboard obj)
        {
            List<DivisionReport> List_Alert = new List<DivisionReport>();
            try
            {

                DataTable dt = new DataTable();
                dt = ObjProviderModel.GetDivMeetingList(obj);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DivisionReport objKPI = new DivisionReport();
                    objKPI.RecordCount = Convert.ToInt32(dt.Rows[i]["RCount"]);
                    objKPI.DivisionName = Helper.isCheckNullOrEmty(dt.Rows[i]["DivisionNameEn"]);
                    objKPI.UserName = Helper.isCheckNullOrEmty(dt.Rows[i]["DisplayNameEn"]);
                    List_Alert.Insert(i, objKPI);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return List_Alert;
        }

        #endregion



        #region EMAIL
        public EmailMeetingInfo GetEmailMeetingInfo(GenInfoAtdParam obj)
        {
            EmailMeetingInfo EmailObj = new EmailMeetingInfo();


            try
            {

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                ds = ObjProviderModel.GetEmailMeetingInfo(obj);
                dt = ds.Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    EmailObj.GenInfoAtdUno = Convert.ToInt32(dt.Rows[i]["GenInfoUno"]);
                    EmailObj.SubjectAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["Subject"]);
                    EmailObj.MomNoAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["MoMNo"]);
                    EmailObj.MeetingDateAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingDate"]);
                    EmailObj.StartTimeAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["StartTime"]);
                    EmailObj.EndTimeAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["EndTime"]);
                    EmailObj.locationName = Helper.isCheckNullOrEmty(dt.Rows[i]["LocationName"]);
                    EmailObj.MeetingName = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingName"]);
                    EmailObj.MeetingRoomName = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingRoom"]);
                    EmailObj.ApprovedName = Helper.isCheckNullOrEmty(dt.Rows[i]["AprovedName"]);
                    EmailObj.CreatedName = Helper.isCheckNullOrEmty(dt.Rows[i]["CreatedName"]);
                    EmailObj.ReviewerName = Helper.isCheckNullOrEmty(dt.Rows[i]["ReviewerName"]);
                    EmailObj.Email = Helper.isCheckNullOrEmty(dt.Rows[i]["Email"]);
                }
                dt = new DataTable();
                dt = ds.Tables[1];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AttendeesAtd objKPI = new AttendeesAtd();
                    objKPI.AttendeesUnoAtd = Convert.ToInt32(dt.Rows[i]["AttendeesUno"]);
                    objKPI.NameAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["Name"]);
                    objKPI.CompanyAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["Company"]);
                    objKPI.ContactAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["Contact"]);
                    objKPI.emailAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["Email"]);
                    objKPI.PrNumberAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["PrNumber"]);
                    objKPI.UserselUno = Convert.ToInt32(dt.Rows[i]["UserUno"]);
                    objKPI.GenInfoAtdUno = Convert.ToInt32(dt.Rows[i]["GenInfoUno"]);
                    objKPI.Status = Helper.isCheckNullOrEmty(dt.Rows[i]["Status"]);
                    objKPI.Role = Helper.isCheckNullOrEmty(dt.Rows[i]["UserRole"]);
                    EmailObj.ListAttendee.Add(objKPI);
                }
                if (ds.Tables.Count == 3)
                {
                    dt = new DataTable();
                    dt = ds.Tables[2];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AgendaDetails objAgenda = new AgendaDetails();
                        objAgenda.AgendaUno = Helper.isnumberCheckNullOrEmty(dt.Rows[i]["AgendaUno"]);
                        objAgenda.AgendaParentUno = Helper.isnumberCheckNullOrEmty(dt.Rows[i]["AgendaParentUno"]);
                        objAgenda.GenInfoAtdUno = Helper.isnumberCheckNullOrEmty(dt.Rows[i]["GenInfoUno"]);
                        objAgenda.Note = Helper.isCheckNullOrEmty(dt.Rows[i]["Note"]);
                        objAgenda.Responsibility = Helper.isCheckNullOrEmty(dt.Rows[i]["Responsibility"]);
                        objAgenda.DueDate = Helper.isCheckNullOrEmty(dt.Rows[i]["DueDate"]);
                        objAgenda.UserName = Helper.isCheckNullOrEmty(dt.Rows[i]["UserName"]);
                        EmailObj.ListAgenta.Add(objAgenda);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return EmailObj;
        }

        #endregion

        #region General Info

        public List<GenInfo> GetGeneralInfomation(GenInfoAtdParam obj)
        {
            List<GenInfo> List_GenInfo = new List<GenInfo>();
            try
            {

                DataTable dt = new DataTable();

                dt = ObjProviderModel.GetGeneralInfomation(obj);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    GenInfo objKPI = new GenInfo();
                    objKPI.GenInfoAtdUno = Convert.ToInt32(dt.Rows[i]["GenInfoUno"]);
                    objKPI.SubjectAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["Subject"]);
                    objKPI.MomNoAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["MoMNo"]);
                    objKPI.FMeetingDateAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingDate"]);
                    objKPI.FStartTimeAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["StartTime"]);
                    objKPI.FEndTimeAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["EndTime"]);
                    objKPI.locationUno = dt.Rows[i]["LocationUno"] == DBNull.Value ? 0 : Convert.ToInt32(dt.Rows[i]["LocationUno"]);
                    objKPI.locationName = Helper.isCheckNullOrEmty(dt.Rows[i]["LocationName"]);
                    objKPI.MeetingTypeUno = dt.Rows[i]["MeetingTypeUno"] == DBNull.Value ? 0 : Convert.ToInt32(dt.Rows[i]["MeetingTypeUno"]);
                    objKPI.MeetingName = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingName"]);
                    objKPI.MeetingRoomName = Helper.isCheckNullOrEmty(dt.Rows[i]["MeetingRoom"]);
                    objKPI.ApprovalStatus = Helper.isCheckNullOrEmty(dt.Rows[i]["ApprovalStatus"]);
                    objKPI.NumberOfAttendees = Helper.isCheckNullOrEmty(dt.Rows[i]["NoOfAttendees"]);
                    objKPI.ReviewerId = Convert.ToInt32(dt.Rows[i]["ReviewedBy"]);
                    objKPI.ApproverId = Convert.ToInt32(dt.Rows[i]["ApprovedBy"]);
                    objKPI.EnteredBy = Convert.ToInt32(dt.Rows[i]["EnteredBy"]);
                    objKPI.ApproverSign = Helper.isCheckNullOrEmty(dt.Rows[i]["ApproverSign"]);
                    objKPI.ReviewerSign = Helper.isCheckNullOrEmty(dt.Rows[i]["ReviewerSign"]);
                    objKPI.CreaterSign = Helper.isCheckNullOrEmty(dt.Rows[i]["CreatedSign"]);
                    objKPI.CreateDate = Helper.isCheckNullOrEmty(dt.Rows[i]["EnteredOn"]);
                    objKPI.ApprovalDate = Helper.isCheckNullOrEmty(dt.Rows[i]["ApprovedOn"]);
                    objKPI.ReviewDate = Helper.isCheckNullOrEmty(dt.Rows[i]["ReviewedOn"]);
                    objKPI.InitiatedBy = Helper.isCheckNullOrEmty(dt.Rows[i]["UserName"]);
                    objKPI.Published = Convert.ToInt32(dt.Rows[i]["Published"]);
                    List_GenInfo.Insert(i, objKPI);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return List_GenInfo;
        }

        public GenInfoAtdParam DelGeneralInfo(GenInfoAtdParam obj)
        {

            try
            {

                ObjProviderModel.DelGeneralInfo(obj);
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return obj;
        }

        public GenInfoAtdParam PublishGeneralInfo(GenInfoAtdParam obj, IWebHostEnvironment _envPost, string AppPath)
        {

            try
            {
                var result = "";
                if (obj.RecordType != 100)
                    result = ObjProviderModel.PublishGeneralInfo(obj);
                if (result == "")
                {
                    EmailHelper.GenInfoObj = obj;
                    EmailHelper._pdf = obj.pdfContent;
                    EmailHelper.MomUno = obj.MomNoAtd;
                    EmailHelper._env = _envPost;
                    EmailHelper._apppath = "http://" + AppPath;
                    if (obj.ActionType == "MEETING")
                    {
                        Task.Run(() => EmailHelper.SendMeetingInviteEmail());
                    }
                    else
                    {
                        Task<string> callTask = Task.Run(() => EmailHelper.SendInviteEmail());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return obj;
        }
        public GenInfoAtdParam CreateFollowupMeeting(GenInfoAtdParam obj)
        {

            try
            {

                obj.GenInfoAtdUno = Convert.ToInt32(ObjProviderModel.CreateFollowupMeeting(obj));
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return obj;
        }
        public string InsertUpdateGeneralInfo(GenInfoAtdInsert obj, IWebHostEnvironment _envPost, string AppPath)
        {
            GenInfoAtdParam EmailObj = new GenInfoAtdParam();
            EmailObj.ActionType = obj.ActionType;
            EmailObj.UserUno = obj.UserUno;
            EmailObj.GenInfoAtdUno = obj.GenInfoAtdUno;
            EmailHelper.GenInfoObj = EmailObj;
            EmailHelper._pdf = obj.pdfContent;
            EmailHelper.MomUno = obj.MomNoAtd;
            EmailHelper._env = _envPost;
            EmailHelper._apppath = "http://" + AppPath;
            string result = ObjProviderModel.GeneralInfoAtd_Insert_Update(obj);
            if (result != "failure" && obj.CallFrom != null && obj.CallFrom.ToUpper() == "APPROVE")
            {
                Task<string> callTask2 = Task.Run(() => EmailHelper.SendMeetingApproveEmail());
            }
            else if (result != "failure" && obj.CallFrom != null && obj.CallFrom.ToUpper() == "REVIEW")
            {
                Task<string> callTask2 = Task.Run(() => EmailHelper.SendMeetingReviewEmail());
            }
            return result;
        }


        #endregion

        #region Attendees 

        public List<AttendeesAtd> GetAttendees(AttendeesAtd obj)
        {
            List<AttendeesAtd> List_Attendees = new List<AttendeesAtd>();
            try
            {

                DataTable dt = new DataTable();

                dt = ObjProviderModel.GetAttendees(obj);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AttendeesAtd objKPI = new AttendeesAtd();
                    objKPI.AttendeesUnoAtd = Convert.ToInt32(dt.Rows[i]["AttendeesUno"]);
                    objKPI.NameAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["Name"]);
                    objKPI.CompanyAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["Company"]);
                    objKPI.ContactAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["Contact"]);
                    objKPI.emailAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["Email"]);
                    objKPI.PrNumberAtd = Helper.isCheckNullOrEmty(dt.Rows[i]["PrNumber"]);
                    objKPI.UserselUno = Convert.ToInt32(dt.Rows[i]["UserUno"]);
                    objKPI.GenInfoAtdUno = Convert.ToInt32(dt.Rows[i]["GenInfoUno"]);
                    objKPI.Status = Helper.isCheckNullOrEmty(dt.Rows[i]["Status"]);
                    objKPI.Role = Helper.isCheckNullOrEmty(dt.Rows[i]["UserRole"]);
                    objKPI.SignImage = Helper.isCheckNullOrEmty(dt.Rows[i]["AttendeeSign"]);
                    objKPI.ApologizedReason = Helper.isCheckNullOrEmty(dt.Rows[i]["ApologizedReason"]);
                    List_Attendees.Insert(i, objKPI);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return List_Attendees;
        }

        public string AttendeColumnUpdate(AttendeesAtd obj)
        {
            string result = ObjProviderModel.AttendeColumnUpdate(obj);
            return result;
        }

        public string InsertUpdateAttendees(AttendeesAtd obj)
        {

            string result = ObjProviderModel.Attendee_Insert_Update(obj);
            return result;
        }

        public string DeleteAttendeesAtd(AttendeesAtd obj)
        {
            obj.UserselUno = obj.UserUno;
            return ObjProviderModel.Attendee_Delete(obj);

        }


        public OutlookAttendeeInsert InsertOutlookAttendees(OutlookAttendeeInsert obj)
        {
            ObjProviderModel.InsertOutlookAttendees(obj);
            objOutlookEWS.SaveOutlookFile(obj.FileJSONString, obj.GenInfoUno, obj.UserUno, obj.ActionType);
            if (obj.ActionType == "MEETING")
            {
                List<OutlookAgenda> agendaObjList = new List<OutlookAgenda>();
                if (obj.AgendaJSONString != null && obj.AgendaJSONString != string.Empty)
                {
                    agendaObjList = JsonConvert.DeserializeObject<List<OutlookAgenda>>(obj.AgendaJSONString);
                    foreach (OutlookAgenda agendaObj in agendaObjList)
                    {
                        AgendaDetails objVal = new AgendaDetails();
                        objVal.AgendaUno = 0;
                        objVal.AgendaParentUno = 0;
                        objVal.ActionType = obj.ActionType;
                        objVal.GenInfoAtdUno = obj.GenInfoUno;
                        objVal.DueDate = DateTime.Today.ToShortDateString();
                        objVal.Note = agendaObj.Agenda;
                        objVal.UserID = obj.UserUno;
                        ObjProviderModel.AgendaDetails_Insert_Update(objVal);
                    }

                }
            }
            return obj;

        }
        #endregion

        #region Agenda

        public List<AgendaDetails> GetAgendaDetails(AgendaDetails obj)
        {
            List<AgendaDetails> List_Agenda = new List<AgendaDetails>();
            try
            {

                DataTable dt = new DataTable();
                dt = ObjProviderModel.GetAgendaDetails(obj);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AgendaDetails objAgenda = new AgendaDetails();
                    objAgenda.AgendaUno = Helper.isnumberCheckNullOrEmty(dt.Rows[i]["AgendaUno"]);
                    objAgenda.AgendaParentUno = Helper.isnumberCheckNullOrEmty(dt.Rows[i]["AgendaParentUno"]);
                    objAgenda.GenInfoAtdUno = Helper.isnumberCheckNullOrEmty(dt.Rows[i]["GenInfoUno"]);
                    objAgenda.Note = Helper.isCheckNullOrEmty(dt.Rows[i]["Note"]);
                    objAgenda.Responsibility = Helper.isCheckNullOrEmty(dt.Rows[i]["Responsibility"]);
                    objAgenda.DueDate = Helper.isCheckNullOrEmty(dt.Rows[i]["DueDate"]);
                    objAgenda.UserName = Helper.isCheckNullOrEmty(dt.Rows[i]["UserName"]);
                    objAgenda.IsDueDate = false;
                    objAgenda.IReps = false;
                    objAgenda.Expanded = false;
                    List_Agenda.Insert(i, objAgenda);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return List_Agenda;
        }

        public string InsertUpdateAgendaDetail(AgendaDetails obj)
        {

            string result = ObjProviderModel.AgendaDetails_Insert_Update(obj);
            return result;
        }

        public string DeleteAgendaDetails(AgendaDetails obj)
        {

            string result = ObjProviderModel.AgendaDetails_Delete(obj);
            return result;
        }

        #endregion

        #region Comment

        public string InsertAtdComment(cmtObject obj)
        {
            return ObjProviderModel.InsertAtdComment(obj);
        }

        public string DelAtdComment(cmtObject obj)
        {
            return ObjProviderModel.DelAtdComment(obj);
        }

        public List<cmtObject> GetAtdComment(cmtObject obj)
        {
            List<cmtObject> List_cmtObject = new List<cmtObject>();
            try
            {

                DataTable dt = new DataTable();
                dt = ObjProviderModel.GetAtdComment(obj);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    cmtObject objSLA = new cmtObject();
                    objSLA.CmdId = Convert.ToInt32(dt.Rows[i]["CommentUno"]);
                    objSLA.CmdText = Convert.ToString(dt.Rows[i]["Comment"]);
                    objSLA.GenralInfoId = Convert.ToInt32(dt.Rows[i]["GenInfoUno"]);
                    objSLA.UserName = Convert.ToString(dt.Rows[i]["UserName"]);
                    objSLA.CmtDate = Convert.ToString(dt.Rows[i]["CommentedOn"]);
                    objSLA.cmtUserName = Helper.isCheckNullOrEmty(dt.Rows[i]["VisibleName"]);
                    objSLA.CmtUsers = Helper.isCheckNullOrEmty(dt.Rows[i]["Commentusers"]);
                    objSLA.CmtUsers = objSLA.CmtUsers == string.Empty ? "Everyone" : objSLA.CmtUsers == obj.UserId.ToString() ? "Self" : "Private";
                    List_cmtObject.Insert(i, objSLA);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return List_cmtObject;
        }

        #endregion

        #region File Attachement
        public string fileUpload(int GenInfoId, int userId, string actionType, string Name, string ContentType, byte[] content)
        {
            string result = ObjProviderModel.fileUpload(GenInfoId, userId, actionType, Name, ContentType, content);
            return result;
        }
        public string DeleteAttachedFile(FileAttachement obj)
        {
            string result = ObjProviderModel.DeleteAttachedFile(obj);
            return result;
        }
        public DataTable DownloadAttachFile(FileAttachement obj)
        {
            return ObjProviderModel.GetFileList(obj);
        }
        public List<FileAttachement> GetFileList(FileAttachement obj)
        {
            List<FileAttachement> ListUpload = new List<FileAttachement>();

            DataTable dt = ObjProviderModel.GetFileList(obj);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                FileAttachement objUpload = new FileAttachement();
                objUpload.AttachmentUno = Convert.ToInt32(dt.Rows[i]["AttachmentUno"]);
                objUpload.GeneralInfoId = Convert.ToInt32(dt.Rows[i]["GenInfoUno"]);
                objUpload.FileName = Helper.isCheckNullOrEmty(dt.Rows[i]["FileName"]);
                objUpload.ContentType = Helper.isCheckNullOrEmty(dt.Rows[i]["ContentType"]);
                objUpload.CreateDate = Helper.isCheckNullOrEmty(dt.Rows[i]["CreateDate"]);
                objUpload.CreateByName = Helper.isCheckNullOrEmty(dt.Rows[i]["UserName"]);
                if (!objUpload.FileName.ToLower().Contains(".ics") && !objUpload.FileName.ToLower().Contains(".exe") && !objUpload.FileName.ToLower().Contains(".bat") && !objUpload.FileName.ToLower().Contains(".msi"))
                    ListUpload.Insert(i, objUpload);
            }

            return ListUpload;
        }

        #endregion

        #region Meeting Appointments 

        public List<OutlookMeeting> GetMeetings(OutlookMeeting obj)
        {
            return objOutlookEWS.GetMeetings(obj);
        }

        #endregion

        #region Alerts
        public List<Alert> GetAlertList(Alert obj)
        {
            List<Alert> List_Alert = new List<Alert>();
            try
            {

                DataTable dt = new DataTable();
                dt = ObjProviderModel.GetAlertList(obj);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Alert objKPI = new Alert();
                    objKPI.Id = Convert.ToInt32(dt.Rows[i]["ID"]);
                    objKPI.GenInfoUno = Convert.ToInt32(dt.Rows[i]["GenInfoUno"]);
                    objKPI.IsVisited = Convert.ToBoolean(dt.Rows[i]["IsVisited"]);
                    objKPI.UserUno = Convert.ToInt32(dt.Rows[i]["UserID"]);
                    objKPI.Description = Helper.isCheckNullOrEmty(dt.Rows[i]["Description"]);
                    objKPI.Type = Helper.isCheckNullOrEmty(dt.Rows[i]["Type"]);
                    objKPI.CreatedDate = Convert.ToDateTime(dt.Rows[i]["CreatedDate"]);
                    List_Alert.Insert(i, objKPI);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return List_Alert;
        }

        public string MarkAsVisited(Alert obj)
        {

            string result = ObjProviderModel.MarkAsVisited(obj);
            return result;
        }
        #endregion




































        #region Division
        // GetDivision Start
        public List<Division> GetDivision(Division obj)
        {
            List<Division> List_Division = new List<Division>();
            try
            {
                string result = string.Empty;
                DataTable dt = new DataTable();
                dt = ObjProviderModel.GetDivision(obj);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Division objDivision = new Division();
                    objDivision.DivisionNameEn = Helper.isCheckNullOrEmty(dt.Rows[i]["DivisionNameEn"]);
                    objDivision.DivisionNameAr = Helper.isCheckNullOrEmty(dt.Rows[i]["DivisionNameAr"]);
                    objDivision.DivisionUno = Convert.ToInt32(dt.Rows[i]["DivisionUno"]);
                    objDivision.DivisionHeadUno = Convert.ToInt32(dt.Rows[i]["DivisionHeadUno"]);
                    objDivision.DivisionAdminUno = Convert.ToInt32(dt.Rows[i]["DivisionAdminUno"]);
                    objDivision.DivisionAdminName = Helper.isCheckNullOrEmty(dt.Rows[i]["DivisionAdminName"]);
                    objDivision.ApproverName = Helper.isCheckNullOrEmty(dt.Rows[i]["ApproverName"]);
                    objDivision.DivisionCode = Helper.isCheckNullOrEmty(dt.Rows[i]["DivisionCode"]);
                    objDivision.CoordinatorName = Helper.isCheckNullOrEmty(dt.Rows[i]["CoordinatorName"]);
                    objDivision.DivisionCoordinatorUno = Convert.ToInt32(dt.Rows[i]["DivisionCoordinatorUno"]);
                    objDivision.DivisionApproverUno = Convert.ToInt32(dt.Rows[i]["DivisionApproverUno"]);

                    List_Division.Insert(i, objDivision);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return List_Division;


        }


        #endregion



    }
}


