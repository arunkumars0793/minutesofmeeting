﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinutesOfMeeting.Models.Helpers
{
    public class Admin
    {
        public class Department
        {
            public int DepartmentUno { get; set; }
            public string DepartmentEn { get; set; }
            public string DepartmentAr { get; set; }
            public string DepartmentCode { get; set; }
            public int DepartmentAdminUno { get; set; }

            public int DivisionUno { get; set; }
            public string DivisionName { get; set; }
            public string DepartmentAdminName { get; set; }
            public string CoordinatorName { get; set; }
            public string ApproverName { get; set; }

            public int KPILastAutoId { get; set; }
            public int DepartmentCoordinatorUno { get; set; }
            public int DepartmentApproverUno { get; set; }
            public int IsActive { get; set; }
            public string EnteredBy { get; set; }
            public DateTime EnteredOn { get; set; }
            public int ModifiedBy { get; set; }
            public DateTime ModifiedOn { get; set; }
            public int DeletedBy { get; set; }
            public DateTime DeletedOn { get; set; }
            public string Flag { get; set; }
            //filter
            public int UserUno { get; set; }
            public int TenantUno { get; set; }
            public int LanguageUno { get; set; }
            public int ItemUno { get; set; }
            public int Condition { get; set; }
            public string XMLData { get; set; }
        }

        public class Division
        {
            public int DivisionUno { get; set; }
            public string DivisionNameEn { get; set; }
            public string DivisionNameAr { get; set; }
            public int DivisionHeadUno { get; set; }
            public int DivisionAdminUno { get; set; }
            public string DivisionCode { get; set; }
            public int KPILastAutoId { get; set; }
            public int DivisionCoordinatorUno { get; set; }
            public int DivisionApproverUno { get; set; }

            public string DivisionName { get; set; }
            public string DivisionAdminName { get; set; }
            public string CoordinatorName { get; set; }
            public string ApproverName { get; set; }

            public int IsActive { get; set; }
            public int EnteredBy { get; set; }
            public DateTime EnteredOn { get; set; }
            public int ModifiedBy { get; set; }
            public DateTime ModifiedOn { get; set; }
            public int DeletedBy { get; set; }
            public DateTime DeletedOn { get; set; }

            public int ID { get; set; }
            public int UserUno { get; set; }
            public int LanguageUno { get; set; }

            public int Condition { get; set; }
            public string Flag { get; set; }
            public string XMLData { get; set; }
        }

        public class User
        {
            public int UserUno { get; set; }
            public int TenantUno { get; set; }
            public int LanguageUno { get; set; }
            public int DivisionUno { get; set; }
            public int ItemUno { get; set; }
            public int Condition { get; set; }
            public string DisplayText { get; set; }
            public string UserImage { get; set; }
        }

        public class Interaction
        {
            public int InteractionUno { get; set; }
            public string InteractionNameEn { get; set; }
            public string InteractionNameAr { get; set; }

            //fillter
            public int UserUno { get; set; }
            public int TenantUno { get; set; }
            public int LanguageUno { get; set; }
            public int ItemUno { get; set; }
            public int Condition { get; set; }
            public string Flag { get; set; }
            public int IsActive { get; set; }
            public int EnteredBy { get; set; }
            public DateTime EnteredOn { get; set; }
            public int ModifiedBy { get; set; }
            public DateTime ModifiedOn { get; set; }
            public int DeletedBy { get; set; }
            public DateTime DeletedOn { get; set; }
            public string XMLData { get; set; }
        }  
    
        public class UserMaster
        {
            public int ID { get; set; }
            public string NTUsername { get; set; }
            public string DisplayNameEn { get; set; }
            public string DisplayNameAr { get; set; }
            public string Firstname { get; set; }
            public string Lastname { get; set; }
            public string EmailId { get; set; }
            public string DesignationEn { get; set; }
            public int DivisionUno { get; set; }
            public int EnteredBy { get; set; }
            public string UserName { get; set; }
            public string NTLogin { get; set; }
            public string DesignationAr { get; set; }
            public string PrNumber { get; set; }
            public string DivisionNameEn { get; set; }
            public string DivisionNameAr { get; set; }
            public int LanguageUno { get; set; }
            public int UserUno { get; set; }
            public int Condition { get; set; }
            public int Role { get; set; }

            public bool IsTBT { get; set; }
            public string Picture { get; set; }
        }
    }
}

    

