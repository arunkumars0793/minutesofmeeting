﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinutesOfMeeting.Models.Helpers
{
    public class Helper
    {
        public static string FixBase64ForImage(string Image)
        {
            System.Text.StringBuilder sbText = new System.Text.StringBuilder(Image, Image.Length);
            sbText.Replace("\r\n", string.Empty); sbText.Replace(" ", string.Empty);
            return sbText.ToString();
        }
        public static string isCheckNullOrEmty_user(object objValue)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(objValue)))
            {
                string strRtn = Convert.ToString(objValue);
                strRtn = strRtn.Replace("0,", "").Replace(",0", "").Replace("0", "");
                return strRtn;
            }
            return string.Empty;
        }
        public static string isCheckNullOrEmty(object objValue)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(objValue)))
            {
                return Convert.ToString(objValue);
            }
            return string.Empty;
        }
        public static string isCheckNullOrEmtyImage(object objValue)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(objValue)))
            {
                return Convert.ToString(objValue);
            }
            return "/assets/img/user-default.png";
        }

        public static int isnumberCheckNullOrEmty(object objValue)
        {
            if (objValue != System.DBNull.Value)
            {
                return Convert.ToInt32(objValue.ToString() != "" ? objValue : "0");
            }
            return 0;
        }
        public static DateTime isdateCheckNullOrEmty(object objValue)
        {
            if (objValue != null)
            {
                return Convert.ToDateTime(objValue);
            }
            return Convert.ToDateTime("1/1/1999");
        }
        public static string isCheckNullOrEmtyAndEncrypt(object objValue)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(objValue)))
            {
                return objValue.ToString();//EncryptStringAES(Convert.ToString(objValue));
            }
            return string.Empty;
        }
        public static DateTime isCheckDatetime(object objValDateTime)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(objValDateTime)))
            {
                return Convert.ToDateTime(objValDateTime);
            }
            return DateTime.MinValue;
        }

        public static string GetFileContentType(string ext)
        {

            string contentType = "";
            switch (ext)
            {
                case ".txt":
                    contentType = "text/plain";
                    break;
                case ".doc":
                case ".dot":
                    contentType = "application/msword";
                    break;
                case ".docx":
                    contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    break;
                case ".dotx":
                    contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.template";
                    break;
                case ".xls":
                case ".xlt":
                case ".xla":
                    contentType = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    break;
                case ".xltx":
                    contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
                    break;
                case ".ppt":
                case ".pot":
                case ".pps":
                case ".ppa":
                    contentType = "application/vnd.ms-powerpoint";
                    break;
                case ".pptx":
                    contentType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                    break;
                case ".potx":
                    contentType = "application/vnd.openxmlformats-officedocument.presentationml.template";
                    break;
                case ".mdb":
                    contentType = "application/vnd.ms-access";
                    break;
                case ".pdf":
                    contentType = "application/pdf";
                    break;
                case ".gif":
                    contentType = "image/gif";
                    break;
                case ".jpeg":
                    contentType = "image/jpeg";
                    break;
                case ".jpg":
                    contentType = "image/jpeg";
                    break;
                case ".png":
                    contentType = "image/png";
                    break;
                case ".avi":
                    contentType = "video/x-msvideo";
                    break;
                case ".mp4":
                    contentType = "video/mpeg";
                    break;
                case ".mpeg":
                    contentType = "video/mpeg";
                    break;
                case ".mpg":
                    contentType = "video/mpeg";
                    break;
            }
            return contentType;
        }
    }
}

 
