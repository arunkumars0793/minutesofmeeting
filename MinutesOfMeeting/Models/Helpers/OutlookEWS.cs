﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Exchange.WebServices.Data;
using MinutesOfMeeting.Models.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace MinutesOfMeeting.Models.Helpers
{
    public class OutlookEWS
    {
        SqlDataProviderBiz ObjProviderModel = new SqlDataProviderBiz();
        ExchangeService exchangeService;
        public IWebHostEnvironment _Errorlog { get; set; }
        private bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            // The default for the validation callback is to reject the URL.
            bool result = false;
            Uri redirectionUri = new Uri(redirectionUrl);
            // Validate the contents of the redirection URL. In this simple validation
            // callback, the redirection URL is considered valid if it is using HTTPS
            // to encrypt the authentication credentials. 
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }


        public List<OutlookMeeting> GetMeetings(OutlookMeeting obj)
        {
            try
            {
                CommonClass objCommonClass = new CommonClass();
                AppSettings appSettings = objCommonClass.GetEmailHostSetting(); // Get the Eamil Hosting details from App settings
                exchangeService = new ExchangeService(ExchangeVersion.Exchange2013);
                exchangeService.Credentials = new WebCredentials(appSettings.EmailUserName, appSettings.EmailPassword);
                exchangeService.AutodiscoverUrl(obj.Email, RedirectionUrlValidationCallback);
                DateTime startDate = Convert.ToDateTime(DateTime.Now.AddDays(obj.DayCount).ToShortDateString());
                DateTime endDate = Convert.ToDateTime(startDate.AddDays(1).ToShortDateString());
                return RetrieveAppointments(obj.Email, startDate, endDate);

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return new List<OutlookMeeting>();
        }


        public string SaveOutlookFile(string FileList, int GeneInfoId, int UserUno, string actionType)
        {
            try
            {
                List<OutlookFile> fileObjList = new List<OutlookFile>();
                ObjProviderModel._Errorlog = _Errorlog;
                if (FileList != null && FileList != string.Empty)
                {
                    fileObjList = JsonConvert.DeserializeObject<List<OutlookFile>>(FileList);

                }
                if (fileObjList.Count > 0)
                {
                    CommonClass objCommonClass = new CommonClass();
                    AppSettings appSettings = objCommonClass.GetEmailHostSetting();
                    exchangeService = new ExchangeService(ExchangeVersion.Exchange2013);
                    exchangeService.Credentials = new WebCredentials(appSettings.EmailUserName, appSettings.EmailPassword);
                    exchangeService.AutodiscoverUrl(fileObjList[0].Email, RedirectionUrlValidationCallback);
                    DateTime startDate = Convert.ToDateTime(DateTime.Now.AddDays(fileObjList[0].DayCount).ToShortDateString());
                    DateTime endDate = Convert.ToDateTime(startDate.AddDays(1).ToShortDateString());
                    try
                    {
                        exchangeService.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, fileObjList[0].Email);
                        CalendarFolder folder = CalendarFolder.Bind(exchangeService, WellKnownFolderName.Calendar, new PropertySet());
                        const int NUM_APPTS = 50;
                        CalendarView cView = new CalendarView(startDate, endDate, NUM_APPTS);
                        FindItemsResults<Appointment> results = folder.FindAppointments(cView);
                        foreach (Appointment appointment in results)
                        {
                            Appointment message = Appointment.Bind(exchangeService, appointment.Id, new PropertySet(ItemSchema.Attachments));
                            foreach (Attachment attachment in message.Attachments)
                            {
                                OutlookFileList fileObj = new OutlookFileList();
                                if (attachment is FileAttachment)
                                {
                                    FileAttachment fileAttachment = attachment as FileAttachment;
                                    {
                                        if (fileObjList.Exists(item => item.FileName == fileAttachment.Name))
                                        {
                                            if (!fileAttachment.Name.ToLower().Contains(".ics") && !fileAttachment.Name.ToLower().Contains(".exe") && !fileAttachment.Name.ToLower().Contains(".bat") && !fileAttachment.Name.ToLower().Contains(".msi"))
                                            {
                                                var i = 0;
                                                i++;
                                                //Load the attachment in memory from Exchange.
                                                MemoryStream stream = new MemoryStream();
                                                fileAttachment.Load(stream);
                                                StreamReader reader = new StreamReader(stream);
                                                stream.Seek(0, SeekOrigin.Begin);
                                                string ext = Path.GetExtension(fileAttachment.Name);
                                                string contentType = CommonClass.GetFileContentType(ext);
                                                byte[] fileContent = stream.GetBuffer();
                                                var name = fileAttachment.Name.Split(".");
                                                var filename = fileAttachment.Name;
                                                if (name.Length > 1)
                                                    filename = "Attachment - " + i + "." + name[1];
                                               
                                                ObjProviderModel.fileUpload(GeneInfoId, UserUno, actionType, filename, contentType, fileContent);
                                            }
                                        }
                                    }
                                }

                            }
                        }
                        exchangeService.ImpersonatedUserId = null;
                    }
                    catch (Exception exp)
                    {
                        ErrorLog.LogFileCreation(_Errorlog, exp);

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }
            return "success";
        }
        /// <summary>
        /// Method to use retrieve the appointments
        /// </summary>
        /// <param name="mailBox"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<OutlookMeeting> RetrieveAppointments(string mailBox, DateTime startDate, DateTime endDate)
        {
            List<OutlookMeeting> ListMeeting = new List<OutlookMeeting>();
            try
            {


                //Set the ImpersonatedUserId property of the ExchangeService object to identify the impersonated user (target account).
                //This example uses the user's SMTP email address.
                exchangeService.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, mailBox);

                //bind to the calendar folder of mailBox and load all its first class properties
                //CalendarFolder folder = CalendarFolder.Bind(exchangeService, WellKnownFolderName.Calendar);
                CalendarFolder folder = CalendarFolder.Bind(exchangeService, WellKnownFolderName.Calendar, new PropertySet());

                //Represents a date range view of appointments in calendar folder search operations
                //If the CalendarView element is specified the Web service returns a list of single calendar items
                //and occurrences of recurring calendar items within the range specified by StartDate and EndDate
                const int NUM_APPTS = 50;
                CalendarView cView = new CalendarView(startDate, endDate, NUM_APPTS);

                //search for appointments matching range specified in calendar view
                FindItemsResults<Appointment> results = folder.FindAppointments(cView);

                //print results
                foreach (Appointment appointment in results)
                {
                    OutlookMeeting emailObj = new OutlookMeeting();
                    //find appointments will only give basic properties.
                    //in order to get more properties (such as BODY), we need to call call EWS again
                    Appointment appointmentDetailed = Appointment.Bind(exchangeService, appointment.Id, new PropertySet(BasePropertySet.FirstClassProperties) { RequestedBodyType = BodyType.Text });


                    emailObj.ApptId = Helper.isCheckNullOrEmty(appointment.Id);
                    emailObj.Subject = Helper.isCheckNullOrEmty(appointment.Subject);
                    emailObj.Start = Helper.isCheckNullOrEmty(appointment.Start);
                    emailObj.End = Helper.isCheckNullOrEmty(appointment.End);
                    emailObj.Location = Helper.isCheckNullOrEmty(appointment.Location);
                    emailObj.MeetingDate = Helper.isCheckNullOrEmty(appointment.Start.ToString("dddd, dd MMMM yyyy HH:mm tt"));
                    emailObj.UserName = Helper.isCheckNullOrEmty(appointment.LastModifiedName);

                    PropertySet propSet = new PropertySet(BasePropertySet.IdOnly, ItemSchema.TextBody, EmailMessageSchema.Body);
                    Appointment messageBody = Appointment.Bind(exchangeService, appointment.Id, propSet);
                    emailObj.BodyText = Helper.isCheckNullOrEmty(messageBody.Body.Text);
                    if (emailObj.BodyText.ToLower().Contains("agenda"))
                    {
                        HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                        doc.LoadHtml(emailObj.BodyText);
                        List<List<string>> table = doc.DocumentNode.SelectNodes("//table")
                        .Descendants("tr")
                        .Skip(1)
                        .Where(tr => tr.Elements("td").Count() == 2)
                        .Select(tr => tr.Elements("td").Select(td => td.InnerText.Trim()).ToList())
                        .ToList();

                        if (table.Count > 1)
                        {
                            if (table[0][1].ToLower().Contains("agenda"))
                            {
                                for (var i = 1; i < table.Count; i++)
                                {
                                    OutlookAgenda agenda = new OutlookAgenda();
                                    agenda.Agenda = table[i][1];
                                    agenda.Active = true;
                                    emailObj.AgendaList.Add(agenda);
                                }
                            }
                        }
                    }



                    if (appointment.IsCancelled.Equals(false))
                    {


                        foreach (Attendee attendee in appointmentDetailed.RequiredAttendees)
                        {
                            try
                            {
                                ExpandGroupResults myGroupMembers = exchangeService.ExpandGroup(attendee.Address);
                                if (myGroupMembers.Count > 0)
                                {
                                    // Display the group members.
                                    foreach (EmailAddress address in myGroupMembers.Members)
                                    {
                                        if (emailObj.ListAttendee.Where(item => item.Email == address.Address).Count() == 0)
                                        {
                                            OutlookAttendee Attendee = new OutlookAttendee();
                                            Attendee.Email = address.Address;
                                            Attendee.Name = address.Name;
                                            Attendee.PRN = "";
                                            Attendee.UserUno = 0;
                                            Attendee.Active = true;
                                            emailObj.ListAttendee.Add(Attendee);
                                        }
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                if (emailObj.ListAttendee.Where(item => item.Email == attendee.Address).Count() == 0)
                                {
                                    OutlookAttendee Attendee = new OutlookAttendee();
                                    Attendee.Email = attendee.Address;
                                    Attendee.Name = attendee.Name;
                                    Attendee.PRN = "";
                                    Attendee.UserUno = 0;
                                    Attendee.Active = true;
                                    emailObj.ListAttendee.Add(Attendee);
                                }
                            }
                        }

                        foreach (Attendee attendee in appointmentDetailed.OptionalAttendees)
                        {
                            try
                            {
                                ExpandGroupResults myGroupMembers = exchangeService.ExpandGroup(attendee.Address);
                                if (myGroupMembers.Count > 0)
                                {
                                    // Display the group members.
                                    foreach (EmailAddress address in myGroupMembers.Members)
                                    {
                                        if (emailObj.ListAttendee.Where(item => item.Email == address.Address).Count() == 0)
                                        {
                                            OutlookAttendee Attendee = new OutlookAttendee();
                                            Attendee.Email = address.Address;
                                            Attendee.Name = address.Name;
                                            Attendee.PRN = "";
                                            Attendee.UserUno = 0;
                                            Attendee.Active = true;
                                            emailObj.ListAttendee.Add(Attendee);
                                        }
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                if (emailObj.ListAttendee.Where(item => item.Email == attendee.Address).Count() == 0)
                                {
                                    OutlookAttendee Attendee = new OutlookAttendee();
                                    Attendee.Email = attendee.Address;
                                    Attendee.Name = attendee.Name;
                                    Attendee.PRN = "";
                                    Attendee.UserUno = 0;
                                    Attendee.Active = true;
                                    emailObj.ListAttendee.Add(Attendee);
                                }
                            }
                        }
                    }

                    // Bind to an existing message item and retrieve the attachments collection.
                    // This method results in an GetItem call to EWS.
                    Appointment message = Appointment.Bind(exchangeService, appointment.Id, new PropertySet(ItemSchema.Attachments));

                    foreach (Attachment attachment in message.Attachments)
                    {
                        var i = 0;
                        i++;
                        OutlookFileList fileObj = new OutlookFileList();
                        if (attachment is FileAttachment)
                        {
                            FileAttachment fileAttachment = attachment as FileAttachment;
                            if (!fileAttachment.Name.ToLower().Contains(".ics") && !fileAttachment.Name.ToLower().Contains(".exe") && !fileAttachment.Name.ToLower().Contains(".bat") && !fileAttachment.Name.ToLower().Contains(".msi"))
                            {
                                fileObj.Id = fileAttachment.Id;
                                var name = fileAttachment.Name.Split(".");
                                if (name.Length > 1)
                                    fileObj.FileName = "Attachment - " + i + "." + name[1];
                                else
                                    fileObj.FileName = fileAttachment.Name;
                                fileObj.Active = true;
                                // Load the attachment into a file.
                                // This call results in a GetAttachment call to EWS.                         
                                emailObj.FileList.Add(fileObj);
                            }
                        }
                      
                    }
                    ListMeeting.Add(emailObj);
                }

                //Set it back to null so that any actions that will be taken using the exchange service
                //applies to impersonating account (i.e.account used in network credentials)
                exchangeService.ImpersonatedUserId = null;
            }
            catch (Exception exp)
            {
                ErrorLog.LogFileCreation(_Errorlog, exp);

            }

            return ListMeeting;
        }
    }
}
