﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinutesOfMeeting.Models.Helpers
{
    public class Users
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int Status { get; set; }
        public string Token { get; set; }
        public string Language { get; set; }
        public int UserID { get; set; }
        public int MasterUserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public string Designation { get; set; }
        public string Division { get; set; }
        public string PhoneNumber { get; set; }
        public string Department { get; set; }
        public string EmailId { get; set; }
     
        public string AccountId { get; set; }
        public string AccountJId { get; set; }
        public string ProfileStatus { get; set; }
        public string ChatStatus { get; set; }
        public int RoleId { get; set; }
        public int Role { get; set; }

        public bool IsTBT { get; set; }
        public string Picture { get; set; }
        public string ProfilePicture { get; set; }
        public int IsDisabledforSearch { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Error { get; set; }
        public bool IsMessage { get; set; }
        public int MessageCount { get; set; }
        public string IsChatVisible { get; set; }
        public string TermsandConditions { get; set; }
        public int GroupID { get; set; }
        public string VersionNo { get; set; }
        public int StartIndex { get; set; }
        public int MaxCount { get; set; }
        public string SearchText { get; set; }
        public bool IsProfileSearch { get; set; }

        public int UserUno { get; set; }
        public int TenantUno { get; set; }
        public int LanguageUno { get; set; }
        public int ItemUno { get; set; }
        public int Condition { get; set; }

        public int CurrentYear { get; set; }
        public int CurrentHalf { get; set; }
        public int CurrentQuarter { get; set; }
        public int CurrentMonth { get; set; }
        public int WellperformKPIRange { get; set; }
        public string UserPhoto { get; set; }
        public string FromEmail { get; set; }
        public int UserGroupUno { get; set; }
        public string UserPRnumber { get; set; }

    }

    public class UsersList
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public int IsActive { get; set; }
        public string AccountId { get; set; }
        public string Designation { get; set; }
        public string Picture { get; set; }
        public string Message { get; set; }
        public string Flag { get; set; }

    }
    public class UserGroup
    {
        public int UsergroupUno { get; set; }
        public string UsergroupName { get; set; }

        public int PageUno { get; set; }
        public string PageName { get; set; }
        public Boolean ShowPage { get; set; }
        public Boolean ShowAdd { get; set; }
        public Boolean ShowEdit { get; set; }
        public Boolean ShowDelete { get; set; }
        public string SectionName { get; set; }
        public string PageType { get; set; }
        public string RightsXML { get; set; }
        public int UserUno { get; set; }
        public int TenantUno { get; set; }
        public int LanguageUno { get; set; }
        public int ItemUno { get; set; }
        public int Condition { get; set; }
        public int EnteredBy { get; set; }

        public UserGroup[] ListPage { get; set; }
    }

    public class Page
    {
        public int PageUno { get; set; }
        public string PageName { get; set; }
        public Boolean ShowPage { get; set; }
        public Boolean ShowAdd { get; set; }
        public Boolean ShowEdit { get; set; }
        public Boolean ShowDelete { get; set; }
        public string SectionName { get; set; }
        public string PageType { get; set; }
        public int UserUno { get; set; }
        public int TenantUno { get; set; }
        public int LanguageUno { get; set; }
        public int ItemUno { get; set; }
        public int Condition { get; set; }
    }

    public class UserRights
    {
        public int PageUno { get; set; }
        public string PageName { get; set; }
        public Boolean ShowPage { get; set; }
        public Boolean ShowAdd { get; set; }
        public Boolean ShowEdit { get; set; }
        public Boolean ShowDelete { get; set; }
        public int UserUno { get; set; }
        public int UsergroupUno { get; set; }
        public string UsergroupName { get; set; }
        public int TenantUno { get; set; }
        public int LanguageUno { get; set; }
        public int ItemUno { get; set; }
        public int Condition { get; set; }
    }
}

