﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MinutesOfMeeting.Models.Helpers
{
    public class CommonClass
    {
        public string isCheckNullOrEmty(object objValue)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(objValue)))
            {
                return Convert.ToString(objValue);
            }
            return string.Empty;
        }

        public string isCheckNullOrEmtyAndEncrypt(object objValue)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(objValue)))
            {
                return EncryptStringAES(Convert.ToString(objValue));
            }
            return string.Empty;
        }
        public DateTime isCheckDatetime(object objValDateTime)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(objValDateTime)))
            {
                return Convert.ToDateTime(objValDateTime);
            }
            return DateTime.MinValue;
        }


        public string GetConnectionStringName()
        {
            var builder = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json");

            var connectionStringConfig = builder.Build();
            if (connectionStringConfig != null)
                return connectionStringConfig.GetConnectionString("DefaultConnection").Replace("~", "\\");
            else
                return string.Empty;

        }
        public string GetErrorLogPath()
        {
            var builder = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json");

            var connectionStringConfig = builder.Build();
            if (connectionStringConfig != null)
                return connectionStringConfig.GetConnectionString("ErrorLog");
            else
                return string.Empty;

        }

        public string GetDataLogPath()
        {
            var builder = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json");

            var connectionStringConfig = builder.Build();
            if (connectionStringConfig != null)
                return connectionStringConfig.GetConnectionString("DataLog");
            else
                return string.Empty;

        }
        public Boolean EnableDataLog()
        {
            var builder = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json");

            var connectionStringConfig = builder.Build();
            if (connectionStringConfig != null)
                return Convert.ToBoolean(connectionStringConfig.GetConnectionString("EnableDataLog"));
            else
                return false;

        }


        public string GetAppSettingsConValue(string key)
        {
            var builder = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json");

            var connectionStringConfig = builder.Build();
            if (connectionStringConfig != null)
                return connectionStringConfig.GetConnectionString(key);
            else
                return "";

        }


        public string GetADDomain()
        {
            var builder = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json");

            var ADDomainConfig = builder.Build();
            if (ADDomainConfig != null)
                return ADDomainConfig.GetConnectionString("Domain");
            else
                return string.Empty;

        }
        public string GetADPath()
        {
            var builder = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json");

            var ADPathConfig = builder.Build();
            if (ADPathConfig != null)
                return ADPathConfig.GetConnectionString("ADPath");
            else
                return string.Empty;

        }

        public AppSettings GetEmailHostSetting()
        {
            AppSettings objAppSettings = new AppSettings();
            var builder = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json");

            var connectionStringConfig = builder.Build();
            if (connectionStringConfig != null)
            {
                objAppSettings.EmailFromAddress = connectionStringConfig.GetSection("AppSettings").GetSection("EmailFromAddress").Value;
                objAppSettings.EmailHost = connectionStringConfig.GetSection("AppSettings").GetSection("EmailHost").Value;
                objAppSettings.EmailUserName = connectionStringConfig.GetSection("AppSettings").GetSection("EmailUserName").Value;
                objAppSettings.EmailPassword = connectionStringConfig.GetSection("AppSettings").GetSection("EmailPassword").Value;
                objAppSettings.EmailPort = connectionStringConfig.GetSection("AppSettings").GetSection("EmailPort").Value;
                objAppSettings.EmailSSL = Convert.ToBoolean(connectionStringConfig.GetSection("AppSettings").GetSection("EmailSSL").Value);
                objAppSettings.EmailExchangeURL = connectionStringConfig.GetSection("AppSettings").GetSection("EmailExchangeURL").Value;
                objAppSettings.SiteURL = connectionStringConfig.GetSection("AppSettings").GetSection("SiteURL").Value;
            }

            return objAppSettings;
        }

        public static AppSettings GetSQLWindowsAuthendicationSetting()
        {
            AppSettings objAppSettings = new AppSettings();
            var builder = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json");

            var connectionStringConfig = builder.Build();
            if (connectionStringConfig != null)
            {
                objAppSettings.IsWindows_Authendication = Convert.ToBoolean(connectionStringConfig.GetSection("AppSettings").GetSection("ISWINDOWS_AUTHENDICATION").Value);
                objAppSettings.Windows_UserName = connectionStringConfig.GetSection("AppSettings").GetSection("WINDOWS_USERNAME").Value;
                objAppSettings.Windows_Password = connectionStringConfig.GetSection("AppSettings").GetSection("WINDOWS_PASSWORD").Value;
                objAppSettings.Windows_Domain = connectionStringConfig.GetSection("AppSettings").GetSection("WINDOWS_DOMAINNAME").Value;
            }

            return objAppSettings;
        }

        public Tuple<string, int, string, bool> GetConfigPageSettingValues()
        {
            var builder = new ConfigurationBuilder()
         .SetBasePath(Directory.GetCurrentDirectory())
         .AddJsonFile("appsettings.json");
            var connectionStringConfig = builder.Build();
            if (connectionStringConfig != null)
            {
                return new Tuple<string, int, string, bool>(connectionStringConfig.GetSection("AppSettings").GetSection("ISCHAT").Value,
                    Convert.ToInt32(connectionStringConfig.GetSection("AppSettings").GetSection("GroupID").Value),
                     connectionStringConfig.GetSection("AppSettings").GetSection("Version").Value, Convert.ToBoolean(connectionStringConfig.GetSection("AppSettings").GetSection("ISPROFILE_SEARCH").Value));
            }

            return new Tuple<string, int, string, bool>("", 0, "", false);
        }


        public byte[] EncryptionHMAC(string strValue)
        {
            //, out byte[] passwordSalt
            byte[] passwordHash;
            if (strValue == null) throw new ArgumentNullException("Encryption");
            if (string.IsNullOrWhiteSpace(strValue)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "Encryption");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                //passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(strValue));
            }
            return passwordHash;
        }

        public string DecryptionHMAC(byte[] storedHash)
        {
            //, out byte[] passwordSalt
            //if (strValue == null) throw new ArgumentNullException("Encryption");
            // if (string.IsNullOrWhiteSpace(strValue)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "Encryption");
            //if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "EncryptionHash");

            string sbinary = "";

            for (int i = 0; i < storedHash.Length; i++)
            {
                sbinary += storedHash[i].ToString("X2"); // hex format
            }
            return (sbinary);



        }

        public static string EncryptString(string text)
        {
            string keyString = "E546C8DF278CD5931069B522E695D4F2";
            var key = Encoding.UTF8.GetBytes(keyString);

            using (var aesAlg = Aes.Create())
            {
                using (var encryptor = aesAlg.CreateEncryptor(key, aesAlg.IV))
                {
                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(text);
                        }

                        var iv = aesAlg.IV;

                        var decryptedContent = msEncrypt.ToArray();

                        var result = new byte[iv.Length + decryptedContent.Length];

                        Buffer.BlockCopy(iv, 0, result, 0, iv.Length);
                        Buffer.BlockCopy(decryptedContent, 0, result, iv.Length, decryptedContent.Length);

                        return Convert.ToBase64String(result);
                    }
                }
            }
        }

        public static string DecryptString(string cipherText)
        {
            string keyString = "E546C8DF278CD5931069B522E695D4F2";
            var fullCipher = Convert.FromBase64String(cipherText);

            var iv = new byte[16];
            var cipher = new byte[fullCipher.Length - iv.Length];

            Buffer.BlockCopy(fullCipher, 0, iv, 0, iv.Length);
            Buffer.BlockCopy(fullCipher, iv.Length, cipher, 0, fullCipher.Length - iv.Length);
            var key = Encoding.UTF8.GetBytes(keyString);

            using (var aesAlg = Aes.Create())
            {
                using (var decryptor = aesAlg.CreateDecryptor(key, iv))
                {
                    string result;
                    using (var msDecrypt = new MemoryStream(cipher))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                result = srDecrypt.ReadToEnd();
                            }
                        }
                    }

                    return result;
                }
            }
        }

        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.  
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }

            // Declare the string used to hold  
            // the decrypted text.  
            string plaintext = null;

            // Create an RijndaelManaged object  
            // with the specified key and IV.  
            using (var rijAlg = new RijndaelManaged())
            {
                //Settings  
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.  
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                try
                {
                    // Create the streams used for decryption.  
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {

                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream  
                                // and place them in a string.  
                                plaintext = srDecrypt.ReadToEnd();

                            }

                        }
                    }
                }
                catch
                {
                    plaintext = "keyError";
                }
            }

            return plaintext;
        }

        private static byte[] EncryptStringToBytes(string plainText, byte[] key, byte[] iv)
        {
            // Check arguments.  
            if (plainText == null || plainText.Length <= 0)
            {
                throw new ArgumentNullException("plainText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            byte[] encrypted;
            // Create a RijndaelManaged object  
            // with the specified key and IV.  
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.  
                var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption.  
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.  
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            // Return the encrypted bytes from the memory stream.  
            return encrypted;
        }

        public static string DecryptStringAES(string cipherText)
        {
            cipherText = (!string.IsNullOrEmpty(cipherText)) ? cipherText.Replace("~", "/") : cipherText;
            var keybytes = Encoding.UTF8.GetBytes("8080808080808080");
            var iv = Encoding.UTF8.GetBytes("8080808080808080");

            var encrypted = Convert.FromBase64String(cipherText);
            var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);
            return string.Format(decriptedFromJavascript);
        }

        public static string EncryptStringAES(string cipherText)
        {
            var keybytes = Encoding.UTF8.GetBytes("8080808080808080");
            var iv = Encoding.UTF8.GetBytes("8080808080808080");

            // var encrypted = Convert.FromBase64String(cipherText);
            var decriptedFromJavascript = EncryptStringToBytes(cipherText, keybytes, iv);
            return Convert.ToBase64String(decriptedFromJavascript);//.Replace("%2F", "~").Replace("/","~");
        }
        // authentication successful so generate jwt token
        public string GetJWTSecurityToken(string userId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(GetSecratValue());
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, userId.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }


        public static string GetSecratValue()
        {
            var builder = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json");

            var connectionStringConfig = builder.Build();
            if (connectionStringConfig != null)
                return connectionStringConfig.GetSection("AppSettings").GetSection("Secret").Value;
            else
                return "imeetingX$@18201687~";

          

        }
        /// <summary>
        /// Escape a string for usage in an LDAP DN to prevent LDAP injection attacks.
        /// There are certain characters that are considered special characters in a DN.
        /// The exhaustive list is the following: ',','\','#','+','<','>',';','"','=', and leading or trailing spaces
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string EscapeForDN(string name)
        {
            StringBuilder sb = new StringBuilder();

            //if (name.Length > 0 && ((name[0] == ' ') || (name[0] == '#')))
            //{
            //    sb.Append('\\'); // add the leading backslash if needed
            //}

            for (int i = 0; i < name.Length; i++)
            {
                char curChar = name[i];
                switch (curChar)
                {
                    case ',':
                        sb.Append(@"\,");
                        break;
                    case '+':
                        sb.Append(@"\+");
                        break;
                    case '"':
                        sb.Append("\\\"");
                        break;
                    case '<':
                        sb.Append(@"\<");
                        break;
                    case '>':
                        sb.Append(@"\>");
                        break;
                    case ';':
                        sb.Append(@"\;");
                        break;
                    default:
                        sb.Append(curChar);
                        break;
                }
            }

            //if (name.Length > 1 && name[name.Length - 1] == ' ')
            //{
            //    sb.Insert(sb.Length - 1, '\\'); // add the trailing backslash if needed
            //}

            return sb.ToString();
        }

        /// <summary>
        /// Escape a string for usage in an LDAP DN to prevent LDAP injection attacks.
        /// </summary>
        public static string EscapeForSearchFilter(string filter)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < filter.Length; i++)
            {
                char curChar = filter[i];
                switch (curChar)
                {
                    case '\\':
                        sb.Append("\\5c");
                        break;
                    case '*':
                        sb.Append("\\2a");
                        break;
                    case '(':
                        sb.Append("\\28");
                        break;
                    case ')':
                        sb.Append("\\29");
                        break;
                    case '\u0000':
                        sb.Append("\\00");
                        break;
                    default:
                        sb.Append(curChar);
                        break;
                }
            }
            return sb.ToString();
        }

        public static string GetFileContentType(string ext)
        {

            string contentType = "";
            switch (ext)
            {
                case ".txt":
                    contentType = "text/plain";
                    break;
                case ".doc":
                case ".dot":
                    contentType = "application/msword";
                    break;
                case ".docx":
                    contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    break;
                case ".dotx":
                    contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.template";
                    break;
                case ".xls":
                case ".xlt":
                case ".xla":
                    contentType = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    break;
                case ".xltx":
                    contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
                    break;
                case ".ppt":
                case ".pot":
                case ".pps":
                case ".ppa":
                    contentType = "application/vnd.ms-powerpoint";
                    break;
                case ".pptx":
                    contentType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                    break;
                case ".potx":
                    contentType = "application/vnd.openxmlformats-officedocument.presentationml.template";
                    break;
                case ".mdb":
                    contentType = "application/vnd.ms-access";
                    break;
                case ".pdf":
                    contentType = "application/pdf";
                    break;
                case ".gif":
                    contentType = "image/gif";
                    break;
                case ".jpeg":
                    contentType = "image/jpeg";
                    break;
                case ".jpg":
                    contentType = "image/jpeg";
                    break;
                case ".png":
                    contentType = "image/png";
                    break;
                case ".avi":
                    contentType = "video/x-msvideo";
                    break;
                case ".mp4":
                    contentType = "video/mpeg";
                    break;
                case ".mpeg":
                    contentType = "video/mpeg";
                    break;
                case ".mpg":
                    contentType = "video/mpeg";
                    break;
            }
            return contentType;
        }

    }
    public class AppSettings
    {
        public string Secret { get; set; }
        public string EmailFromAddress { get; set; }
        public string EmailHost { get; set; }
        public string EmailPort { get; set; }
        public bool EmailSSL { get; set; }
        public string EmailUserName { get; set; }
        public string EmailPassword { get; set; }
        public string EmailExchangeURL { get; set; }
        public bool IsWindows_Authendication { get; set; }
        public string Windows_UserName { get; set; }
        public string Windows_Password { get; set; }
        public string Windows_Domain { get; set; }
        public string SiteURL { get; set; }

    }

}

