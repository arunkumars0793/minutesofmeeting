﻿using Microsoft.AspNetCore.Hosting;
using MinutesOfMeeting.Models.DAL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MinutesOfMeeting.Models.Helpers
{
    
    public class ErrorLog
    {
        
        public static void LogFileCreation(IWebHostEnvironment _env, Exception ex)
        {
            string ErrorMessage = string.Empty;
            StackTrace stackTrace = new StackTrace();
            StackFrame stackFrame = stackTrace.GetFrame(1);
            MethodBase methodBase = stackFrame.GetMethod();

            if (stackFrame != null && methodBase != null && ex.StackTrace != null && ex.Message != null)
            {
                ErrorMessage = "MethodName :" + methodBase.Name + Environment.NewLine + "Message :" + ex.Message;
            }
            else
            {
                ErrorMessage = ex.Message;
            }

            SqlDataProviderBiz DataModel = new SqlDataProviderBiz();
            DataModel._Errorlog = _env;
            DataModel.InsertLog(ErrorMessage, "ErrorLog");
        }


        
        public static void DataLogFileCreation(IWebHostEnvironment _env, string message)
        {

            try
            {
               
                CommonClass comClass = new CommonClass();
                if (comClass.EnableDataLog())
                {
                    SqlDataProviderBiz DataModel = new SqlDataProviderBiz();
                    DataModel._Errorlog = _env;
                    DataModel.InsertLog(message, "DataLog");
                }

            }
            catch (Exception)
            {
                
            }



        }



        public static void ErrorLogFile(IWebHostEnvironment _env, Exception ex)
        {

            try
            {
                CommonClass comClass = new CommonClass();
                string LogFile_RootPath = comClass.GetErrorLogPath(); 
                if (_env != null)
                {
                    var webRoot = _env.WebRootPath;
                    string LogFile_Path = LogFile_RootPath
                                       + Path.DirectorySeparatorChar.ToString()
                                       + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + ".txt";


                    using (StreamWriter writer = new StreamWriter(LogFile_Path, true))
                    {
                        StackTrace stackTrace = new StackTrace();
                        StackFrame stackFrame = stackTrace.GetFrame(1);
                        MethodBase methodBase = stackFrame.GetMethod();
                        writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);

                        if (stackFrame != null && methodBase != null && ex.StackTrace != null && ex.Message != null)
                        {
                            writer.WriteLine("MethodName :" + methodBase.Name + Environment.NewLine + "Message :" + ex.Message +
                               "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                        }
                        else
                        {
                            writer.WriteLine(Environment.NewLine + "Message :" + ex.Message + "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                        }
                        writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                    }
                }

            }
            catch (Exception)
            {

            }



        }

        public static void DataLogFile(IWebHostEnvironment _env, string message)
        {

            try
            {

                CommonClass comClass = new CommonClass();
                if (comClass.EnableDataLog())
                {
                    string LogFile_RootPath = comClass.GetDataLogPath();
                    if (_env != null)
                    {
                        var webRoot = _env.WebRootPath;
                        string LogFile_Path = LogFile_RootPath
                                           + Path.DirectorySeparatorChar.ToString()
                                           + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + ".txt";


                        using (StreamWriter writer = new StreamWriter(LogFile_Path, true))
                        {

                            writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                            writer.WriteLine(Environment.NewLine + "Message :" + message + "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                            writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                        }
                    }

                }

            }
            catch (Exception ex)
            {

            }



        }

        public static string GetLogString(Exception exception, string environmentStackTrace)
        {
            List<string> environmentStackTraceLines = GetUserStackTraceLines(environmentStackTrace);
            environmentStackTraceLines.RemoveAt(0);

            List<string> stackTraceLines = GetStackTraceLines(exception.StackTrace);
            stackTraceLines.AddRange(environmentStackTraceLines);

            string fullStackTrace = String.Join(Environment.NewLine, stackTraceLines);

            string logMessage = exception.Message + Environment.NewLine + fullStackTrace;
            return logMessage;
        }

        /// <summary>
        ///  Gets a list of stack frame lines, as strings.
        /// </summary>
        /// <param name="stackTrace">Stack trace string.</param>
        private static List<string> GetStackTraceLines(string stackTrace)
        {
            return stackTrace.Split(new[] { Environment.NewLine }, StringSplitOptions.None).ToList();
        }

        /// <summary>
        ///  Gets a list of stack frame lines, as strings, only including those for which line number is known.
        /// </summary>
        /// <param name="fullStackTrace">Full stack trace, including external code.</param>
        private static List<string> GetUserStackTraceLines(string fullStackTrace)
        {
            List<string> outputList = new List<string>();
            Regex regex = new Regex(@"([^\)]*\)) in (.*):line (\d)*$");

            List<string> stackTraceLines = GetStackTraceLines(fullStackTrace);
            foreach (string stackTraceLine in stackTraceLines)
            {
                if (!regex.IsMatch(stackTraceLine))
                {
                    continue;
                }

                outputList.Add(stackTraceLine);
            }

            return outputList;
        }

        //private static string GetErrorLogPath()
        //{
        //    var builder = new ConfigurationBuilder()
        //   .SetBasePath(Directory.GetCurrentDirectory())
        //   .AddJsonFile("appsettings.json");

        //    var connectionStringConfig = builder.Build();
        //    if (connectionStringConfig != null)
        //        return connectionStringConfig.GetConnectionString("ErrorLog");
        //    else
        //        return string.Empty;

        //}

    }
}
