﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinutesOfMeeting.Models.Helpers
{


    public class AgendaDetails
    {
        public int AgendaUno { get; set; }
        public int AgendaParentUno { get; set; }
        public int? GenInfoAtdUno { get; set; }
        public string Note { get; set; }
        public string Responsibility { get; set; }
        public string ResponsibilityName { get; set; }
        public string DueDate { get; set; }
        public string UserName { get; set; }
        public int? IsActive { get; set; }
        public int? UserID { get; set; }
        public int? StartIndex { get; set; }
        public int? MaxCount { get; set; }
        public bool? IsDueDate { get; set; }
        public bool? IReps { get; set; }
        public string ActionType { get; set; }
        public bool? Expanded { get; set; }
    }


    public class Attendees
    {
        public int AttendeesUno { get; set; }
        public int userselUno { get; set; }
        public string userselName { get; set; }

        public int AtdCatgorizeUno { get; set; }
        public string AtdCatgorizeName { get; set; }

        public int roleUno { get; set; }
        public string roleName { get; set; }

        public string Name { get; set; }
        public string Company { get; set; }
        public int Contact { get; set; }
        public string email { get; set; }
        public string PrNumber { get; set; }

        public int UserUno { get; set; }
        public int LanguageUno { get; set; }

        public int IsActive { get; set; }
        public int EnteredBy { get; set; }
        public DateTime EnteredOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public string XMLData { get; set; }
        public string Icon { get; set; }
        public string Type { get; set; }
    }
    public class GenInfoAtdParam
    {
        public int? UserUno { get; set; }
        public int? RecordType { get; set; }

        public string ActionType { get; set; }

        public int? GenInfoAtdUno { get; set; }
        public string pdfContent { get; set; }
        public string MomNoAtd { get; set; }
    }

    public class DivisionReport
    {
        public string UserName { get; set; }
        public string DivisionName { get; set; }
        public int RecordCount { get; set; }
    }

    public class cmtObject
    {
        public string CmtUsers { get; set; }
        public string cmtUserName { get; set; }
        public int? CmdId { get; set; }
        public string CmdText { get; set; }
        public string UserName { get; set; }
        public string CmtDate { get; set; }
        public int? GenralInfoId { get; set; }
        public int? UserId { get; set; }
        public int? MaxCount { get; set; }
        public int? StartIndex { get; set; }
        public string ActionType { get; set; }
    }
    public class GenInfo
    {
        public int GenInfoAtdUno { get; set; }

        public int locationUno { get; set; }
        public string locationName { get; set; }
        public int MeetingTypeUno { get; set; }
        public string MeetingName { get; set; }
        public string MeetingCode { get; set; }
        public int MeetingRoomUno { get; set; }
        public string MeetingRoomName { get; set; }
        public string SubjectAtd { get; set; }
        public string MomNoAtd { get; set; }
        public DateTime MeetingDateAtd { get; set; }
        public string FMeetingDateAtd { get; set; }
        public DateTime StartTimeAtd { get; set; }
        public string FStartTimeAtd { get; set; }
        public DateTime EndTimeAtd { get; set; }
        public string FEndTimeAtd { get; set; }
        public int UserUno { get; set; }
        public string CallFrom { get; set; }
        public int LanguageUno { get; set; }
        public int Published { get; set; }
        public int Condition { get; set; }
        public int IsActive { get; set; }
        public int EnteredBy { get; set; }
        public DateTime EnteredOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public int? ReviewerId { get; set; }
        public int? ApproverId { get; set; }
        public string CreaterSign { get; set; }
        public string ApproverSign { get; set; }
        public string ReviewerSign { get; set; }
        public string ApprovalDate { get; set; }
        public string ReviewDate { get; set; }
        public string InitiatedBy { get; set; }
        public string CreateDate { get; set; }
        public string ApprovalStatus { get; set; }
        public string NumberOfAttendees { get; set; }
    }
    public class AttendeesAtd
    {
        public int AttendeesUnoAtd { get; set; }
        public int GenInfoAtdUno { get; set; }
        public string PrNumberAtd { get; set; }
        public string Status { get; set; }
        public string Role { get; set; }
        public string NameAtd { get; set; }
        public string CompanyAtd { get; set; }
        public string emailAtd { get; set; }
        public string ContactAtd { get; set; }
        public int UserselUno { get; set; }
        public int UserUno { get; set; }
        public string SignImage { get; set; }
        public string ActionType { get; set; }
        public int? MaxCount { get; set; }
        public int? StartIndex { get; set; }
        public string UpdateType { get; set; }
        public string FullAttendeeName { get; set; }
        public string ApologizedReason { get; set; }

    }
    public class FileAttachement
    {
        public int? AttachmentUno { get; set; }
        public int? GeneralInfoId { get; set; }
        public int? UserId { get; set; }
        public string ActionType { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string CreateByName { get; set; }
        public string CreateDate { get; set; }

    }
    public class GenInfoAtdInsert
    {
        public int? GenInfoAtdUno { get; set; }
        public int? GenInfoRefAtdId { get; set; }
        public int? locationUno { get; set; }
        public int? MeetingTypeUno { get; set; }
        public string MeetingName { get; set; }
        public int? MeetingRoomUno { get; set; }
        public string MeetingRoomName { get; set; }
        public string SubjectAtd { get; set; }
        public string MomNoAtd { get; set; }
        public DateTime MeetingDateAtd { get; set; }
        public string FMeetingDateAtd { get; set; }
        public DateTime StartTimeAtd { get; set; }
        public string FStartTimeAtd { get; set; }
        public DateTime EndTimeAtd { get; set; }
        public string FEndTimeAtd { get; set; }
        public int? UserUno { get; set; }
        public int LanguageUno { get; set; }
        public int? ReviewerId { get; set; }
        public int? ApproverId { get; set; }
        public string CreaterSign { get; set; }
        public string ApproverSign { get; set; }
        public string ReviewerSign { get; set; }
        public string CallFrom { get; set; }
        public string ActionType { get; set; }
        public string pdfContent { get; set; }

    }
    public class MOMDashboard
    {
        public double? NoOfDraftMeeting { get; set; }
        public double? NoOfNotPublishMeeting { get; set; }
        public double? NoOfPublishedMeeting { get; set; }
        public int? UserUno { get; set; }
        public int? NoOfMyMeetings { get; set; }
        public int? NoOfMyAttendanceSheets { get; set; }
        public int? NoOfPartMeetings { get; set; }
        public int? NoOfMyTasks { get; set; }
        public int? NoOfMyTBTSessions { get; set; }
        public string SearchText { get; set; }
        public string ActionType { get; set; }

        public string Startdt { get; set; }
        public string Enddt { get; set; }
        public List<MOMDashboard_MyMeetings> LstMOMDashboard_MyMeetings { get; set; }
        public List<MOMDashboard_PartofMeetings> LstMOMDashboard_PartofMeetings { get; set; }
        public List<MOMDashboard_MyMeetings> lstMOMDashboard_TodayMeetings { get; set; }


    }

    public class MOMDashboard_MyMeetings
    {
        public int? GenInfoAtdUno { get; set; }
        public string MoMNoAtd { get; set; }
        public string SubjectAtd { get; set; }
        public string MeetingDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string ApprovalStatus { get; set; }
        public string MeetingName { get; set; }
        public string MeetingLocation { get; set; }
        public string MeetingRoom { get; set; }
    }
    public class MOMDashboard_PartofMeetings
    {
        public string MeetingName { get; set; }
        public int? GenInfoAtdUno { get; set; }
        public string MoMNoAtd { get; set; }
        public string SubjectAtd { get; set; }
        public string MeetingDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string ApprovalStatus { get; set; }
        public string MeetingLocation { get; set; }
        public string MeetingRoom { get; set; }
    }
    public class OutlookMeeting
    {
        public string Email { get; set; }
        public string Subject { get; set; }
        public string BodyText { get; set; }
        public string UserName { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public string MeetingDate { get; set; }
        public string Location { get; set; }
        public double DayCount { get; set; }
        public string ApptId { get; set; }
        public List<OutlookAttendee> ListAttendee { get; set; } = new List<OutlookAttendee>();
        public List<OutlookFileList> FileList { get; set; } = new List<OutlookFileList>();
        public List<OutlookAgenda> AgendaList { get; set; } = new List<OutlookAgenda>();
    }

    public class OutlookAttendee
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string PRN { get; set; }
        public int? UserUno { get; set; }
        public bool Active { get; set; }
    }


    public class OutlookFileList
    {
        public string FileName { get; set; }
        public string Id { get; set; }
        public bool Active { get; set; }
    }

    public class OutlookAgenda
    {
        public string Agenda { get; set; }
        public bool Active { get; set; }
    }

    public class OutlookFile
    {
        public string Id { get; set; }
        public string FileName { get; set; }
        public string Email { get; set; }
        public int DayCount { get; set; }
    }
    public class EmailMeetingInfo
    {
        public string ReviewerName { get; set; }
        public int GenInfoAtdUno { get; set; }
        public string locationName { get; set; }
        public string MeetingName { get; set; }
        public string MeetingRoomName { get; set; }
        public string SubjectAtd { get; set; }
        public string MomNoAtd { get; set; }
        public string MeetingDateAtd { get; set; }
        public string StartTimeAtd { get; set; }
        public string EndTimeAtd { get; set; }
        public string CreatedName { get; set; }
        public string ApprovedName { get; set; }
        public string Email { get; set; }
        public List<AgendaDetails> ListAgenta { get; set; } = new List<AgendaDetails>();
        public List<AttendeesAtd> ListAttendee { get; set; } = new List<AttendeesAtd>();
    }

    public class OutlookAttendeeInsert
    {
        public int GenInfoUno { get; set; }
        public int UserUno { get; set; }
        public string AttendeeJSONString { get; set; }
        public string FileJSONString { get; set; }
        public string AgendaJSONString { get; set; }

        public string ActionType { get; set; }

    }

    public class Alert
    {
        public int? UserUno { get; set; }
        public int? Id { get; set; }
        public int? GenInfoUno { get; set; }
        public bool? IsVisited { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public int Condition { get; set; }
        public int? MaxCount { get; set; }
        public int? StartIndex { get; set; }
    }
}
