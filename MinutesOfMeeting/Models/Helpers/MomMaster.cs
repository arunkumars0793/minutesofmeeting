﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinutesOfMeeting.Models.Helpers
{
   
    public class MeetingType
    {
        public int MeetingTypeUno { get; set; }
        public string MeetingTypecode { get; set; }
        public string MeetingName { get; set; }
        public int LanguageUno { get; set; }        
        public int UserUno { get; set; }    

    }
    public class MeetingLocation
    {
        public int LocationUno { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public int UserUno { get; set; }
        public int? LanguageUno { get; set; }
        
     
    }
    public class MeetingRoom
    {
        public int MeetingRoomUno { get; set; }
        public string MeetingRoomCode { get; set; }
        public string MeetingRoomName { get; set; }
        public int locationUno { get; set; }
        public string locationName { get; set; }
        public int IsActive { get; set; }
        public string EnteredBy { get; set; }
        public DateTime EnteredOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public string Flag { get; set; }
        //filter
        public int UserUno { get; set; }

        public int LanguageUno { get; set; }

        public string XMLData { get; set; }
        public string Icon { get; set; }
        public string Type { get; set; }
    }

 
}

 