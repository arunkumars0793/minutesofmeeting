using HtmlAgilityPack;
using Microsoft.AspNetCore.Hosting;
using MinutesOfMeeting.Models.BAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace MinutesOfMeeting.Models.Helpers
{
    public static class EmailHelper
    {
        private static BusinessLayer ObjBusiness = new BusinessLayer();
        public static GenInfoAtdParam GenInfoObj { get; set; }
        public static string _EmailTemplatePath { get; set; }
        public static IWebHostEnvironment _env { get; set; }
        public static string _apppath { get; set; }
        public static string _pdf { get; set; }

        public static string MomUno { get; set; }
        public static string toEmailId { get; set; }

        #region"Attend && TBD" 
        public static async Task<string> SendInviteEmail()
        {
            try
            {
                _EmailTemplatePath = GetTemplatePath("attendee", _env);
                await Task.Run(() => CreateEmail());

            }
            catch (Exception)
            {
                return "failure";
            }
            return "success";
        }


        #endregion


        #region"Meeting" 
        public static async Task<string> SendMeetingInviteEmail()
        {
            try
            {
                _EmailTemplatePath = GetTemplatePath("meeting-invite", _env);
                await Task.Run(() => CreateEmail());


            }
            catch (Exception)
            {
                return "failure";
            }
            return "success";
        }

        public static async Task<string> SendMeetingApproveEmail()
        {
            try
            {
                //_EmailTemplatePath = GetTemplatePath("approve", _env);
                //await Task.Run(() => CreateEmail());
                await SendMeetingInviteEmail();
            }
            catch (Exception)
            {
                return "failure";
            }
            return "success";
        }

        public static async Task<string> SendMeetingReviewEmail()
        {
            try
            {
                _EmailTemplatePath = GetTemplatePath("review", _env);
                await Task.Run(() => CreateEmail());


            }
            catch (Exception)
            {
                return "failure";
            }
            return "success";
        }
        #endregion

        public static void CreateEmail()
        {
            ErrorLog.DataLogFileCreation(_env, " Mail is started.. \n" + _EmailTemplatePath);
            toEmailId = string.Empty;
            var AttendeeList = string.Empty;
            var AgentaList = string.Empty;
            CommonClass objCommonClass = new CommonClass();
            AppSettings appSettings = objCommonClass.GetEmailHostSetting();
            _apppath = appSettings.SiteURL;
            var EmailListObj = ObjBusiness.GetEmailMeetingInfo(GenInfoObj);
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(_EmailTemplatePath))
            {
                body = reader.ReadToEnd();
            }
            foreach (var item in EmailListObj.ListAttendee)
            {
                if (!string.IsNullOrEmpty(item.NameAtd))
                    AttendeeList += "<li>" + item.NameAtd + "</li>";
                if (!string.IsNullOrEmpty(item.emailAtd))
                    toEmailId += item.emailAtd + ";";
            }

            foreach (var item in EmailListObj.ListAgenta)
            {
                AttendeeList += "<li>" + item.Note + "</li>";
                if (!string.IsNullOrEmpty(item.Note))
                    AgentaList += item.Note + ";";
            }
            body = body.Replace("{{MeetingSubject}}", EmailListObj.SubjectAtd);
            body = body.Replace("{{Approver}}", EmailListObj.ApprovedName);
            body = body.Replace("{{Reviewer}}", EmailListObj.ReviewerName);
            body = body.Replace("{{MeetingName}}", EmailListObj.MeetingName);
            body = body.Replace("{{Name}}", EmailListObj.CreatedName);
            body = body.Replace("{{AgentaList}}", AgentaList);
            body = body.Replace("{{Date}}", EmailListObj.MeetingDateAtd);
            body = body.Replace("{{Type}}", GenInfoObj.ActionType == "TBD" ? "Tool Box Talk" : GenInfoObj.ActionType == "ATTENDANCE" ? "Digital Attendence" : "Minutes of Meeting");
            body = body.Replace("{{Time}}", EmailListObj.StartTimeAtd + " - " + EmailListObj.EndTimeAtd);
            body = body.Replace("{{Location}}", EmailListObj.MeetingRoomName + ", " + EmailListObj.locationName);
            body = body.Replace("{{AttendeeList}}", AttendeeList);
            body = body.Replace("{{MLocation}}", EmailListObj.locationName);
            var URL = "/#/login?QMId=" + CommonClass.EncryptStringAES(EmailListObj.GenInfoAtdUno.ToString()) + "&QMType=" + CommonClass.EncryptStringAES(GenInfoObj.ActionType);
            body = body.Replace("{{URL}}", !_apppath.ToLower().Contains("qa") ? _apppath + URL : _apppath + "/imeetingx"+ URL);
            if (toEmailId != string.Empty)
            {
                SendHtmlFormattedEmail(EmailListObj.MeetingName + " : " + EmailListObj.SubjectAtd, body, toEmailId, EmailListObj.Email);
            }
            ErrorLog.DataLogFileCreation(_env, EmailListObj.MeetingName + " Mail format is completed \n EmailTo " + toEmailId);
        }


        private static string GetTemplatePath(string templateType, IWebHostEnvironment _env)
        {
            var webRoot = _env.WebRootPath;
            string pathToFile = _env.WebRootPath
                          + Path.DirectorySeparatorChar.ToString()
                          + "assets"
                          + Path.DirectorySeparatorChar.ToString()
                          + "EmailTemplates"
                          + Path.DirectorySeparatorChar.ToString();

            switch (templateType)
            {
                case "approve":
                    {
                        pathToFile += Path.DirectorySeparatorChar.ToString() + "approved-email.html";
                        break;
                    }
                case "attendee":
                    {
                        pathToFile += Path.DirectorySeparatorChar.ToString() + "attendee-invite.html";
                        break;
                    }

                case "meeting-invite":
                    {
                        pathToFile += Path.DirectorySeparatorChar.ToString() + "meeting-invite.html";
                        break;
                    }
                case "review":
                    {
                        pathToFile += Path.DirectorySeparatorChar.ToString() + "reviewed-email.html";
                        break;
                    }

            }
            return pathToFile;
        }
        private static void SendHtmlFormattedEmail(string subject, string body, string toAddress,string toCC)
        {
            try
            {
                //toAddress = "sureshkumara@salzertechnologies.com";
                CommonClass obCommon = new CommonClass();
                AppSettings objAppsettings = obCommon.GetEmailHostSetting();
                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.From = new MailAddress(objAppsettings.EmailFromAddress);
                    mailMessage.Subject = subject;
                    mailMessage.Body = body;
                    mailMessage.IsBodyHtml = true;
                    if (toAddress.Contains(";"))
                    {
                        foreach (var address in toAddress.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            mailMessage.To.Add(address);
                        }
                    }
                    else
                        mailMessage.To.Add(new MailAddress(toAddress));

                    mailMessage.CC.Add(new MailAddress(toCC));
                    //For image display
                    string imgPath_emilimg = _env.WebRootPath + Path.DirectorySeparatorChar.ToString() + "assets" + Path.DirectorySeparatorChar.ToString() + "img" + Path.DirectorySeparatorChar.ToString();
                    var inlinemainLogo = new LinkedResource(imgPath_emilimg + "dewa-email-logo.png", "image/png");
                    inlinemainLogo.ContentId = "img_mainlogo";
                    var inlineLogo_s1 = new LinkedResource(imgPath_emilimg + "s1.png", "image/png");
                    inlineLogo_s1.ContentId = "img_s1";
                    var inlineLogo_s2 = new LinkedResource(imgPath_emilimg + "s2.png", "image/png");
                    inlineLogo_s2.ContentId = "img_s2";
                    var inlineLogo_s3 = new LinkedResource(imgPath_emilimg + "s3.png", "image/png");
                    inlineLogo_s3.ContentId = "img_s3";
                    var inlineLogo_s4 = new LinkedResource(imgPath_emilimg + "s4.png", "image/png");
                    inlineLogo_s4.ContentId = "img_s4";
                    var inlineLogo_header = new LinkedResource(imgPath_emilimg + "header-image-email.png", "image/png");
                    inlineLogo_header.ContentId = "img_header";
                    var inlineLogo_banner = new LinkedResource(imgPath_emilimg + "email-banner.png", "image/png");
                    inlineLogo_banner.ContentId = "img_banner";
                    var view = AlternateView.CreateAlternateViewFromString(body, Encoding.UTF8, "text/html");
                    view.LinkedResources.Add(inlinemainLogo);
                    view.LinkedResources.Add(inlineLogo_s1);
                    view.LinkedResources.Add(inlineLogo_s2);
                    view.LinkedResources.Add(inlineLogo_s3);
                    view.LinkedResources.Add(inlineLogo_s4);
                    view.LinkedResources.Add(inlineLogo_header);
                    view.LinkedResources.Add(inlineLogo_banner);
                    mailMessage.AlternateViews.Add(view);

                    if (!string.IsNullOrEmpty(_pdf))
                    {
                        var bytes = Convert.FromBase64String(_pdf);
                        MemoryStream strm = new MemoryStream(bytes);
                        Attachment data = new Attachment(strm, MomUno + ".pdf");
                        ContentDisposition disposition = data.ContentDisposition;
                        data.ContentId = GenInfoObj.GenInfoAtdUno.ToString();
                        data.ContentDisposition.Inline = true;
                        mailMessage.Attachments.Add(data);
                    }
                    //Setting SMTP
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = objAppsettings.EmailHost;
                    smtp.EnableSsl = objAppsettings.EmailSSL;
                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                    NetworkCred.UserName = objAppsettings.EmailUserName;
                    NetworkCred.Password = objAppsettings.EmailPassword;
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = Convert.ToInt32(objAppsettings.EmailPort);
                    smtp.Send(mailMessage);
                    ErrorLog.DataLogFileCreation(_env, subject + " Mail was sent.");
                }
            }
            catch (SmtpException exc)
            {
                ErrorLog.LogFileCreation(_env, exc);
            }
            catch (InvalidOperationException exc)
            {
                ErrorLog.LogFileCreation(_env, exc);
            }
            catch (Exception exc)
            {
                ErrorLog.LogFileCreation(_env, exc);
            }
        }
    }
}
