﻿using Microsoft.ApplicationBlocks.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Exchange.WebServices.Data;
using Microsoft.Extensions.Configuration;
using MinutesOfMeeting.Models.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using static MinutesOfMeeting.Models.Helpers.Admin;

namespace MinutesOfMeeting.Models.DAL
{
    public class SqlDataProviderBiz
    {
        public SqlDataProviderBiz(IConfiguration configuration, IWebHostEnvironment _env)
        {
            Configuration = configuration;
            //_Errorlog = _env;
        }


        public IConfiguration Configuration { get; }
        private CommonClass objCommonClass = new CommonClass();
        private static String _connectionString;
        public IWebHostEnvironment _Errorlog { get; set; }


        #region[:: Connection String::]
        #region[::Constructor Implementation::]
        public SqlDataProviderBiz()
        {
            if (_connectionString == null)
            {
                _connectionString = objCommonClass.GetConnectionStringName();
            }
        }
        #endregion

        public string connectionDB()
        {
            _connectionString = objCommonClass.GetConnectionStringName();
            return _connectionString;
        }
        #endregion

        #region Properties
        public static String ConnectionString
        {
            get { return _connectionString; }
        }
        #endregion

        #region Login

        public DataTable GetEmailContactInfo_ByUserID(Users obj)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@UserUno", obj.UserUno),
                 new SqlParameter("@TenantUno", obj.TenantUno)

            };
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, "spGetMstUserEmail", parameters.ToArray());
                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return dt;
        }

        public DataTable GetUserInfo_ByUserID(Users obj)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@UserUno", obj.UserUno),
                new SqlParameter("@NTLogin", obj.UserName),
                new SqlParameter("@LanguageUno", obj.LanguageUno),
                new SqlParameter("@Condition", 4)

            };
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, "spGetMasterUser", parameters.ToArray());
                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return dt;
        }
        #endregion

        #region Meeting Type

        public DataTable GetMeetingType(MeetingType obj)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {

                new SqlParameter("@UserUno", obj.UserUno),
                new SqlParameter("@LanguageUno", obj.LanguageUno)

            };
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, "spGetMasterMeetingType", parameters.ToArray());
                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return dt;
        }
        public string MeetingType_Insert_Update(MeetingType KPI)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@MeetingTypeUno", KPI.MeetingTypeUno),
                    new SqlParameter("@MeetingTypecode", KPI.MeetingTypecode),
                    new SqlParameter("@MeetingName", KPI.MeetingName),
                    new SqlParameter("@UserUno", KPI.UserUno),
                    new SqlParameter("@Condititon", 1)
                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[5].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[6].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spInsertUpdateMasterMeetingType", parameters.ToArray());
                int rtnParameter = (parameters[5].Value != null) ? Convert.ToInt32(parameters[5].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = (parameters[6].Value != null) ? parameters[6].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }
        public string MeetingType_Delete(MeetingType KPI)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@MeetingTypeUno", KPI.MeetingTypeUno),
                    new SqlParameter("@MeetingTypecode", KPI.MeetingTypecode),
                    new SqlParameter("@MeetingName", KPI.MeetingName),
                    new SqlParameter("@UserUno", KPI.UserUno),
                    new SqlParameter("@Condititon", 2)

                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[5].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[6].Direction = ParameterDirection.Output;
                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spInsertUpdateMasterMeetingType", parameters.ToArray());
                int rtnParameter = (parameters[5].Value != null) ? Convert.ToInt32(parameters[5].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = (parameters[6].Value != null) ? parameters[6].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }

        #endregion

        #region Location Master      
        public DataTable GetLocation(MeetingLocation obj)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {

                new SqlParameter("@UserUno", obj.UserUno),
                new SqlParameter("@LanguageUno", obj.LanguageUno)

            };
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, "spGetMasterMeetingLocation", parameters.ToArray());
                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return dt;
        }

        public string Location_Insert_Update(MeetingLocation KPI)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@locationUno", KPI.LocationUno),
                    new SqlParameter("@locationCode", KPI.LocationCode),
                    new SqlParameter("@locationName", KPI.LocationName),
                    new SqlParameter("@UserUno", KPI.UserUno),
                    new SqlParameter("@Condition", 1)
                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[5].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[6].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spInsertUpdateMasterMeetingLocation", parameters.ToArray());
                int rtnParameter = (parameters[5].Value != null) ? Convert.ToInt32(parameters[5].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = (parameters[6].Value != null) ? parameters[6].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }

        public string Location_Delete(MeetingLocation KPI)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@locationUno", KPI.LocationUno),
                    new SqlParameter("@locationCode", KPI.LocationCode),
                    new SqlParameter("@locationName", KPI.LocationName),
                    new SqlParameter("@UserUno", KPI.UserUno),
                    new SqlParameter("@Condition", 2)
                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[5].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[6].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spInsertUpdateMasterMeetingLocation", parameters.ToArray());
                int rtnParameter = (parameters[5].Value != null) ? Convert.ToInt32(parameters[5].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = (parameters[6].Value != null) ? parameters[6].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }


        #endregion

        #region Meeting Room

        public DataTable GetMeetingRoom(MeetingRoom obj)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@UserUno", obj.UserUno),
                new SqlParameter("@LanguageUno", obj.LanguageUno)

            };
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, "spGetMasterMeetingRoom", parameters.ToArray());
                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return dt;
        }

        public string MeetingRoom_Insert_Update(MeetingRoom KPI)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@MeetingRoomUno", KPI.MeetingRoomUno),
                    new SqlParameter("@MeetingRoomCode", KPI.MeetingRoomCode),
                    new SqlParameter("@MeetingRoomName", KPI.MeetingRoomName),
                    new SqlParameter("@locationUno", KPI.locationUno),
                    new SqlParameter("@UserUno", KPI.UserUno),
                    new SqlParameter("@Condititon", 1),

                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[6].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[7].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spInsertUpdateMasterMeetingRoom", parameters.ToArray());
                int rtnParameter = (parameters[6].Value != null) ? Convert.ToInt32(parameters[6].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = (parameters[7].Value != null) ? parameters[7].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }

        public string MeetingRoom_Delete(MeetingRoom KPI)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@MeetingRoomUno", KPI.MeetingRoomUno),
                    new SqlParameter("@MeetingRoomCode", KPI.MeetingRoomCode),
                    new SqlParameter("@MeetingRoomName", KPI.MeetingRoomName),
                    new SqlParameter("@locationUno", KPI.locationUno),
                    new SqlParameter("@UserUno", KPI.UserUno),
                    new SqlParameter("@Condititon", 2),

                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[6].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[7].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spInsertUpdateMasterMeetingRoom", parameters.ToArray());
                int rtnParameter = (parameters[6].Value != null) ? Convert.ToInt32(parameters[6].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = (parameters[7].Value != null) ? parameters[7].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }


        #endregion

        #region User Master

        public DataTable GetUserMasters(UserMaster obj)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@UserUno", obj.UserUno),
                new SqlParameter("@NTLogin", ""),
                new SqlParameter("@LanguageUno", obj.LanguageUno),
                new SqlParameter("@Condition", obj.Condition),


            };
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, "spGetMasterUser", parameters.ToArray());
                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return dt;
        }

        public string InsertUpdateUserMaster(UserMaster objUser)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@ID", objUser.ID),
                    new SqlParameter("@NTUsername", objUser.NTUsername),
                    new SqlParameter("@DisplayNameEn", objUser.DisplayNameEn),
                    new SqlParameter("@DisplayNameAr", objUser.DisplayNameAr),
                    new SqlParameter("@Firstname", objUser.Firstname),
                    new SqlParameter("@Lastname", objUser.Lastname),
                    new SqlParameter("@EmailId", objUser.EmailId),
                    new SqlParameter("@DesignationEn", objUser.DesignationEn),
                    new SqlParameter("@DesignationAr", objUser.DesignationAr),
                    new SqlParameter("@DivisionUno", objUser.DivisionUno),
                    new SqlParameter("@PrNumber", objUser.PrNumber),
                    new SqlParameter("@EnteredBy", objUser.EnteredBy),
                    new SqlParameter("@UserUno", objUser.UserUno),
                    new SqlParameter("@Condititon", 1),

                };

                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[14].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[15].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spInsertUpdateMasterUser", parameters.ToArray());
                int rtnParameter = (parameters[14].Value != null) ? Convert.ToInt32(parameters[14].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = (parameters[15].Value != null) ? parameters[15].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }


        public string DeleteUserMaster(UserMaster objUser)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@ID", objUser.ID),
                    new SqlParameter("@NTUsername", objUser.NTUsername),
                    new SqlParameter("@DisplayNameEn", objUser.DisplayNameEn),
                    new SqlParameter("@DisplayNameAr", objUser.DisplayNameAr),
                    new SqlParameter("@Firstname", objUser.Firstname),
                    new SqlParameter("@Lastname", objUser.Lastname),
                    new SqlParameter("@EmailId", objUser.EmailId),
                    new SqlParameter("@DesignationEn", objUser.DesignationEn),
                    new SqlParameter("@DesignationAr", objUser.DesignationAr),
                    new SqlParameter("@DivisionUno", objUser.DivisionUno),
                    new SqlParameter("@PrNumber", objUser.PrNumber),
                    new SqlParameter("@EnteredBy", objUser.EnteredBy),
                    new SqlParameter("@UserUno", objUser.UserUno),
                    new SqlParameter("@Condititon", 2),

                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[14].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[15].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spInsertUpdateMasterUser", parameters.ToArray());
                int rtnParameter = (parameters[14].Value != null) ? Convert.ToInt32(parameters[14].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = (parameters[15].Value != null) ? parameters[15].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }



        #endregion

        #region Get Search List
        public DataSet GetSearchList(MOMDashboard obj)
        {

            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@UserUno", obj.UserUno),
                new SqlParameter("@SearchString", obj.SearchText),
                new SqlParameter("@ActionType", obj.ActionType),


            };
            DataSet ds = new DataSet();
            try
            {
                ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, "spGetMeeting_SearchText", parameters.ToArray());

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }
            return ds;
        }

        public DataTable GetDivMeetingList(MOMDashboard obj)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {

                new SqlParameter("@Startdt", obj.Startdt),
                new SqlParameter("@Enddt", obj.Enddt),
                new SqlParameter("@SearchText", obj.SearchText),
                new SqlParameter("@ActionType", obj.ActionType)
            };
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, "spGetReportByDivision", parameters.ToArray());
                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return dt;
        }




        #endregion

        #region Dashboard
        public DataSet GetMOMDashboard(MOMDashboard obj)
        {

            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@UserUno", obj.UserUno)
            };
            DataSet ds = new DataSet();
            try
            {
                ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, "spGetMoMDashBoard", parameters.ToArray());

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }
            return ds;
        }

        #endregion




        #region EMAIL
        public DataSet GetEmailMeetingInfo(GenInfoAtdParam obj)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {   new SqlParameter("@UserUno", obj.UserUno),
                new SqlParameter("@type", obj.ActionType),
                new SqlParameter("@GenInfoUno", obj.GenInfoAtdUno)
            };
            DataSet ds = new DataSet();
            try
            {
                ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, "spGetEmailInfo", parameters.ToArray());

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }
            return ds;
        }

        #endregion


        #region Genenral Info 

        public DataTable GetGeneralInfomation(GenInfoAtdParam obj)
        {
            obj.ActionType = obj.ActionType == "TBD" ? "spGetTBD" : obj.ActionType == "MEETING" ? "spGetMeeting" : "spGetAttendence";
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@UserUno", obj.UserUno),
                new SqlParameter("@GenInfoUno", obj.GenInfoAtdUno),
                new SqlParameter("@type", obj.RecordType),
            };
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, obj.ActionType, parameters.ToArray());
                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return dt;
        }
        public string DelGeneralInfo(GenInfoAtdParam KPI)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@GenInfoUno", KPI.GenInfoAtdUno),
                    new SqlParameter("@UserUno", KPI.UserUno),
                    new SqlParameter("@ActionType", KPI.ActionType),
                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[3].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[4].Direction = ParameterDirection.Output;
                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spDeleteGenInfo", parameters.ToArray());
                int rtnParameter = (parameters[3].Value != null) ? Convert.ToInt32(parameters[3].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = (parameters[4].Value != null) ? parameters[4].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }

        public string PublishGeneralInfo(GenInfoAtdParam KPI)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@GenInfoUno", KPI.GenInfoAtdUno),
                    new SqlParameter("@UserUno", KPI.UserUno),
                    new SqlParameter("@ActionType", KPI.ActionType),

                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[3].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[4].Direction = ParameterDirection.Output;
                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spPublishGenInfo", parameters.ToArray());
                int rtnParameter = (parameters[3].Value != null) ? Convert.ToInt32(parameters[3].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = (parameters[4].Value != null) ? parameters[4].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }

        public string CreateFollowupMeeting(GenInfoAtdParam KPI)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@GenInfoUno", KPI.GenInfoAtdUno),
                    new SqlParameter("@UserUno", KPI.UserUno),
                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[2].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[3].Direction = ParameterDirection.Output;
                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spInsertUpdateMeetingFollowUp", parameters.ToArray());
                int rtnParameter = (parameters[2].Value != null) ? Convert.ToInt32(parameters[2].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = Convert.ToString(rtnParameter);
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }
        public string GeneralInfoAtd_Insert_Update(GenInfoAtdInsert KPI)
        {
            KPI.ActionType = KPI.ActionType == "TBD" ? "spInsertUpdateTBD" : KPI.ActionType == "MEETING" ? "spInsertUpdateMeeting" : "spInsertUpdateAttendence";
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@GenInfoUno", KPI.GenInfoAtdUno),
                    new SqlParameter("@Subject", KPI.SubjectAtd),
                    new SqlParameter("@MoMNo", KPI.MomNoAtd),
                    new SqlParameter("@MeetingTypeUno", KPI.MeetingTypeUno),
                    new SqlParameter("@MeetingDate", KPI.MeetingDateAtd),
                    new SqlParameter("@StartTime", KPI.StartTimeAtd),
                    new SqlParameter("@EndTime", KPI.EndTimeAtd),
                    new SqlParameter("@LocationUno", KPI.locationUno),
                    new SqlParameter("@MeetingRoom", KPI.MeetingRoomName),
                    new SqlParameter("@UserUno", KPI.UserUno),
                    new SqlParameter("@ApproverSign", KPI.ApproverSign),
                    new SqlParameter("@ReviewerSign", KPI.ReviewerSign),
                    new SqlParameter("@CreatedSign", KPI.CreaterSign),
                    new SqlParameter("@InsUpdType", KPI.CallFrom),
                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[14].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[15].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, KPI.ActionType, parameters.ToArray());
                int rtnParameter = (parameters[14].Value != null) ? Convert.ToInt32(parameters[14].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = Convert.ToString(rtnParameter);
                }
                else
                {
                    result = "failure";
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }



        #endregion

        #region Attendee 

        public DataTable GetAttendees(AttendeesAtd obj)
        {
            obj.ActionType = obj.ActionType == "TBD" ? "spGetTBD_Attendees" : obj.ActionType == "MEETING" ? "spGetMeeting_Attendees" : "spGetAttendence_Attendees";
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@UserUno", obj.UserUno),
                new SqlParameter("@LanguageUno", 0),
                new SqlParameter("@GenInfoUno", obj.GenInfoAtdUno),
                new SqlParameter("@StartIndex", obj.StartIndex),
                new SqlParameter("@MaxCount", obj.MaxCount)
            };
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, obj.ActionType, parameters.ToArray());
                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return dt;
        }

        public string Attendee_Insert_Update(AttendeesAtd KPI)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@AttendeesUno", KPI.AttendeesUnoAtd),
                    new SqlParameter("@GenInfoUno", KPI.GenInfoAtdUno),
                    new SqlParameter("@Name", KPI.NameAtd),
                    new SqlParameter("@Company", KPI.CompanyAtd),
                    new SqlParameter("@Contact", KPI.ContactAtd),
                    new SqlParameter("@Status", KPI.Status),
                    new SqlParameter("@Email", KPI.emailAtd),
                    new SqlParameter("@PrNumber", KPI.PrNumberAtd),
                    new SqlParameter("@UserUno", KPI.UserselUno),
                    new SqlParameter("@UserRole", KPI.Role),
                    new SqlParameter("@CurrentUserUno", KPI.UserUno),
                    new SqlParameter("@TableName", KPI.ActionType),

                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[12].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[13].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spInsertUpdateAttendees", parameters.ToArray());
                int rtnParameter = (parameters[12].Value != null) ? Convert.ToInt32(parameters[12].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = Convert.ToString(rtnParameter);
                }
                else
                {
                    result = Convert.ToString(parameters[13].Value);
                }


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }

        public string Attendee_Delete(AttendeesAtd KPI)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@AttendeesUno", KPI.AttendeesUnoAtd),
                    new SqlParameter("@UserUno", KPI.UserUno),
                    new SqlParameter("@ActionType", KPI.ActionType)
                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[3].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[4].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spDeleteAttendee", parameters.ToArray());
                int rtnParameter = (parameters[3].Value != null) ? Convert.ToInt32(parameters[3].Value) : 0;

                if (rtnParameter > 0)
                {
                    result = (parameters[4].Value != null) ? parameters[4].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }
        internal string InsertOutlookAttendees(OutlookAttendeeInsert obj)
        {
            string result = string.Empty;
            try
            {
                List<OutlookAttendee> atdObjList = JsonConvert.DeserializeObject<List<OutlookAttendee>>(obj.AttendeeJSONString);

                var table = new DataTable();

                table.Columns.Add("Name", typeof(string));
                table.Columns.Add("Email", typeof(string));
                table.Columns.Add("PRN", typeof(string));
                table.Columns.Add("UserUno", typeof(int));

                for (var k = 0; k < atdObjList.Count; k++)
                {
                    table.Rows.Add(atdObjList[k].Name, atdObjList[k].Email, atdObjList[k].PRN, atdObjList[k].UserUno);
                }
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@GenInfoUno", obj.GenInfoUno),
                    new SqlParameter("@UserUno", obj.UserUno),
                    new SqlParameter("@ActionType", obj.ActionType)
                };
                var atdList = new SqlParameter("@AttendeeList", SqlDbType.Structured);
                atdList.TypeName = "dbo.AttendeeList";
                atdList.Value = table;
                parameters.Add(atdList);
                parameters[3].Direction = ParameterDirection.Input;
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[4].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[5].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spInsertOutlookAttendee", parameters.ToArray());
                int rtnParameter = (parameters[4].Value != null) ? Convert.ToInt32(parameters[4].Value) : 0;

                if (rtnParameter > 0)
                {
                    result = (parameters[5].Value != null) ? parameters[5].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }
        public string AttendeColumnUpdate(AttendeesAtd objAttendees)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@AttendeesUno", objAttendees.AttendeesUnoAtd),
                    new SqlParameter("@GenInfoUno", objAttendees.GenInfoAtdUno),
                    new SqlParameter("@UpdateType", objAttendees.UpdateType),
                    new SqlParameter("@Status", objAttendees.Status),
                    new SqlParameter("@Role", objAttendees.Role),
                    new SqlParameter("@AttendeeSign", objAttendees.SignImage),
                    new SqlParameter("@UserUno", objAttendees.UserUno),
                    new SqlParameter("@ActionType", objAttendees.ActionType),
                    new SqlParameter("@ApologizedReason", objAttendees.ApologizedReason),
                };

                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[9].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[10].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spUpdateAttendeesInfo", parameters.ToArray());
                int rtnParameter = (parameters[9].Value != null) ? Convert.ToInt32(parameters[9].Value) : 0;

                if (rtnParameter > 0)
                {
                    result = (parameters[10].Value != null) ? parameters[10].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }

            }
            catch (Exception ex)
            {
                result = "failure";
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }
            return result;

        }


        #endregion

        #region Agenda

        public DataTable GetAgendaDetails(AgendaDetails obj)
        {
            obj.ActionType = obj.ActionType == "TBD" ? "spGetTBD_Agenda" : "spGetMeeting_Agenda";
            List<SqlParameter> parameters = new List<SqlParameter>
            {

                new SqlParameter("@UserUno", obj.UserID),
                new SqlParameter("@GenInfoUno", obj.GenInfoAtdUno),
                new SqlParameter("@StartIndex", obj.StartIndex),
                new SqlParameter("@MaxCount", obj.MaxCount)

            };
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, obj.ActionType, parameters.ToArray());
                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return dt;
        }

        public string AgendaDetails_Insert_Update(AgendaDetails obj)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@AgendaUno", obj.AgendaUno),
                    new SqlParameter("@AgendaParentUno", obj.AgendaParentUno),
                    new SqlParameter("@GenInfoUno", obj.GenInfoAtdUno),
                    new SqlParameter("@Note", obj.Note),
                    new SqlParameter("@Responsibility", obj.Responsibility),
                    new SqlParameter("@DueDate", obj.DueDate),
                    new SqlParameter("@CurrentUserUno", obj.UserID),
                    new SqlParameter("@TableName", obj.ActionType)
                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[8].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[9].Direction = ParameterDirection.Output;
                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spInsertUpdateMAgenda", parameters.ToArray());
                int rtnParameter = (parameters[8].Value != null) ? Convert.ToInt32(parameters[8].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = (parameters[9].Value != null) ? parameters[9].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }

        public string AgendaDetails_Delete(AgendaDetails obj)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@AgendaUno", obj.AgendaUno),
                    new SqlParameter("@UserUno", obj.UserID),
                    new SqlParameter("@ActionType", obj.ActionType)
                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[3].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[4].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spDeleteAgenda", parameters.ToArray());
                int rtnParameter = (parameters[3].Value != null) ? Convert.ToInt32(parameters[3].Value) : 0;
                // if (i > 0 && rtnParameter > 0)
                if (rtnParameter > 0)
                {
                    result = (parameters[4].Value != null) ? parameters[4].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }


        #endregion

        #region Comment

        public DataTable GetAtdComment(cmtObject obj)
        {
            obj.ActionType = obj.ActionType == "TBD" ? "spGetTBD_Comment" : obj.ActionType == "MEETING" ? "spGetMeeting_Comment" : "spGetAttendence_Comment";
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@UserUno", obj.UserId),
                new SqlParameter("@GenInfoUno", obj.GenralInfoId),
                new SqlParameter("@StartIndex", obj.StartIndex),
                new SqlParameter("@MaxCount", obj.MaxCount),

            };
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, obj.ActionType, parameters.ToArray());
                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return dt;
        }

        public string InsertAtdComment(cmtObject cmtObj)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@CommentUno", cmtObj.CmdId),
                    new SqlParameter("@CurrentUserUno", cmtObj.UserId),
                    new SqlParameter("@GenInfoUno", cmtObj.GenralInfoId),
                    new SqlParameter("@Comment", cmtObj.CmdText),
                    new SqlParameter("@TableName", cmtObj.ActionType),
                    new SqlParameter("@CommentUsers", cmtObj.CmtUsers),
                };


                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[6].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[7].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spInsertUpdateComment", parameters.ToArray());
                int rtnParameter = (parameters[6].Value != null) ? Convert.ToInt32(parameters[6].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = (parameters[7].Value != null) ? parameters[7].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }

        public string DelAtdComment(cmtObject cmtObj)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@CommentUno", cmtObj.CmdId),
                    new SqlParameter("@ActionType", cmtObj.ActionType),
                    new SqlParameter("@UserUno", cmtObj.UserId)

                };

                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[3].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[4].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spDeleteComment", parameters.ToArray());
                int rtnParameter = (parameters[3].Value != null) ? Convert.ToInt32(parameters[3].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = (parameters[4].Value != null) ? parameters[4].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }

        #endregion Comment

        #region FileUpload
        public string fileUpload(int GenInfoId, int userId, string actionType, string Name, string ContentType, byte[] content)
        {
            string result = string.Empty;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@GenInfoUno", GenInfoId),
                    new SqlParameter("@ContentType", ContentType),
                    new SqlParameter("@Name", Name),
                    new SqlParameter("@FileContent", content),
                    new SqlParameter("@CurrentUserUno", userId),
                    new SqlParameter("@TableName", actionType)

                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[6].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[7].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spInsertUpdateDocument", parameters.ToArray());
                int rtnParameter = (parameters[6].Value != null) ? Convert.ToInt32(parameters[6].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = (parameters[6].Value != null) ? parameters[6].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                result = "failure";
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }
        public string DeleteAttachedFile(FileAttachement obj)
        {
            string result = string.Empty;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@AttachmentUno", obj.AttachmentUno),
                    new SqlParameter("@UserUno", obj.UserId),
                    new SqlParameter("@ActionType", obj.ActionType),

                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[3].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[4].Direction = ParameterDirection.Output;

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spDeleteDocument", parameters.ToArray());
                int rtnParameter = (parameters[3].Value != null) ? Convert.ToInt32(parameters[3].Value) : 0;
                if (rtnParameter > 0)
                {
                    result = (parameters[4].Value != null) ? parameters[4].Value.ToString() : "";
                }
                else
                {
                    result = "failure";
                }


            }
            catch (Exception ex)
            {
                result = "failure";
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }

        public DataTable GetFileList(FileAttachement obj)
        {
            obj.ActionType = obj.ActionType == "TBD" ? "spGetTBD_Documents" : "spGetMeeting_Documents";
            DataTable dt = new DataTable();
            try
            {

                List<SqlParameter> parameters = new List<SqlParameter>
                {   new SqlParameter("@AttachmentUno", obj.AttachmentUno),
                    new SqlParameter("@GenInfoUno", obj.GeneralInfoId)

                };

                DataSet ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, obj.ActionType, parameters.ToArray());
                dt = ds.Tables[0];


            }
            catch (Exception ex)
            {

                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return dt;
        }
        #endregion


        #region Alerts

        public DataTable GetAlertList(Alert obj)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {

                new SqlParameter("@UserUno", obj.UserUno),
                new SqlParameter("@StartIndex", obj.StartIndex),
                new SqlParameter("@MaxCount", obj.MaxCount),
                new SqlParameter("@Condition", obj.Condition),
            };
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, "spGetExpertAlertList", parameters.ToArray());
                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return dt;
        }

        public string MarkAsVisited(Alert KPI)
        {
            string result = string.Empty;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@UserUno", KPI.UserUno)

                };
                parameters.Add(new SqlParameter("@OutParameter", SqlDbType.Int));
                parameters[1].Direction = ParameterDirection.Output;
                parameters.Add(new SqlParameter("@OutErrorMessage", SqlDbType.NVarChar, 1000));
                parameters[2].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, "spMarkAsVisitExpertAlert", parameters.ToArray());
                result = "success";


            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return result;
        }


        #endregion


        #region Error Log
        public string InsertLog(string LogDesc, string LogType)
        {
            string result = string.Empty;
            try
            {
                String query = "INSERT INTO dbo.TblLogFile (LogDesc,LogType) VALUES (@LogDesc,@LogType)";
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@LogDesc", LogDesc),
                    new SqlParameter("@LogType", LogType)

                };

                int i = SqlHelper.ExecuteNonQuery(_connectionString, CommandType.Text, query, parameters.ToArray());
                result = "";


            }
            catch (Exception ex)
            {
                ErrorLog.ErrorLogFile(_Errorlog, ex);
            }

            return result;
        }
        #endregion






        #region Division
        //GetDivision Start
        public DataTable GetDivision(Division obj)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@ID", obj.ID),
                new SqlParameter("@UserUno", obj.UserUno),
                new SqlParameter("@LanguageUno", obj.LanguageUno),
                new SqlParameter("@Condition", obj.Condition),


            };
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(_connectionString, CommandType.StoredProcedure, "spGetMstDivision", parameters.ToArray());
                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_Errorlog, ex);
            }

            return dt;
        }
        // GetDivision End


        #endregion


    }
}
