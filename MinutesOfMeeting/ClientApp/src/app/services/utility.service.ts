import { Injectable } from '@angular/core';



@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  onlyNumber(e) {
    if ((e.charCode < 48 || e.charCode > 57))
      return false;
    return true;
  }

  public static clone(jsonObject: object) {
    return JSON.parse(JSON.stringify(jsonObject));
  }

  public static convertUTCDate(convertdate) {
    return new Date(new Date(convertdate).toLocaleDateString() + 'UTC');
  }


  public convertTime24to12(time24h): string {
    var timex = time24h.split(':');
    timex[0] = timex[0].split(' ').length > 1 ? timex[0].split(' ')[1] : timex[0];
    if (timex[0] !== undefined && timex[1] !== undefined) {
      var hor = parseInt(timex[0]) > 12 ? (parseInt(timex[0]) - 12) : timex[0];
      var minu = timex[1];
      var merid = parseInt(timex[0]) < 12 ? 'PM' : 'AM';
      return hor + ':' + minu + ' ' + merid;


    }
  }

 

  public isNumericNull(value): number {
    return (value == null || value == undefined) ? 0 : value;
  }
  public isDateNull(value): Date {
    return (value == null || value == undefined) ? new Date('1/1/1999') : value;
  }
  public isStringNull(value): string {
    return (value == null || value == undefined) ? '' : value.trim();
  }
  public ArrayToCommaSeperator(value): string {
    let stRtn = '';
    if (value != null && value != undefined) {
      //for (var i = 0; i < value.length; i++) {
      //  stRtn += value[i];
      //}
      //stRtn = stRtn.replace(/,\s*$/, "");
      stRtn = value.toString();
    }
    return stRtn;
  }
  public CommaSeperatorToArray(value: string): any {

    if (value != null && value != undefined && value != '' && value.indexOf(",") > 0) {
      
      return value.split(',');
    }
    else if (value != null && value != '') {
      let inpUno: number[] = [];
      inpUno.push(parseInt(value))
      return inpUno;
    }
    else if (value == null || value == undefined || value == '') {
      return [];
    }


    return value;
  }
  



  InboxFilter_store(objfilterBy: number, objsla: number, objyear: number, objquater: number, objdivision: number, objfrequency: number) {
    if (window.localStorage) {
      // Create item:
      let key = 'inbox_filter_musherat_storage';
      localStorage.removeItem(key);
      let myObj = { filterBy: objfilterBy, sla: objsla, year: objyear, quater: objquater, division: objdivision, frequency: objfrequency };
      localStorage.setItem(key, JSON.stringify(myObj));
    }
  }
  InboxFilter_get() {
    if (window.localStorage) {
      // Get item:
      let key = 'inbox_filter_musherat_storage';
      return JSON.parse(localStorage.getItem(key));
    }
    return null;
  }
}



