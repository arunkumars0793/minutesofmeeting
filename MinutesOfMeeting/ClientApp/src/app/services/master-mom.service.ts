import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { MeetingType, MeetingLocation, MeetingRoom } from '../models/MasterMOM';
import { momDashboard, MeetingAppoinments, outlookAttendeeInsert, Attendees, GenInfoAtd, AttendeesAtd, FileAttachement, cmtObject, AgendaDetails, alert } from '../models/MOMForm';

import { saveAs } from 'file-saver';
import { usermaster, division } from '../models/admin';
@Injectable({
  providedIn: 'root'
})
export class MasterMOMService {
  // Define API
  apiURL = environment.apiBase; //'http://localhost:59094/';
  constructor(private http: HttpClient) { }
  /*========================================
  CRUD Methods for consuming RESTful API
  =========================================*/
  // Http Options
  // Http Options
  private GetHttpOptions() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.jwtToken()
      })
    }
  }
  httpOptions_file = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.jwtToken()
    })
  };

  private GetFileHttpOptions() {
    return {
      responseType: "blob",
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.jwtToken()
      })
    }
  }
  
  //JWT token access
  public jwtToken() {

    var struser = localStorage.getItem('accessinfo');
    if (struser) {
      let currentAccess = JSON.parse(struser);
      // create authorization header with jwt token           
      if (currentAccess && currentAccess.token) {
        //let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
        return currentAccess.token;
      }
    }
  }


  //MeetingType//
  public getMeetingType(objMeetingType: MeetingType) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetMeetingType', objMeetingType, this.GetHttpOptions());
  }
  public addEditMeetingType(objMeetingType: MeetingType) {
    return this.http.post(this.apiURL + '/api/MasterMOM/InsertUpdateMeetingType', objMeetingType, this.GetHttpOptions());
  }
  public deleteMeetingType(objMeetingType: MeetingType) {
    return this.http.post(this.apiURL + '/api/MasterMOM/DeleteMeetingType', objMeetingType, this.GetHttpOptions());
  }


  //location//
  public getLocation(objMeetingLocation: MeetingLocation) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetLocation', objMeetingLocation, this.GetHttpOptions());
  }

  public addEditLocation(objMeetingLocation: MeetingLocation) {
    return this.http.post(this.apiURL + '/api/MasterMOM/InsertUpdateLocation', objMeetingLocation, this.GetHttpOptions());
  }
  public deleteLocation(objMeetingLocation: MeetingLocation) {
    return this.http.post(this.apiURL + '/api/MasterMOM/DeleteLocation', objMeetingLocation, this.GetHttpOptions());
  }


  //MeetingRoom//
  public getMeetingRoom(objMeetingRoom: MeetingRoom) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetMeetingRoom', objMeetingRoom, this.GetHttpOptions());
  }

  public addEditMeetingRoom(objMeetingRoom: MeetingRoom) {
    return this.http.post(this.apiURL + '/api/MasterMOM/InsertUpdateMeetingRoom', objMeetingRoom, this.GetHttpOptions());
  }
  public deleteMeetingRoom(objMeetingRoom: MeetingRoom) {
    return this.http.post(this.apiURL + '/api/MasterMOM/DeleteMeetingRoom', objMeetingRoom, this.GetHttpOptions());
  }
  public getDropMeetingLocation(objMeetingRoom: MeetingRoom) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetDropMeetingLocation', objMeetingRoom, this.GetHttpOptions());
  }


 

  //attendees//
  public getAttendees(objAttendees: Attendees) {
  
    return this.http.post(this.apiURL + '/api/MasterMOM/GetAttendees', objAttendees, this.GetHttpOptions());
  }
  public addEditAttendees(objAttendees: Attendees) {
    return this.http.post(this.apiURL + '/api/MasterMOM/InsertUpdateAttendees', objAttendees, this.GetHttpOptions());
  }
  public deleteAttendees(objAttendees: Attendees) {
    return this.http.post(this.apiURL + '/api/MasterMOM/DeleteAttendees', objAttendees, this.GetHttpOptions());
  }
  public InsertOutlookAttendees(inputVal: outlookAttendeeInsert) {  
    return this.http.post(this.apiURL + '/api/MasterMOM/InsertOutlookAttendees', inputVal, this.GetHttpOptions());
  }

  public delAtdComment(inputVal: cmtObject) {
  
    return this.http.post(this.apiURL + '/api/MasterMOM/DelAtdComment', inputVal, this.GetHttpOptions());
  }
  public insertAtdComment(inputVal: cmtObject) {
  
    return this.http.post(this.apiURL + '/api/MasterMOM/InsertAtdComment', inputVal, this.GetHttpOptions());
  }
  public getAtdComment(inputVal: cmtObject) {  
    return this.http.post(this.apiURL + '/api/MasterMOM/GetAtdComment', inputVal, this.GetHttpOptions());
  }

  ////meeting_attendance start
  public getGeneralInfoAtd(objGenInfoAtd: GenInfoAtd) {
  
    return this.http.post(this.apiURL + '/api/MasterMOM/GetGeneralInfomation', objGenInfoAtd, this.GetHttpOptions());
  }

  public delGeneralInfoAtd(objGenInfoAtd: GenInfoAtd) {
  
    return this.http.post(this.apiURL + '/api/MasterMOM/DelGeneralInfoAtd', objGenInfoAtd, this.GetHttpOptions());
  }

  public publishGeneralInfo(objGenInfoAtd: GenInfoAtd) {
  
    return this.http.post(this.apiURL + '/api/MasterMOM/PublishGeneralInfo', objGenInfoAtd, this.GetHttpOptions());
  }

  public createFollowupMeeting(objGenInfoAtd: GenInfoAtd) {
  
    return this.http.post(this.apiURL + '/api/MasterMOM/CreateFollowupMeeting', objGenInfoAtd, this.GetHttpOptions());
  }

  public getMOMNumber(objGenInfoAtd: GenInfoAtd) {
  
    return this.http.post(this.apiURL + '/api/MasterMOM/GetMOMNumber', objGenInfoAtd, this.GetHttpOptions());
  }
  public addEditGeneralInfoAtd(objGenInfoAtd: GenInfoAtd) {
    return this.http.post(this.apiURL + '/api/MasterMOM/InsertUpdateGeneralInfoAtd', objGenInfoAtd, this.GetHttpOptions());
  }
  public deleteGeneralInfoAtd(objGenInfoAtd: GenInfoAtd) {
    return this.http.post(this.apiURL + '/api/MasterMOM/DeleteGeneralInfoAtd', objGenInfoAtd, this.GetHttpOptions());
  }
  public getMeetingTypedropsAtd(objGenInfoAtd: GenInfoAtd) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetDropMeetingTypeAtd', objGenInfoAtd, this.GetHttpOptions());
  }
  public getDropMeetingLocationAtd(objGenInfoAtd: GenInfoAtd) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetDropMeetingLocationAtd', objGenInfoAtd, this.GetHttpOptions());
  }
  public getDropMeetingRoomsAtd(objGenInfoAtd: GenInfoAtd) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetDropMeetingRoomAtd', objGenInfoAtd, this.GetHttpOptions());
  }

  //AttendeesAtd
  public getAttendeesAtd(objAttendeesAtd: AttendeesAtd) {
  
    return this.http.post(this.apiURL + '/api/MasterMOM/GetAttendeesAtd', objAttendeesAtd, this.GetHttpOptions());
  }

  public getAttendeDD(objAttendeesAtd: AttendeesAtd) {
  
    return this.http.post(this.apiURL + '/api/MasterMOM/GetAttendeesAtd', objAttendeesAtd, this.GetHttpOptions());
  }


  public getGeneralInfoDetails(objAttendeesAtd: AttendeesAtd) {
  
    return this.http.post(this.apiURL + '/api/MasterMOM/getGeneralInfoDetails', objAttendeesAtd, this.GetHttpOptions());
  }
  public AttendeColumnUpdate(objAttendeesAtd: AttendeesAtd) {
  
    return this.http.post(this.apiURL + '/api/MasterMOM/AttendeColumnUpdate', objAttendeesAtd, this.GetHttpOptions());
  }
  public ApologizedReasonUpdate(objAttendeesAtd: AttendeesAtd) {

    return this.http.post(this.apiURL + '/api/MasterMOM/ApologizedReasonUpdate', objAttendeesAtd, this.GetHttpOptions());
  }

  public addEditAttendeesAtd(objAttendeesAtd: AttendeesAtd) {
    return this.http.post(this.apiURL + '/api/MasterMOM/InsertUpdateAttendeesAtd', objAttendeesAtd, this.GetHttpOptions());
  }
  public deleteAttendeesAtd(objAttendeesAtd: AttendeesAtd) {
    return this.http.post(this.apiURL + '/api/MasterMOM/DeleteAttendeesAtd', objAttendeesAtd, this.GetHttpOptions());
  }
  public getUserList(objAttendeesAtd: AttendeesAtd) {
    return this.http.post(this.apiURL + '/api/MasterMOM/getUserList', objAttendeesAtd, this.GetHttpOptions());
  }
  public getDropAtdCategorizeAtd(objAttendeesAtd: AttendeesAtd) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetDropAtdCategorizeAtd', objAttendeesAtd, this.GetHttpOptions());
  }
  public getDropRoleAtd(objAttendeesAtd: AttendeesAtd) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetDropRoleAtd', objAttendeesAtd, this.GetHttpOptions());
  }
  //Fileattachements

  public GetFileList(obj: FileAttachement) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetFileList', obj, this.GetHttpOptions());
  }

  downloadAttachFile(fileType: string, obj: FileAttachement) {
    this.http.post(this.apiURL + '/api/MasterMOM/DownloadAttachFile', obj, {
      responseType: "blob",
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.jwtToken()
      })})
      .subscribe((res) => {
        var blob = new Blob([res], { type: fileType })
        saveAs(blob, obj.fileName);
      });
  }
  public deleteAttachedFile(objSLA: FileAttachement) {
    return this.http.post(this.apiURL + '/api/MasterMOM/DeleteAttachedFile', objSLA, this.GetHttpOptions());
  }


  // Agenda
  public addEditAgenda(objAgenda: AgendaDetails) {
    return this.http.post(this.apiURL + '/api/MasterMOM/InsertUpdateAgendaDetail', objAgenda, this.GetHttpOptions());
  }
  public deleteAgenda(objAgenda: AgendaDetails) {
    return this.http.post(this.apiURL + '/api/MasterMOM/DeleteAgendaDetails', objAgenda, this.GetHttpOptions());
  }
  public getAgendaDetails(objAgenda: AgendaDetails) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetAgendaDetails', objAgenda, this.GetHttpOptions());
  }
  public getMOMDashboard(objMoMDashboard: momDashboard) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetMOMDashboard', objMoMDashboard, this.GetHttpOptions());
  }

  public getSearchList(objMoMDashboard: momDashboard) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetSearchList', objMoMDashboard, this.GetHttpOptions());
  }

    public getDivMeetingList(objMoMDashboard: momDashboard) {
      return this.http.post(this.apiURL + '/api/MasterMOM/GetDivMeetingList', objMoMDashboard, this.GetHttpOptions());
  }

  //Usermaster
  public getUserMasters(objusermaster: usermaster) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetUserMasters', objusermaster, this.GetHttpOptions());
  }
  public addEditUserMaster(objusermaster: usermaster) {
    return this.http.post(this.apiURL + '/api/MasterMOM/InsertUpdateUserMaster', objusermaster, this.GetHttpOptions());
  }
  public deleteUserMaster(objusermaster: usermaster) {
    return this.http.post(this.apiURL + '/api/MasterMOM/DeleteUserMaster', objusermaster, this.GetHttpOptions());
  }
  //Division
  public getDivisions(objDivision: division) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetDivision', objDivision, this.GetHttpOptions());
  }

  public getMeetings(objMeeting: MeetingAppoinments) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetMeetings', objMeeting, this.GetHttpOptions());
  }


  //Alert
  public getAlertList(objAlert: alert) {
    return this.http.post(this.apiURL + '/api/MasterMOM/GetAlertList', objAlert, this.GetHttpOptions());
  }

  public markAsVisited(objAlert: alert) {
    return this.http.post(this.apiURL + '/api/MasterMOM/MarkAsVisited', objAlert, this.GetHttpOptions());
  }


}
