import { Injectable } from '@angular/core';
import {  Users } from '../models/users';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  // Define API
  apiURL = environment.apiBase; //'http://localhost:59094/';
  constructor(private http: HttpClient) { }
  /*========================================
  CRUD Methods for consuming RESTful API
  =========================================*/

  private GetHttpOptions() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.jwtToken()
      })
    }
  }

  //JWT token access
  public jwtToken() {

    var struser = sessionStorage.getItem('accessinfo');
    if (struser) {
      let currentAccess = JSON.parse(struser);
      // create authorization header with jwt token           
      if (currentAccess && currentAccess.token) {
        //let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
        return currentAccess.token;
      }
    }
  }




  //Login
  //Expert_Award Get
  AdUserAuthenticate(objusermaster: Users)
  {
    
    return this.http.post(this.apiURL + '/api/Login/AuthenticateUser', objusermaster, this.GetHttpOptions());

  }

  GetUserInfo_ByUserID(objusermaster: Users) {

    objusermaster.language = this.getCurrentLanguage();
    return this.http.post(this.apiURL + '/api/Login/GetUserInfo_ByUserID', objusermaster, this.GetHttpOptions());
  }

  getCurrentLanguage() {

    //var lng = this._langService.getLang();
    //if (lng) { return lng.ISOCode; }
    //else { return 'en'; }

    return 'en';
  }

}
