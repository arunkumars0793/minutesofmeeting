import { Injectable } from '@angular/core';
import { Users } from '../models/users';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public headers: Headers;
  apiURL: string = "";
  UserList: any = [];
  OutlookMeetingList: any = [];
  constructor(private _router: Router) {
    //this.headers = new Headers({ 'Content-Type': 'application/json' });
    //let options = new RequestOptions({ headers: this.headers });
    //this.apiURL = baseUrl;

  }

  getUserName(id) {
    let UserObj = this.UserList.filter((item) => item.userselUno == id);
    if (UserObj && UserObj.length > 0) {
      return UserObj[0].nameAtd;
    }
    return '';
  }

  getCurrentLanguage() {

    // var lng = this._langService.getLang();
    // if (lng) { return lng.ISOCode; }
    //else { return 'en'; }
    return 'en';
  }


  logout() {

    let objusers = new Users;
    var users = this.getUserInfo();
    localStorage.removeItem('userinfo');
    localStorage.removeItem('accessinfo');
    this._router.navigate(['/login']);
  }
  getUserInfo() {
    var user;
    var struser = localStorage.getItem('userinfo');
    if (struser) {
      user = JSON.parse(struser);
    }
    else {
      this._router.navigate(['/login']);
    }
    return user;
  }
  getUserRights(pagename: string) {
    let userRightsData: any = [];
    
    var struserRights = sessionStorage.getItem('userrights');
    if (struserRights) {
      var userRights: any = JSON.parse(struserRights);
      userRightsData = userRights.filter(item => item.pageName == pagename);
    }
    return userRightsData;
  }
  UpdatetUserTermsInfo() {


    var struser = localStorage.getItem('userinfo');
    if (struser) {
      struser = struser.replace('"termsandconditions":"0"', '"termsandconditions":"1"');
      localStorage.removeItem('userinfo');
      localStorage.setItem('userinfo', struser);
    }
  }
}
