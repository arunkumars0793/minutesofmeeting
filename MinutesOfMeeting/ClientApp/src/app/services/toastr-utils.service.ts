import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
@Injectable()
export class ToastrUtilsService {

  constructor(private toastr: ToastrService) { }
  UpdateMessage() {
    this.toastr.success('Successfully Updated', '',
    {
      timeOut:900,
      closeButton:true,
      positionClass: 'toast-bottom-right',
    });
  }

  DeleteMessage() {
    this.toastr.success('Successfully Deleted', '',
    {
      timeOut:900,
      closeButton:true,
      positionClass: 'toast-bottom-right',
    });
  }
 

  SaveMessage() {
    this.toastr.success('Successfully Saved', '',
    {
      timeOut:900,
      closeButton:true,
      positionClass: 'toast-bottom-right',
    });
  }

  SuccessCustomMessage(message: string = 'Successfully Saved',timeout:number = 900) {
    this.toastr.success(message, '',
    {
      timeOut:timeout,
      closeButton:true,
      positionClass: 'toast-bottom-right',
    });
  }

  WarnMessage(message:string = '',timeout:number = 900) {
    this.toastr.warning(message, '',
    {
      timeOut:timeout,
      closeButton:true,
      positionClass: 'toast-bottom-right',
    });
  }

  InfoMessage(message:string = '') {
    this.toastr.info(message, '',
    {
      timeOut:900,
      closeButton:true,
      positionClass: 'toast-bottom-right',
    });
  }


  ErrorMessage() {
    this.toastr.error('Error Occured!', '',
    {
      timeOut:900,
      closeButton:true,
      positionClass: 'toast-bottom-right',
    });
  }
  ValidationMessage() {
    this.toastr.error('Please enter the value!', '',
    {
      timeOut:900,
      closeButton:true,
      positionClass: 'toast-bottom-right',
    });
  }
  ErrorCustomMessage(message: string = 'Error Occured!', timeout:number = 900) {
    this.toastr.error(message, 'Error Occured!',
    {
      timeOut:timeout,
      closeButton:true,
      positionClass: 'toast-bottom-right',
    });
  }
}
