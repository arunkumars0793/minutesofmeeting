import { Data } from '@syncfusion/ej2-angular-grids';

export class AddMeeting {
  addMeetingUno?: number;

  meetingTypeUno?: number;
  meetingName?: string;

  subject?: string;
  mOMNo?: number;

  locationUno?: number;
  locationName?: string;

  meetingRoomUno?: number;
  meetingRoomName?: string;

  userselUno?: number;
  userselName?: string;

  atdCatgorizeUno?: number;
  atdCatgorizeName?: string;


  roleUno?: number;
  roleName?: string;

  meetingDate?: string;
  startTime?: string;
  endTime?: string;

  userUno?: number;
  languageUno?: number;

  actionType: string;
  icon: string = 'AddMeeting';
  type: string = 'AddMeeting';
}
export class GenInfo {
  callFrom: string;
  genInfoUno?: number;
  genInfoRefId?: number;
  meetingTypeUno?: number;
  meetingName?: string;
  subject?: string;
  momNo?: number;
  locationUno?: number;
  locationName?: string;
  meetingRoomUno?: number;
  meetingRoomName?: string;
  meetingDate?: Date;
  fMeetingDate?: string;
  startTime?: Date;
  fStartTime?: string;
  endTime?: Date;
  fEndTime?: string;
  userUno?: number;
  languageUno?: number;
  actionType: string;
  icon: string = 'GenInfo';
  type: string = 'GenInfo';
}
export class Agenda {
  agendaDtlsUno?: number;
  description?: string;
  responsibility?: string;
  dueDate?: string;

  userUno?: number;
  languageUno?: number;

  actionType: string;
  icon: string = 'Agenda';
  type: string = 'Agenda';
}
export class Attendees {
  attendeesUno?: number;
  userselUno?: number;
  userselName?: string;
  prNumber?: string;
  atdCatgorizeUno?: number;
  atdCatgorizeName?: string;
  roleUno?: number;
  roleName?: string;
  name?: string;
  company?: string;
  contact?: number;
  email?: string;
  userUno?: number;
  languageUno?: number;
  actionType: string;
  icon: string = 'Attendees';
  type: string = 'Attendees';
}
export class GenInfoAtd {
  genInfoAtdUno?: number;
  published?: number;
  meetingTypeUno?: number;
  meetingName?: string;
  subjectAtd?: string;
  approvalStatus?: string;
  momNoAtd?: any = '';
  locationUno?: number;
  locationName?: string;
  meetingRoomUno?: number;
  meetingRoomName?: string;
  meetingDateAtd?: any;
  fMeetingDateAtd?: string;
  startTimeAtd?: any;
  fStartTimeAtd?: string;
  endTimeAtd?: any;
  fEndTimeAtd?: string;
  userUno?: number;
  languageUno?: number;
  actionType: string;
  callFrom: string;
  createrSign: string;
  createDate: string;
  approverSign: string;
  reviewerSign: string;
  approvalDate: string;
  reviewDate: string;
  enteredBy?: number;
  approverId: number;
  reviewerId: number;
  type: string;
  pdfContent: string;
  initiatedBy:string
  recordType: number;
}




export class cmtObject {
  cmdId: number;
  genralInfoId: number;
  userId: number;
  cmdText: string;
  cmtUsers: string;
  cmtUserName: string;
  startIndex: number;
  maxCount: number;
  actionType: string;
}

export class alert {
  userUno: number;
  id: number;
  genInfoUno: number;
  isVisited: boolean;
  startIndex: number;
  maxCount: number;
  createdDate: Date;
  type?: string;
  description?: string;
  condition?: number;
}

export class MeetingAppoinments {
  apptId?: string;
  subject?: string;
  start?: string;
  end?: string;
  email?: string = '';
  location?: string;
  meetingDate?: string;
  UserName?: string;
  dayCount?: number;
}

export class OutlookFile {
  id?: string;
  fileName?: string;
  email?: string;
  dayCount?: number;
}

export class MeetingAttendee {
  email?: string = '';
  PRN?: string;
  name?: string;
  userUno?: number;
  active: true;
}


export class outlookAttendeeInsert {
  genInfoUno: number;
  userUno: number;
  attendeeJSONString?: string;
  fileJSONString?: string;
  agendaJSONString?: string;
  actionType: string;
}

export class AttendeesAtd {
  attendeesUnoAtd?: number;
  genInfoAtdUno?: number;
  userselUno?: number;
  userselName?: string;
  prNumberAtd?: string;
  role?: string;
  status?: string;
  nameAtd?: string;
  companyAtd?: string;
  contactAtd?: string;
  emailAtd?: string;
  userUno?: number;
  actionType: string;
  updateType: string;
  signImage: string;
  startIndex: number;
  maxCount: number;
  apologizedReason: string;
}
export class FileAttachement {
  attachmentUno?: number;
  generalInfoId?: number;
  fileName?: string;
  contentType?: string;
  createByName?: string;
  createDate?: string;
  userId?: number;
  actionType: string;
}
/*Agenda*/
export class AgendaDetails {
  agendaUno?: number;
  agendaParentUno?: number;
  genInfoAtdUno?: number;
  note?: string;
  responsibility?: any;
  responsibilityName?: string;
  dueDate?: any;
  isActive?: number;
  userID?: number;
  startIndex?: number;
  maxCount?: number;
  userName: string;
  idVal: string;
  isDueDate: boolean;
  iReps: boolean;
  actionType: string;
}
export class momDashboard {
  userUno?: number;
  startdt?: Date;
  enddt?: Date;
  actionType: string='';
  searchText: string='';
  noOfMyMeetings?: number;
  noOfMyAttendanceSheets?: number;
  noOfMyTasks?: string;
  noOfMyTBTSessions?: number;
  noOfDraftMeeting?: number;
  noOfNotPublishMeeting?: number;
  noOfPublishedMeeting?: number;
  noOfPartMeetings?: number;
  lstMOMDashboard_MyMeetings: momDashboard_MyMeetings[] = [];
  lstMOMDashboard_PartofMeetings: momDashboard_PartofMeetings[] = [];
  
}

export class momDashboard_MyMeetings {
  genInfoAtdUno?: number;
  moMNoAtd?: string;
  subjectAtd?: string;
  meetingDate?: string;
  startTime?: string;
  endTime?: string;
}

export class momDashboard_PartofMeetings {
  genInfoAtdUno?: number;
  moMNoAtd?: string;
  subjectAtd?: string;
  meetingDate?: string;
  startTime?: string;
  endTime?: string;
  approvalStatus?: string;
}
