
export class MeetingType {
  meetingTypeUno?: number;
  meetingTypecode?: string;
  meetingName?: string;
  userUno?: number;
  languageUno?: number;

}

export class MeetingLocation {
  locationUno?: number;
  locationCode?: string;
  locationName?: string;
  userUno?: number;
  languageUno?: number;
  actionType: string;
  icon: string = 'Location';
  type: string = 'Location';
}
export class MeetingRoom {
  meetingRoomUno?: number;
  meetingRoomCode?: string;
  meetingRoomName?: string;
  locationUno?: number;
  locationName?: string;
  userUno?: number;
  languageUno?: number;

}



