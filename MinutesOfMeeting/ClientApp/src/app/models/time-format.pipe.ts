import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeFormat'
})
export class TimeFormatPipe implements PipeTransform {

  transform(time24h: any, args?: any): any {
    time24h = new Date(time24h).toLocaleTimeString();
    if (time24h == undefined || time24h == null) { return time24h }
    else {
      var timex = time24h.split(':');
      timex[0] = timex[0].split(' ').length > 1 ? timex[0].split(' ')[1] : timex[0];
      if (timex[0] !== undefined && timex[1] !== undefined) {
        var hor = parseInt(timex[0]) > 12 ? (parseInt(timex[0]) - 12) : timex[0];
        var minu = timex[1];
        var merid = parseInt(timex[0]) < 12 ? 'AM' : 'PM';
        return hor + ':' + minu + ' ' + merid;


      }

    }

  }

}
