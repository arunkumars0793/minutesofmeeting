export class Users {
  id: number;
  userId: number;
  masterUserID: number;
  userName: string;
  password: string;
  token: string;
  language: string;
  status: number;
  firstName: string;
  lastName: string;
  displayName: string;
  designation: string;
  division: string;
  phoneNumber: string;
  department: string;
  emailId: string;
  picture: string;
  accountId: string;
  accountJId: string;
  profileStatus: string;
  chatStatus: string;
  roleId: number;
  role: string;
  isDisabledforSearch: number;
  profilePicture: string;
  message: string;
  createdBy: number;
  createdDate: string;
  modifiedBy: number;
  modifiedDate: string;
  Flag: string;
  Error: string;
  isMessage: boolean = false;
  messageCount: number = 0;
  isChatVisible: string = "";
  termsandConditions: string;
  groupID: number;
  versionNo: string;
  startIndex: number;
  maxCount: number;
  searchText: string;
  isProfileSearch: boolean = false;
  currentYear: number;
  currentHalf: number;
  currentQuarter: number;
  currentMonth: number;
  tenantUno: number;
  wellperformKPIRange: number;
  userGroupUno: number;
}


export class usergroup {
  usergroupUno?: number;
  usergroupName?: string;
  rightsXML?: string;
  userUno: number;
  itemUno: number;
  languageUno?: number;
  condition?: number;
  enteredBy?: number;

  pageUno?: number;
  pageName?: string;
  showPage?: boolean;
  showAdd?: boolean;
  showEdit?: boolean;
  showDelete?: boolean;
  pageType?: string;
  sectionName?: string;
  listPage: usergroup[] = [];
  tenantUno?: number;

}


export class page {
  pageUno?: number;
  pageName?: string;
  showPage?: boolean;
  showAdd?: boolean;
  showEdit?: boolean;
  showDelete?: boolean;
  pageType?: string;
  sectionName?: string;
  userUno: number;
  itemUno: number;
  languageUno?: number;
  condition?: number;
  tenantUno?: number;
}
export class userrights {
  usergroupUno?: number;
  usergroupName?: string;
  pageUno?: number;
  pageName?: string;
  showPage?: boolean;
  showAdd?: boolean;
  showEdit?: boolean;
  showDelete?: boolean;
  userUno: number;
  tenantUno?: number;
  languageUno?: number;
  condition?: number;


}
