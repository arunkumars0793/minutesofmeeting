export class Notification {
  id: number;
  notifyId: number;
  notifyIdCollection: string;
  notifyType: string;
  userID: number;
  createdUserID: number;
  profilePicture: string;
  userName: string;
  title: string;
  description: string;
  isVisited: number;
  isNotifyPopup: boolean;
  isGroup: number;
  groupName: string;
  groupID: number;
  createdBy: number;
  createdDate: string;
  modifiedBy: number;
  modifiedDate: string;
  startIndex: number;
  maxCount: number;
  maxMessageCount: number;
}

export class SearchUsers {
  id: number;
  userID: number;
  postID: number;
  groupID: number;
  userName: string;
  status: number;
  firstName: string;
  lastName: string;
  displayName: string;
  designation: string;
  division: string;
  phoneNumber: string;
  department: string;
  emailId: string;
  landLine: string;
  emailto: string;
  landLineto: string;
  accountId: string;
  role: string;
  profilePicture: string;
  isFollowed: number;
  thanksCount: number;
  fllowersCount: number;
  creditsCount: number;
  searchText: string;
  startIndex: number;
  maxCount: number;
  objSearchFilter: SearchFilter;
}

export class SearchFilterKey {
  id: number;
  value: string;
}


export class SearchFilter {
  userID: number;
  pageLanguage: string;
  location: SearchFilterKey[] = [];
  skills: SearchFilterKey[] = [];
  language: SearchFilterKey[] = [];
  department: SearchFilterKey[] = [];
}
export class Notifycount {
  generalNotifyCount: number;
  messageNotifyCount: number;
  isNotifyPage: boolean;
}




import * as CryptoJS from 'crypto-js';
export class EncryptDecrypt {

  EncryptText(inpText: any) {
    // //Encrypt the text with Base64
    // //var key = CryptoJS.enc.Base64.parse("#base64Key#");
    //// var iv = CryptoJS.enc.Base64.parse("#base64IV#");
    // //Impementing the Key and IV and encrypt the text
    // //var Hmackeyword = CryptoJS.HmacSHA1(inpText, 'secret key 123').toString();  
    // //var AESkeyword = CryptoJS.AES.encrypt(inpText, 'secret key 123').toString();         
    // //return AESkeyword;

    var key = CryptoJS.enc.Utf8.parse('8080808080808080');
    var iv = CryptoJS.enc.Utf8.parse('8080808080808080');

    var AESkeyword = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(inpText), key,
      {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
      });
    return AESkeyword.toString().replace('%2F', '~').replace('/', '~');
    return inpText;

  }
  DecryptText(inpText: any) {

    var bytesKeyword = CryptoJS.AES.decrypt(inpText, 'secret key 123');
    inpText = (inpText != null && inpText != undefined) ? inpText.replace('~', '/') : inpText;
    var key = CryptoJS.enc.Utf8.parse('8080808080808080');
    var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
    var bytesKeyword = CryptoJS.AES.decrypt(inpText, key,
      {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
      });

    var plainText = bytesKeyword.toString(CryptoJS.enc.Utf8);
    return plainText;
    return inpText;
  }
}

export class Constants {
  static readonly DATE_FMT = 'dd/MMM/yyyy';
  static readonly DATE_TIME_FMT = '${Constants.DATE_FMT} hh:mm:ss';
}

export class chatMessage {
  message_sent: string;
  message_received: string;
  message_from_jid: string;
  message_date: Date;
  message_read: boolean;
}
