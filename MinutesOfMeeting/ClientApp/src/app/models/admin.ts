export class interaction {
  interactionUno?: number;
  interactionNameEn?: string;
  interactionNameAr?: string;
  isActive?: number;
  user?: number;
  //filter
  iD?: number;
  tenantUno?: number;
  languageUno?: number;
  itemUno?: number;
  condition?: number;
  actionType: string;
}
export class division {
  divisionUno?: number;
  divisionNameEn?: string;
  divisionNameAr?: string;
  divisionHeadUno?: number;
  divisionAdminUno?: number;
  divisionName?: string;
  divisionAdminName?: string;
  coordinatorName?: string;
  approverName?: string;
  divisionCode?: string;
  kPILastAutoId?: number;
  divisionCoordinatorUno?: number;
  divisionApproverUno?: number;
  isActive?: number;
  user?: number;
  //filter
  id?: number;

  languageUno?: number;

  condition?: number;
  actionType: string;
}
export class user {
  displayText?: string;
  userImage?: string;
  userUno?: number;
  tenantUno?: number;
  divisionUno?: number;

  //filter  
  languageUno?: number;
  itemUno?: number;
  condition?: number;
}

export class usermaster {
  iD?: number;
  id?: number;
  ntUsername?: string;
  displayNameEn?: string;
  displayNameAr?: string;
  firstname?: string;
  lastname?: string;
  emailId?: string;
  designationEn?: string;
  divisionUno?: number; 
  enteredBy?: string;
  userName?: string;
  designationAr?: string; 
  divisionNameEn?: string;
  divisionNameAr?: string;
  prNumber?: string;
  //filter
  userUno?: number;
  languageUno?: number;
  condition?: number;
  actionType: string;
}


