import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from './services/user.service';
import { Spinkit } from 'ng-http-loader';
import { UserIdleService } from 'angular-user-idle';
import { defaultOptions } from 'ngx-extended-pdf-viewer';
import { environment } from '../environments/environment';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  title = 'ClientApp';
  public spinkit = Spinkit;
  public KPINotification_data: any = [];
  user: any = {};
  login_UserID: number; // number
  currentYear?: number;
  currentHalf?: number;
  currentQuarter?: number;
  currentMonth?: number;
  isAdmin: boolean = false;
  nitifyCount: string = '';
  idleCount: number = 0;
  profilePicture: string = '';
  public displayText: string = 'Please do any action to reactivate your session.';
  public displayname: string = '';
  public role: string = '';
  public dataTarget_notify = 'dropdown2';
  restApi: any;
  returnUrl: string;

  @ViewChild('warnIdleDialog', { static: false }) warnIdleDialog: DialogComponent;

  constructor(private _activatedRoute: ActivatedRoute, private userService: UserService, public router: Router, private userIdle: UserIdleService) {
    defaultOptions.workerSrc = environment.apiBase + '/assets/pdf.worker-es5.js';}
  ngOnInit() {
   

    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe(count => {
      this.idleCount = 25 - count;
      this.displayText = 'Please do any action to reactivate your session.';
      this.nitifyCount = '('+ (25 - count) + ')s left to logout your session';
      if (count==1)
      this.warnIdleDialog.show()
    });

    // Start watch when time is up.
    this.userIdle.onTimeout().subscribe(() => {
    
      this.userIdle.resetTimer();
      this.userIdle.stopWatching();
      this.displayText = '';
      this.nitifyCount = 'Your session has been logged out due to inactivity..!'
      this.userService.logout();
    });






    document.onscroll = (args: any): void => {
      this.userIdle.resetTimer();
      this.warnIdleDialog.hide();
      if (document.getElementsByClassName("e-tooltip-wrap").length > 0) {
        for (var i = 0; i < document.getElementsByClassName("e-tooltip-wrap").length; i++) {
          document.getElementsByClassName("e-tooltip-wrap")[i].classList.toggle('e-popup-open');
          document.getElementsByClassName("e-tooltip-wrap")[i].classList.add('e-popup-close');
        }
      }
      if (args.path.filter(x => x.tagName == 'EJS-DIALOG').length==0) {
        //if (document.getElementsByClassName("e-popup-open").length > 0 && document.getElementsByClassName("e-popup-open")[0].classList.contains('e-popup-open')) {
        //  document.getElementsByClassName("e-popup-open")[0].classList.add('e-popup-close');
        //  document.getElementsByClassName("e-tooltip-wrap")[0].classList.toggle('e-tooltip-wrap');
        //}
      }

    }

    document.onclick = (args: any): void => {
      this.userIdle.resetTimer();
      this.warnIdleDialog.hide()
      if (document.getElementsByClassName("e-tooltip-wrap").length > 0) {
        for (var i = 0; i < document.getElementsByClassName("e-tooltip-wrap").length; i++) {
          document.getElementsByClassName("e-tooltip-wrap")[i].classList.toggle('e-popup-open');
          document.getElementsByClassName("e-tooltip-wrap")[i].classList.add('e-popup-close');
        }
      }
    

    }

    document.onmousemove = (args: any): void => {
      this.userIdle.resetTimer();
      this.warnIdleDialog.hide()
      
    }



    this.user = this.userService.getUserInfo();
    if (this.user == undefined || this.user.id <= 0) {
      this.router.navigate(['/login']);
      this.returnUrl = this.router['returnUrl'] || '/';
    }
  }
  
  onActivate(event) {
    window.scroll(0, 0);
    this.user = this.userService.getUserInfo();
    this.login_UserID = this.user.id;
   ;
    this.displayname = this.user.displayname;
    this.role = this.user.role;
    this.profilePicture = this.user.profilePicture;
    this.isAdmin = (this.user.role == 'Administrator') ? true : false;

    
  }


  logout() {

    this.userService.logout();
  }
}

   




  



