import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import { MeetingType } from '../../models/MasterMOM';
import { SaveEventArgs, DialogEditEventArgs, ExcelExportProperties } from '@syncfusion/ej2-grids';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import { ClickEventArgs } from '@syncfusion/ej2-navigations/src/toolbar';
import { UtilityService } from '../../services/utility.service';
import { UserService } from '../../services/user.service';
import { MasterMOMService } from '../../services/master-mom.service';
import { ToastrUtilsService } from './../../services/toastr-utils.service';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { Router } from '@angular/router';
@Component({
  selector: 'app-meeting-type',
  templateUrl: './meeting-type.component.html',
  providers: [ToastrUtilsService]
})
export class MeetingTypeComponent implements OnInit {
  public data: any;
  public user_data: any;
  public editSettings: Object;
  public filterSettings: Object;
  public toolbar: string[];
  public pageSettings: Object;
  public orderData: object;
  public orderForm: FormGroup;
  public submitClicked: boolean = false;
  public pageAction: string = 'add';

  @ViewChild('grid', { static: false }) public grid: GridComponent;
  public permission: number = 1;
  user: any = {};
  login_UserID: number; // number

  constructor(public utilityService: UtilityService,
    private toastr: ToastrUtilsService,
    public momMasterservice: MasterMOMService,
    private spinner: NgxSpinnerService,
    private userService: UserService,
    private _router: Router,
    private coolDialogs: NgxCoolDialogsService) { }

  ngOnInit() {
    var user = this.userService.getUserInfo();
    this.login_UserID = user.id;
    var Role = Number(user.role);
    if (Role != 1) {
      this._router.navigate(['/dashboard'])
    }
    this.loadMeetingType();
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, mode: 'Dialog' };
    if (this.permission == 1) {
      this.toolbar = ['Add', 'Edit', 'Delete', 'Search', 'ExcelExport'];
    }
    else
      this.toolbar = ['Search', 'ExcelExport'];

    this.pageSettings = { pageSize: 20, pageSizes: true };
    this.filterSettings = { type: 'Excel' };




  }


  // Get Meeting Type list
  loadMeetingType() {
    
    this.spinner.show();
    let inpValue = new MeetingType();
    inpValue.languageUno = 1033;
    inpValue.userUno = 1;
    
    this.momMasterservice.getMeetingType(inpValue)
      .subscribe(data => {
        
        this.data = data;
        //console.log(this.postData);
        this.spinner.hide();
      },
        () => {
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });
  }

  createFormGroup(data1: any): FormGroup {
    
    return new FormGroup({
      meetingTypeUno: new FormControl(data1.meetingTypeUno),
      meetingTypecode: new FormControl(data1.meetingTypecode, Validators.required),
      meetingName: new FormControl(data1.meetingName, Validators.required),

    });
  }

  actionBegin(args: SaveEventArgs): void {
    
    if (args.requestType === 'beginEdit' || args.requestType === 'add') {
      this.pageAction = (args.requestType === 'beginEdit') ? 'edit' : 'add';
      if (this.permission == 1) {
        this.submitClicked = false;
        this.orderForm = this.createFormGroup(args.rowData);
      }
      else {
        args.cancel = true;
      }
    }
    if (args.requestType === 'save') {
      this.submitClicked = true;
      //this.AddEditMeetingType(args.data)
      if (this.orderForm.valid) {
        args.data = this.orderForm.value;
        if (!this.AddEditMeetingType(args.data)) {
          this.submitClicked = false;
          args.cancel = true;
        }
        this.submitClicked = false;
      } else {
        args.cancel = true;
      }
    }

    if (args.requestType === 'delete') {
      args.cancel = true;
      this.coolDialogs.confirm('Please confirm to delete?')
        .subscribe(res => {
          if (res) {
            this.DeleteMeetingType(args.data);
          }
        });

    }
  }
  actionComplete(args: DialogEditEventArgs): void {
    if ((args.requestType === 'beginEdit' || args.requestType === 'add')) {
      const dialog = args.dialog;
      dialog.width = 900;
      // Set initail Focus
      if (args.requestType === 'beginEdit') {
        let objdata: any = args.rowData;
        dialog.header = args.requestType === 'beginEdit' ? 'Details of ' + objdata.meetingTypecode + ":" + objdata.meetingName : '';
      }

    }

  }
  AddEditMeetingType(data1: any) {
    
    if (this.orderForm.invalid) {

      this.toastr.ErrorCustomMessage('Something went wrong');
      return false;
    }



    if (this.DuplicateNameCheck(this.utilityService.isStringNull(data1.meetingTypecode), this.utilityService.isNumericNull(data1.meetingTypeUno))) {

      this.toastr.ErrorCustomMessage('Duplicates not allowed');
      return false;
    }
    else {
      
      let objMeetingType = new MeetingType();
      objMeetingType.meetingTypeUno = (this.pageAction == "add") ? 0 : this.utilityService.isNumericNull(data1.meetingTypeUno);
      objMeetingType.meetingTypecode = this.utilityService.isStringNull(data1.meetingTypecode);
      objMeetingType.meetingName = this.utilityService.isStringNull(data1.meetingName);

      objMeetingType.userUno = this.login_UserID;

      
      this.momMasterservice.addEditMeetingType(objMeetingType)
        .subscribe(() => {
          this.toastr.SaveMessage;
          this.loadMeetingType();
          this.spinner.hide();
        },
          () => {
            //Called when error
            this.toastr.ErrorMessage;
            this.spinner.hide();
          }
        ).add(() => {
          //Called when operation is complete (both success and error)
          this.spinner.hide();
        });
      return true;
    }
  }
  DuplicateNameCheck(meetingTypecode: string, meetingTypeUno: number) {
    if (this.data != null && this.data != undefined) {
      var rtnValue = false;
      this.data.forEach((grdData) => {
        //  if (roles.some(role => role.includes(extLink.role))) {
        //    extLink.visible = true;
        //  }
        if (grdData.meetingTypecode.toLocaleLowerCase() == meetingTypecode.toLocaleLowerCase() && meetingTypeUno != grdData.meetingTypeUno) {
          rtnValue = true;
        }
      });

      return rtnValue;
    }
  }

  DeleteMeetingType(data1: any) {

    let objMeetingType = new MeetingType();
    objMeetingType.meetingTypeUno = data1[0].meetingTypeUno;
    objMeetingType.userUno = this.login_UserID;
    //objDeaprtment.tenantUno = 1;
    
    this.momMasterservice.deleteMeetingType(objMeetingType)
      .subscribe(() => {
        
        this.toastr.DeleteMessage();
        this.loadMeetingType();
        this.spinner.hide();
      },
        () => {
          this.toastr.ErrorMessage();
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }
  public focusIn(target: HTMLElement): void {
    target.parentElement.classList.add('e-input-focus');
  }

  public focusOut(target: HTMLElement): void {
    target.parentElement.classList.remove('e-input-focus');
  }
  toolbarClick(args: ClickEventArgs): void {
    
    if (args.item.id === 'grdData_excelexport') {
      const excelExportProperties: ExcelExportProperties = {
        includeHiddenColumn: true,
        fileName: 'meetingType_' + new Date().toLocaleDateString() + '.xlsx'
      };
      this.grid.excelExport(excelExportProperties);
    }
  }
  get meetingTypecode(): AbstractControl { return this.orderForm.get('meetingTypecode'); }
  get meetingName(): AbstractControl { return this.orderForm.get('meetingName'); }


  get meetingTypeUno(): AbstractControl { return this.orderForm.get('meetingTypeUno'); }
  //get divisionUno(): AbstractControl { return this.orderForm.get('divisionUno'); }
}
