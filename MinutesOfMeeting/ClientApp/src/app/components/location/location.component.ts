import { Component, OnInit, ViewChild } from '@angular/core';
import { UtilityService } from '../../services/utility.service';
import { MasterMOMService } from '../../services/master-mom.service';
import { NgxSpinnerService } from "ngx-spinner";
import { MeetingLocation } from '../../models/MasterMOM';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { GridComponent, SaveEventArgs, DialogEditEventArgs, ExcelExportProperties, IFilter, FilterSettingsModel } from '@syncfusion/ej2-angular-grids';

import { ClickEventArgs } from '@syncfusion/ej2-navigations/src/toolbar';
import { ToastrUtilsService } from './../../services/toastr-utils.service';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  providers: [ToastrUtilsService]
})
export class LocationComponent implements OnInit {
  public data: any;
  //public division_data: any;
  public user_data: any;
  public editSettings: Object;
  public filterOptions: FilterSettingsModel;
  public filter: IFilter;
  public toolbar: string[];
  public pageSettings: Object;
  public orderData: object;
  //public Locationdata: object[];
  public orderForm: FormGroup;
  public submitClicked: boolean = false;
  public pageAction: string = 'add';
  public formatoptions: Object;
  @ViewChild('grid', { static: false }) public grid: GridComponent;
  public permission: number = 1;
  user: any = {};
  login_UserID: number; // number


  constructor(public utilityService: UtilityService,
    private toastr: ToastrUtilsService,
    public momMasterservice: MasterMOMService,
    private userService: UserService,
    private coolDialogs: NgxCoolDialogsService,
    private _router: Router,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    var user = this.userService.getUserInfo();
    this.login_UserID = user.id;

    var Role = Number(user.role);
    if (Role!=1) {
      this._router.navigate(['/dashboard'])
    }
    this.loadLocation();
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, mode: 'Dialog' };
    //this.toolbar = ['Add', 'Edit', 'Delete', 'Update', 'Cancel', 'Search', 'ExcelExport'];
    if (this.permission == 1) {
      this.toolbar = ['Add', 'Edit', 'Delete', 'Search', 'ExcelExport'];
    }
    else
      this.toolbar = ['Search', 'ExcelExport'];


    this.pageSettings = { pageSize: 20, pageSizes: true };
    this.filterOptions = {
      type: 'Menu'
    };
    this.filter = {
      type: 'CheckBox'
    };
    this.formatoptions = { type: 'date', format: 'dd/MM/yyyy' };
  }

  // Get Location list
  loadLocation() {
    
    this.spinner.show();
    let inpValue = new MeetingLocation();
    inpValue.languageUno = 1033;
    inpValue.userUno = this.login_UserID;
    
    this.momMasterservice.getLocation(inpValue)
      .subscribe(data => {
        
        this.data = data;
        //console.log(this.postData);
        this.spinner.hide();
      },
        () => {
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });
  }
  createFormGroup(data1: any): FormGroup {
    
    return new FormGroup({
      locationUno: new FormControl(data1.locationUno),
      locationCode: new FormControl(data1.locationCode, Validators.required),
      locationName: new FormControl(data1.locationName, Validators.required),

    });
  }

  actionBegin(args: SaveEventArgs): void {
    
    if (args.requestType === 'beginEdit' || args.requestType === 'add') {
      this.pageAction = (args.requestType === 'beginEdit') ? 'edit' : 'add';
      if (this.permission == 1) {
        this.submitClicked = false;
        this.orderForm = this.createFormGroup(args.rowData);
      }
      else {
        args.cancel = true;
      }
    }
    if (args.requestType === 'save') {
      this.submitClicked = true;
      //this.AddEditMeetingType(args.data)
      if (this.orderForm.valid) {
        args.data = this.orderForm.value;
        if (!this.AddEditLocation(args.data)) {
          this.submitClicked = false;
          args.cancel = true;
        }
        this.submitClicked = false;
      } else {
        args.cancel = true;
        //this.submitClicked = false;
      }
    }

    if (args.requestType === 'delete') {
      args.cancel = true;
      this.coolDialogs.confirm('Please confirm to delete?')
        .subscribe(res => {
          if (res) {
            this.DeleteLocation(args.data);
          }
        });
    }
  }
  actionComplete(args: DialogEditEventArgs): void {
    if ((args.requestType === 'beginEdit' || args.requestType === 'add')) {
      const dialog = args.dialog;
      dialog.width = 900;
      // Set initail Focus
      if (args.requestType === 'beginEdit') {
        let objdata: any = args.rowData;
        dialog.header = args.requestType === 'beginEdit' ? 'Details of ' + objdata.locationCode + ":" + objdata.locationName : '';
      }

    }

  }

  AddEditLocation(data1: any) {
    
    if (this.orderForm.invalid) {
      this.toastr.ErrorCustomMessage('Something went wrong');

      return false;
    }



    if (this.DuplicateNameCheck(this.utilityService.isStringNull(data1.locationCode), this.utilityService.isNumericNull(data1.locationUno))) {
      this.toastr.ErrorCustomMessage('Duplicates not allowed');
      return false;
    }
    else {
      
      let objMeetingLocation = new MeetingLocation();
      objMeetingLocation.actionType = this.pageAction;
      objMeetingLocation.locationUno = (this.pageAction == "add") ? 0 : this.utilityService.isNumericNull(data1.locationUno);
      objMeetingLocation.locationCode = this.utilityService.isStringNull(data1.locationCode);
      objMeetingLocation.locationName = this.utilityService.isStringNull(data1.locationName);
      //objMeetingType.meetingTypecode = this.utilityService.isStringNull(data1.meetingTypecode);
      //objMeetingType.meetingTypeUno = 0;
      objMeetingLocation.userUno = 1;

      
      this.momMasterservice.addEditLocation(objMeetingLocation)
        .subscribe(data => {
          
          if (data != null && data != undefined && data != '') {
            this.toastr.ErrorCustomMessage('Something went wrong');
          }
          else {
            this.toastr.SaveMessage();
            this.loadLocation();
          }


          //console.log(this.postData);
          this.spinner.hide();
        },
          () => {
            //Called when error
            this.spinner.hide();
          }
        ).add(() => {
          //Called when operation is complete (both success and error)
          this.spinner.hide();
        });
      return true;
    }
  }

  DuplicateNameCheck(locationCode: string, locationUno: number) {
    if (this.data != null && this.data != undefined) {
      var rtnValue = false;
      this.data.forEach((grdData) => {
        //  if (roles.some(role => role.includes(extLink.role))) {
        //    extLink.visible = true;
        //  }
        if (grdData.locationCode.toLocaleLowerCase() == locationCode.toLocaleLowerCase() && locationUno != grdData.locationUno) {
          rtnValue = true;
        }
      });

      return rtnValue;
    }
  }
  DeleteLocation(data1: any) {

    let objMeetingLocation = new MeetingLocation();
    objMeetingLocation.actionType = this.pageAction;
    objMeetingLocation.locationUno = data1[0].locationUno;
    objMeetingLocation.userUno = 1;
    //objDeaprtment.tenantUno = 1;
    
    this.momMasterservice.deleteLocation(objMeetingLocation)
      .subscribe(() => {
        
        this.toastr.DeleteMessage();
        this.loadLocation();
        this.spinner.hide();
      },
        () => {
          this.toastr.ErrorMessage();
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {

        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }

  public focusIn(target: HTMLElement): void {
    target.parentElement.classList.add('e-input-focus');
  }

  public focusOut(target: HTMLElement): void {
    target.parentElement.classList.remove('e-input-focus');
  }
  toolbarClick(args: ClickEventArgs): void {
    
    if (args.item.id === 'grdData_excelexport') {
      const excelExportProperties: ExcelExportProperties = {
        includeHiddenColumn: true,
        fileName: 'location_' + new Date().toLocaleDateString() + '.xlsx'
      };
      this.grid.excelExport(excelExportProperties);
    }
  }
  get locationCode(): AbstractControl { return this.orderForm.get('locationCode'); }
  get locationName(): AbstractControl { return this.orderForm.get('locationName'); }


  get locationUno(): AbstractControl { return this.orderForm.get('locationUno'); }
}
