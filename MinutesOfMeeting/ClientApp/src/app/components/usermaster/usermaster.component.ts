import { Component, OnInit, ViewChild } from '@angular/core';
import { UtilityService } from '../../services/utility.service';
import { MasterMOMService } from '../../services/master-mom.service';
import { NgxSpinnerService } from "ngx-spinner";
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { GridComponent, SaveEventArgs, DialogEditEventArgs, ExcelExportProperties } from '@syncfusion/ej2-angular-grids';
import { Query } from '@syncfusion/ej2-data';
import { ClickEventArgs } from '@syncfusion/ej2-navigations/src/toolbar';
import { usermaster, interaction, division } from '../../models/admin';
import { FilteringEventArgs } from '@syncfusion/ej2-dropdowns';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { ToastrUtilsService } from './../../services/toastr-utils.service';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { momDashboard_MyMeetings, momDashboard, GenInfoAtd } from '../../models/MOMForm';
//import html2canvas from 'html2canvas';
//import * as jsPDF from 'jspdf';
import { EncryptDecrypt } from '../../models/Common';
import { AdminService } from '../../services/admin.service';
import { Users } from '../../models/users';

@Component({
  selector: 'app-usermaster',
  templateUrl: './usermaster.component.html',
  styleUrls: ['./../userprofile/userprofile.component.css'],
  providers: [ToastrUtilsService]
})
export class UsermasterComponent implements OnInit {
  public data: any;
  public user_data: any;
  public division_data: any;
  public filterSettings: Object;
  public editSettings: Object;
  public toolbar: string[];
  public pageSettings: Object;
  public orderData: object;
  public interactionData: object[];

  meetingList: any = [];
  public usermasterForm: FormGroup;
  public submitClicked: boolean = false;
  public isView: boolean = false;
  public pageAction: string = 'add';
  public ntUser: string = '';
  public gridData: any;
  //public iseditmode: boolean = false;
  @ViewChild('grid', { static: false }) public grid: GridComponent;
  public permission: number = 1;
  user: any = {};
  login_UserID: number; // number
  tempuseraccess = false;
  todayTime = new Date().toLocaleTimeString()
  todayDate = new Date().toLocaleDateString();
  public objmomDashboard: momDashboard = new momDashboard();
  public lstMOMDashboard_TodayMeetings: momDashboard_MyMeetings[] = [];
  constructor(public utilityService: UtilityService,
    private toastr: ToastrUtilsService,
    public momMasterservice: MasterMOMService,
    private userService: UserService,
    private _router: Router,
    private activatedRoute: ActivatedRoute,
    private coolDialogs: NgxCoolDialogsService,
    private spinner: NgxSpinnerService,
    public restApi: AdminService) { }
  ngOnInit() {

    var user = this.userService.getUserInfo();
    this.login_UserID = user.id;
    this.getDivisions();
    this.loadNTUserMaster();
    this.loadUserMaster();
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, mode: 'Dialog' };
    var Role = Number(user.role);
    if (Role != 1) {
      this._router.navigate(['/dashboard'])
    }
    if (this.permission == 1) {
      this.toolbar = ['Add', 'Edit', 'Delete', 'Search', 'ExcelExport'];
    }
    else
      this.toolbar = ['Search', 'ExcelExport'];

    this.pageSettings = { pageSize: 20, pageSizes: true };
    this.filterSettings = { type: 'Excel' };

    this.activatedRoute.queryParams
      .subscribe(params => {

        if (params.isusermanage)
          this.tempuseraccess = true;
        else {
          this.tempuseraccess = false;
        }

      }
      );

  }
  loadUserMaster() {

    this.spinner.show();
    let inpValue = new usermaster();
    inpValue.languageUno = 1033;
    inpValue.userUno = this.login_UserID;
    inpValue.condition = 1;

    this.momMasterservice.getUserMasters(inpValue)
      .subscribe(data => {
        
        this.user_data = data;
        this.spinner.hide();
      },
        (error) => {

          this.spinner.hide();
        }
      ).add(() => {

        this.spinner.hide();
      });
  }

  // Get user list
  loadNTUserMaster() {
    this.spinner.show();
    let inpValue = new interaction();
    inpValue.languageUno = 1033;
    inpValue.condition = 5;

    
    this.momMasterservice.getUserMasters(inpValue)
      .subscribe(data => {
        

        this.spinner.hide();
      },
        (error) => {

          this.spinner.hide();
        }
      ).add(() => {

        this.spinner.hide();
      });
  }

  getDivisions() {

    let inpValue = new division();
    //inpValue.id = 3;
    inpValue.languageUno = 1033;
    inpValue.condition = 1;

    this.momMasterservice.getDivisions(inpValue)
      .subscribe(data => {
        this.division_data = data;
      },
        (error) => {
          //Called when error
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
      });
  }

  createFormGroup(data1: any): FormGroup {
    
    return new FormGroup({
      id: new FormControl(data1.id),
     
      prNumber: new FormControl(data1.prNumber, Validators.required),
      displayNameEn: new FormControl(data1.displayNameEn, Validators.required),
      displayNameAr: new FormControl(data1.displayNameAr, Validators.required),
      firstname: new FormControl(data1.firstname),
      lastname: new FormControl(data1.lastname),
      emailId: new FormControl(data1.emailId, Validators.email),
      designationEn: new FormControl(data1.designationEn),
      designationAr: new FormControl(data1.designationAr),
      divisionUno: new FormControl(data1.divisionUno, Validators.required),
    });
  }
  actionBegin(args: SaveEventArgs): void {
    
    if (args.requestType === 'beginEdit' || args.requestType === 'add') {
      this.pageAction = (args.requestType === 'beginEdit') ? 'edit' : 'add';
      if (this.permission == 1) {
        this.submitClicked = false;
        this.usermasterForm = this.createFormGroup(args.rowData);

      }
      else {
        args.cancel = true;
      }
    }
    if (args.requestType === 'save') {
      this.submitClicked = true;
      //this.AddEditMeetingType(args.data)
      if (this.usermasterForm.valid) {
        args.data = this.usermasterForm.value;
        if (!this.AddEditUserMaster(args.data)) {
          this.submitClicked = false;
          args.cancel = true;
        }
        this.submitClicked = false;
      } else {
        args.cancel = true;
        //this.submitClicked = false;
      }
    }


    if (args.requestType === 'delete') {
      args.cancel = true;
      this.coolDialogs.confirm('Please confirm to delete?')
        .subscribe(res => {
          if (res) {
            this.DeleteInteraction(args.data);
          }
        });
    }
  }

  actionComplete(args: DialogEditEventArgs): void {
    //this.iseditmode = false;
    if ((args.requestType === 'beginEdit' || args.requestType === 'add')) {
      const dialog = args.dialog;
      dialog.width = 900;
      // Set initail Focus
      if (args.requestType === 'beginEdit') {
        let objdata: any = args.rowData;
        dialog.header = args.requestType === 'beginEdit' ? 'Details of ' + objdata.displayNameEn : '';
        //this.iseditmode = true;
      }

    }

  }
  AddEditUserMaster(data1: any) {
    
    if (this.usermasterForm.invalid) {
      this.toastr.ErrorCustomMessage('Something went wrong');

      return false;
    }

    if (this.DuplicateNameCheck(this.utilityService.isStringNull(data1.emailId), data1.id)) {

      this.toastr.ErrorCustomMessage('Duplicates not allowed');
      return false;
    }
    else {
      
      let objUser = new usermaster();
      objUser.actionType = this.pageAction;
      objUser.id = (this.pageAction == "add") ? 0 : this.utilityService.isNumericNull(data1.id);
      objUser.ntUsername = this.utilityService.isStringNull(data1.emailId);
      objUser.prNumber = this.utilityService.isStringNull(data1.prNumber);
      objUser.displayNameEn = this.utilityService.isStringNull(data1.displayNameEn);
      objUser.displayNameAr = this.utilityService.isStringNull(data1.displayNameAr);
      objUser.emailId = this.utilityService.isStringNull(data1.emailId);
      objUser.firstname = this.utilityService.isStringNull(data1.firstname);
      objUser.lastname = this.utilityService.isStringNull(data1.lastname);
      objUser.designationEn = this.utilityService.isStringNull(data1.designationEn);
      objUser.designationAr = this.utilityService.isStringNull(data1.designationAr);
      objUser.divisionUno = this.utilityService.isNumericNull(data1.divisionUno);
      objUser.userUno = this.login_UserID;

      
      this.momMasterservice.addEditUserMaster(objUser)
        .subscribe(data => {
          
          if (data != null && data != undefined && data != '') {
            this.toastr.ErrorCustomMessage('Something went wrong');
          }
          else {
            this.toastr.SaveMessage();
            this.loadUserMaster();

          }
          this.spinner.hide();
        },
          () => {
            //Called when error
            this.spinner.hide();
          }
        ).add(() => {
          //Called when operation is complete (both success and error)
          this.spinner.hide();
        });
      return true;
    }
  }
  DuplicateNameCheck(emailId, id) {
    debugger;
    if (this.user_data) {
     
      let val = this.user_data.filter((grdData) => grdData.emailId.toLocaleLowerCase() == emailId.toLocaleLowerCase() && id != grdData.id)
      if (val && val.length>0)
      return true;

  
    }
    return false;
  }

  DeleteInteraction(data1: any) {

    let objUser = new usermaster();
    objUser.actionType = this.pageAction;
    objUser.id = data1[0].id;
    objUser.userUno = 1;

    
    this.momMasterservice.deleteUserMaster(objUser)
      .subscribe(() => {
        
        this.toastr.DeleteMessage();
        this.loadUserMaster();
        this.spinner.hide();
      },
        () => {
          this.toastr.ErrorMessage();
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {

        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }

  public focusIn(target: HTMLElement): void {
    target.parentElement.classList.add('e-input-focus');
  }

  public focusOut(target: HTMLElement): void {
    target.parentElement.classList.remove('e-input-focus');
  }
  toolbarClick(args: ClickEventArgs): void {
    
    if (args.item.id === 'grdData_excelexport') {
      const excelExportProperties: ExcelExportProperties = {
        includeHiddenColumn: true,
        fileName: 'user_' + new Date().toLocaleDateString() + '.xlsx'
      };
      this.grid.excelExport(excelExportProperties);
    }
  }

  loadView(data) {
    this.gridData = data;
    this.isView = true;
    this.loadGeneralInfo(this.gridData.iD)
  }

  // Get GeneralInfo list
  loadGeneralInfo(id) {
    let inpValue = new GenInfoAtd();
    inpValue.userUno = id;
    inpValue.actionType = "MEETING";
    inpValue.genInfoAtdUno = 0;
    inpValue.recordType = 1;
    
    this.momMasterservice.getGeneralInfoAtd(inpValue)
      .subscribe((data: any) => {
        this.meetingList = data;
        this.spinner.hide();
      },
        () => {
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }


  switchUser(id: any) {

    

    this.coolDialogs.confirm('Please confirm to switch the user?')
      .subscribe(res => {
        if (res) {
          let objusers = new Users;
          var objEncryptDecrypt = new EncryptDecrypt();

          var userName = objEncryptDecrypt.EncryptText(id); //this.f.username.value.trim();
          objusers.userName = userName;//this.usermodel.username;//userName;
          objusers.tenantUno = 1;
          this.restApi.GetUserInfo_ByUserID(objusers).subscribe(
            data => {

              let objdata: any = data;
              if (objdata != null && objdata.length > 0 && objdata[0] != null) {

                //Get User default information
                var userid = data[0].id;
                var displayname = (data[0].displayName != null && data[0].displayName != '') ? data[0].displayName : '';
                var username = (data[0].userName != null && data[0].userName != '') ? data[0].userName : '';
                var firstname = (data[0].firstName != null && data[0].firstName != '') ? data[0].firstName : '';
                var lastname = (data[0].lastname != null && data[0].lastname != '') ? data[0].lastname : '';
                var email = (data[0].emailId != null && data[0].emailId != '') ? data[0].emailId : '';
                var userPRnumber = (data[0].userPRnumber != null && data[0].userPRnumber != '') ? data[0].userPRnumber : '';
                var isTBT = (data[0].isTBT != null && data[0].isTBT != '') ? data[0].isTBT : false;
                var division = (data[0].division != null && data[0].division != '') ? data[0].division : '';
                var designation = (data[0].designation != null && data[0].designation != '') ? data[0].designation : '';
                var role = (data[0].role != null && data[0].role != '') ? Number(data[0].role) : 0;
                var picture = (data[0].picture != null && data[0].picture != '') ? data[0].picture : '';
                //Assign User value to the Session
                var struser = '{"id":"' + userid + '","displayname":"' + displayname + '","username":"' + username + '","firstname":"' + firstname + '","lastname":"' + lastname + '","email":"' + email + '","isTBT":"' + isTBT + '","division":"' + division + '","picture":"' + picture + '","role":"' + role + '","designation":"' + designation + '","userPRnumber":"' + userPRnumber + '"}';
                localStorage.removeItem('userinfo');
                localStorage.setItem('userinfo', struser);
                this.spinner.hide();
                this.toastr.SuccessCustomMessage('User switched..!');
                this._router.navigate(['/dashboard']);

              }
              else {
               
                this.spinner.hide();
                this.toastr.ErrorCustomMessage('Your Account is locked or not matched, contact your system administrator.')
              }
            },
            error => {
              this.spinner.hide();
            
              this.toastr.ErrorCustomMessage('SQL or Network connectivity error is occured,  contact your system administrator.');
              this.spinner.hide();
            },
            () => {
              this.spinner.hide();
            });
         
        }
      });


  }


  loadViewMeeting(id: any) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/view-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QMType: objEncryptDecrypt.EncryptText('MEETING') } });
  }

  //DownLoadFiles() {
   
  //  var temp = this;
  //  this.spinner.show();
  //  let filename = temp.gridData.firstname + '.pdf';
  //  setTimeout(() => {
  //    var htmlDoc = document.getElementById('user-profile-view');


  //    html2canvas(htmlDoc).then(canvas => {

  //      var pdf = new jsPDF('p', 'pt', [canvas.width, canvas.height]);
  //      var imgData = canvas.toDataURL("image/jpeg", 1.0);
  //      pdf.addImage(imgData, 0, 0, canvas.width, canvas.height);
  //      pdf.save(filename);
  //      temp.spinner.hide();
  //    });

  //  }, 3000);
  //}





  get prNumber(): AbstractControl { return this.usermasterForm.get('prNumber'); }
  get displayNameEn(): AbstractControl { return this.usermasterForm.get('displayNameEn'); }
  get displayNameAr(): AbstractControl { return this.usermasterForm.get('displayNameAr'); }
  get emailId(): AbstractControl { return this.usermasterForm.get('emailId'); }
  get divisionUno(): AbstractControl { return this.usermasterForm.get('divisionUno'); }
}




