import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../services/user.service';
import { alert, MeetingAppoinments } from '../../models/MOMForm';
import { NgxSpinnerService } from "ngx-spinner";
import { MasterMOMService } from '../../services/master-mom.service';
import { Router } from '@angular/router';
import { EncryptDecrypt } from './../../models/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  login_User: '';
  login_UserID: 0;
  alertList: any = [];
  alertCount: 0;
  isTBT = false;
  firstname = '';
  Role = 0;
  picture = '';

  IsIE = /msie\s|trident\//i.test(window.navigator.userAgent);
  public closeOnDocumentClick: boolean = false;
  constructor(private userService: UserService, private _router: Router, public momMasterservice: MasterMOMService,

    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    
    var user = this.userService.getUserInfo();
    this.login_User = user.displayname;
    this.firstname = user.firstname;

    this.isTBT = user.isTBT == 'false' ? false : true;;
    this.login_UserID = user.id;
    this.Role = Number(user.role);
    this.picture = user.picture;
    this.loadAlerts();
   
    this.openOutlookDialog();

    let menuicon = document.getElementsByClassName("gn-icon-menu")[0];
    menuicon.addEventListener("mouseover", IconmouseOver);

    function IconmouseOver() {
      menuwrapper.classList.add('gn-open-part')
    }

    let menuwrapper = document.getElementsByClassName("gn-menu-wrapper")[0];
    menuwrapper.addEventListener("mouseover", mouseOver);
    menuwrapper.addEventListener("mouseout", mouseOut);
    function mouseOver() {
      menuwrapper.classList.add('gn-open-all')
    }
    function mouseOut() {
      menuwrapper.classList.remove('gn-open-all')
      menuwrapper.classList.remove('gn-open-part')
    }
    // fixed header
    window.onscroll = function () { myFunction() };
   
    // Get the header
    var header = document.getElementById("myHeader");

    // Get the offset position of the navbar
   
   
    // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
    function myFunction() {
     
      if (window.pageYOffset >= header.offsetTop ) {
        header.classList.add("sticky");
      }
       if (window.pageYOffset <= 90)  {
        header.classList.remove("sticky");

      }
    }
  }

  loadValues(obj) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/view-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(obj.genInfoUno), QMType: objEncryptDecrypt.EncryptText(obj.type) } });
  }

  openOutlookDialog() {
    var user = this.userService.getUserInfo();
    var loginUsrEmail = user.email;
    let inputValue = new MeetingAppoinments();
    inputValue.email = loginUsrEmail;
    inputValue.dayCount = 0;
    this.momMasterservice.getMeetings(inputValue)
      .subscribe((data: any) => {
        this.userService.OutlookMeetingList = data;
      });
  }

  loadAlerts() {
    

    this.spinner.show();
    let inpValue = new alert();
    inpValue.userUno = this.login_UserID;
    inpValue.condition = 0;
    inpValue.startIndex = 1;
    inpValue.maxCount = 100;
    this.momMasterservice.getAlertList(inpValue)
      .subscribe((data: any) => {
        this.alertList = data;
        this.alertCount = this.alertList.length;
      },
        () => {
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }
  logout() {
    this.userService.logout();
  }

  markAsVisited() {
    
    let inpValue = new alert();
    inpValue.userUno = this.login_UserID;
    inpValue.condition = 0;
    inpValue.startIndex = 1;
    inpValue.maxCount = 100;
    this.momMasterservice.markAsVisited(inpValue)
      .subscribe((data: any) => {
        this.alertCount = 0;
      },
        () => {
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }

}
