import { Component, OnInit } from '@angular/core';
import { Users } from '../../models/users';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import { EncryptDecrypt } from '../../models/Common';
import { AdminService } from '../../services/admin.service';
import { ToastrUtilsService } from './../../services/toastr-utils.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserIdleService } from 'angular-user-idle';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ToastrUtilsService]
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted: boolean = false;
  GeneralInfoID = 0;
  invalid: boolean = false;
  user: Users;
  usermodel: any = {};
  isError: boolean = false;
  returnUrl: string = '';
  loading = false;
  languages: any[];
  signintext: string = 'SIGN IN';
  isPageLoad: boolean = false;
  isPageSwitch: boolean = false;
  userRights_data: any;
  pageType: string = "";
  generalInfoID: number = 0;
  constructor(private formBuilder: FormBuilder, private userIdle: UserIdleService, private toastr: ToastrUtilsService, private _activatedRoute: ActivatedRoute, private router: Router, private spinner: NgxSpinnerService, public restApi: AdminService) { }

  
  ngAfterViewInit(): void {

    this._activatedRoute.queryParams.subscribe((params: Params) => {
      let QMId = params['QMId'];
      let QMType = params['QMType'];
      var objEncryptDecrypt = new EncryptDecrypt();
      this.generalInfoID = (QMId  && QMId != '') ? Number(objEncryptDecrypt.DecryptText(QMId.replace(' ','+'))) : 0;
      this.pageType = (QMType  && QMType != '') ? objEncryptDecrypt.DecryptText(QMType) : 0;

      if (this.generalInfoID > 0 && localStorage.getItem('userinfo')) {
   
        this.router.navigate(['/view-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(this.generalInfoID), QMType: objEncryptDecrypt.EncryptText(this.pageType ) } });
      }
       
    });
    window.scroll(0, 0);
  }

  ngOnInit() {
    
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  get f() { return this.loginForm.controls; }
  onSubmit() {
    
    this.invalid = false;
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    else {
      if (this.f.username.value.trim().toLowerCase() != "" && this.f.password.value.trim().toLowerCase() != "") {
        this.AdLogin();

      }
      else {
        this.invalid = true;
      }
    }
  }

  //Authendicate Ad user
  AdLogin() {
    
    let objusers = new Users;
    this.isError = false;
    var objEncryptDecrypt = new EncryptDecrypt();
    
    var userName = objEncryptDecrypt.EncryptText(this.f.username.value.trim()); //this.f.username.value.trim();
    var password = objEncryptDecrypt.EncryptText(this.f.password.value.trim()); //this.f.password.value.trim();
    objusers.userName = userName;//this.usermodel.username;//userName;
    objusers.password = password;//this.usermodel.password;//password;
    objusers.tenantUno = 1;
    this.spinner.show();
    this.loading = true;
    this.restApi.AdUserAuthenticate(objusers).subscribe(
      data => {
        let objdata: any = data;
        this.user = objdata;
        this.isError = (objdata.error != "" && objdata.error != null) ? true : false;
        if (!this.isError) {
          this.spinner.hide();
          sessionStorage.setItem('accessinfo', '{"token":"' + objdata.token + '"}');
          objusers.emailId = objdata.emailId;
          //load user property values
          this.restApi.GetUserInfo_ByUserID(objusers).subscribe(
            data => {
              
              let objdata: any = data;
              if (objdata != null && objdata.length > 0 && objdata[0] != null) {

                //Get User default information
                var userid = data[0].id;
                var displayname = (data[0].displayName != null && data[0].displayName != '') ? data[0].displayName : '';
                var username = (data[0].userName != null && data[0].userName != '') ? data[0].userName : '';
                var firstname = (data[0].firstName != null && data[0].firstName != '') ? data[0].firstName : '';
                var lastname = (data[0].lastname != null && data[0].lastname != '') ? data[0].lastname : '';
                var email = (data[0].emailId != null && data[0].emailId != '') ? data[0].emailId : '';
                var userPRnumber = (data[0].userPRnumber != null && data[0].userPRnumber != '') ? data[0].userPRnumber : '';
                var isTBT = (data[0].isTBT != null && data[0].isTBT != '') ? data[0].isTBT : false;
                var division = (data[0].division != null && data[0].division != '') ? data[0].division : '';
                var designation = (data[0].designation != null && data[0].designation != '') ? data[0].designation : '';
                var role = (data[0].role != null && data[0].role != '') ? Number(data[0].role) : 0;
                var picture = (data[0].picture != null && data[0].picture != '') ? data[0].picture : '';
                //Assign User value to the Session
                var struser = '{"id":"' + userid + '","displayname":"' + displayname + '","username":"' + username + '","firstname":"' + firstname + '","lastname":"' + lastname + '","email":"' + email + '","isTBT":"' + isTBT + '","division":"' + division + '","picture":"' + picture + '","role":"' + role + '","designation":"' + designation + '","userPRnumber":"' + userPRnumber + '"}';
                localStorage.removeItem('userinfo');
                localStorage.setItem('userinfo', struser);
                var elementElBacktoTop = document.getElementById('elbacktotop') as HTMLInputElement;
                if (elementElBacktoTop != null) {
                  elementElBacktoTop.style.display = "block";
                }

                setTimeout(() => {
                  this.loading = false;
                  this.spinner.hide();
                  //this._router.navigate([this.returnUrl]);
                  this.initServiceCalls();
                }, 1000);

              }
              else {
                this.loading = false;
                this.spinner.hide();
                this.toastr.ErrorCustomMessage('Your Account is locked or not matched, contact your system administrator.')
              }
            },
            error => {
              this.loading = false;
              this.spinner.hide();
              // this._toastr.error('Invalid user name or password. Please try again later or contact your systems administrator.', '', { toastLife: 2000 });
              this.toastr.ErrorCustomMessage('SQL or Network connectivity error is occured,  contact your system administrator.');
              this.spinner.hide();
            },
            () => {
              //'onCompleted' callback.
              this.loading = false;
              this.spinner.hide();
            });

        }
        else {
          this.loading = false;
          this.spinner.hide();
          this.toastr.ErrorCustomMessage('Invalid user name or password. Please try again later or contact your system administrator.');
          this.spinner.hide();
        }
      },
      error => {
        this.loading = false;
        this.spinner.hide();
        console.log(error);
        // this._toastr.error('Invalid user name or password. Please try again later or contact your systems administrator.', '', { toastLife: 2000 });
        this.toastr.ErrorCustomMessage('Invalid user name or password. Please try again later or contact your system administrator.');
        this.spinner.hide();
      },
      () => {
        //'onCompleted' callback.
        this.loading = false;
        this.spinner.hide();
      })



  }
  initServiceCalls() {
    this.userIdle.startWatching();
    var objEncryptDecrypt = new EncryptDecrypt();
    if (this.generalInfoID > 0 && localStorage.getItem('userinfo')) {
      var objEncryptDecrypt = new EncryptDecrypt();
      this.router.navigate(['/view-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(this.generalInfoID), QMType: objEncryptDecrypt.EncryptText(this.pageType) } });
    }
    else {
      this.router.navigate(['/dashboard']);
    }
   
  }


}
