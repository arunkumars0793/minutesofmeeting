import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { UtilityService } from '../../services/utility.service';
import { UserService } from '../../services/user.service';
import { MasterMOMService } from '../../services/master-mom.service';
import { NgxSpinnerService } from "ngx-spinner";
import { momDashboard, momDashboard_MyMeetings, AttendeesAtd, momDashboard_PartofMeetings, MeetingAppoinments } from '../../models/MOMForm';
import { Router } from '@angular/router';
import { EncryptDecrypt } from './../../models/common';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})



export class DashboardComponent implements OnInit {

  user: any = {};
  login_UserID: number;
  prNumber: string = '';
  isTBT = false;
  public objmomDashboard: momDashboard = new momDashboard();
  public lstMOMDashboard_MyMeetings: momDashboard_MyMeetings[] = [];
  public lstMOMDashboard_PartofMeetings: momDashboard_PartofMeetings[] = [];
  public lstMOMDashboard_TodayMeetings: momDashboard_MyMeetings[] = [];
  public commentList: any = [];
  searchForm: FormGroup;
  searchForm2: FormGroup;
  submitted = false;
  submitted2 = false;
  @Output() searchEvent = new EventEmitter();
  saveButtonText: string = 'Submit';
  todayTime = new Date().toLocaleTimeString()
  todayDate = new Date().toLocaleDateString();
  search1: string;
  public data: Object[];

  constructor(private formBuilder: FormBuilder, public momMasterservice: MasterMOMService,
    public utilityService: UtilityService, private _router: Router, private spinner: NgxSpinnerService,
    private userService: UserService) {
  }

  ngOnInit() {

    this.user = this.userService.getUserInfo();
    this.login_UserID = this.user.id;
    this.prNumber = this.user.userPRnumber;
    this.isTBT = this.user.isTBT == 'false' ? false : true;
    this.loadGeneralInfo();
    this.getSelUserAtd();
    this.searchForm2 = this.formBuilder.group({
      searchText: [null, Validators.required],

    });
    this.searchForm = this.formBuilder.group({
      searchText: [null, Validators.required],

    });

  }

  get t() { return this.searchForm.controls; }

  get p() { return this.searchForm2.controls; }
  search() {
    this.submitted = true;
    var searchText = this.searchForm.value.searchText;
    if (searchText != undefined && searchText.trim() != "") {
      var objEncryptDecrypt = new EncryptDecrypt();
      this._router.navigate(['search'], { queryParams: { QText: objEncryptDecrypt.EncryptText(searchText) } });
    }

  }

  search2() {
    this.submitted2 = true;
    var searchText = this.searchForm2.value.searchText;
    if (searchText != undefined && searchText.trim() != "") {
      var objEncryptDecrypt = new EncryptDecrypt();
      this._router.navigate(['search'], { queryParams: { QText: objEncryptDecrypt.EncryptText(searchText) } });
    }

  }
  createFormGroup(data1: any): FormGroup {
    return new FormGroup({
      searchText: new FormControl(data1.searchText),
    });
  }

 


  loadGeneralInfo() {

    let inpValue = new momDashboard();
    inpValue.userUno = this.login_UserID;



    this.momMasterservice.getMOMDashboard(inpValue)
      .subscribe((data: any) => {
        this.objmomDashboard = data;
        this.lstMOMDashboard_MyMeetings = data.lstMOMDashboard_MyMeetings;
        this.lstMOMDashboard_PartofMeetings = data.lstMOMDashboard_PartofMeetings;
        this.lstMOMDashboard_TodayMeetings = data.lstMOMDashboard_TodayMeetings;
        if (this.objmomDashboard.noOfDraftMeeting > 0 || this.objmomDashboard.noOfNotPublishMeeting > 0 || this.objmomDashboard.noOfPublishedMeeting > 0)
          this.data = [
            { 'x': 'Draft', y: this.objmomDashboard.noOfDraftMeeting, text: 'Draft: ' + this.objmomDashboard.noOfDraftMeeting + '%', r: '100' },
            { 'x': 'Not Published', y: this.objmomDashboard.noOfNotPublishMeeting, text: 'Not Published: ' + this.objmomDashboard.noOfNotPublishMeeting + '%', r: '100' },
            { 'x': 'Published', y: this.objmomDashboard.noOfPublishedMeeting, text: 'Published: ' + this.objmomDashboard.noOfPublishedMeeting + '%', r: '100' }
          ]
        this.spinner.hide();
      },
        (error) => {
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }
  loadUpdateMeeting(id: any) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/add-update-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QType: objEncryptDecrypt.EncryptText('MEETING') } });
  }

  loadAddMeeting() {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/add-update-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(0), QType: objEncryptDecrypt.EncryptText('MEETING') } });
  }

  loadViewMeeting(id: any) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/view-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QMType: objEncryptDecrypt.EncryptText('MEETING') } });
  }

  loadAttList(val = 0) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/attendance-list'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(val) } });
  }

  loadMeetingList(val = 0) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/meeting-list'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(val) } });
  }

  loadTBTList(val = 0) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/tbd-list'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(val) } });
  }

  //Get User List
  getSelUserAtd() {

    let inpValue = new AttendeesAtd();
    inpValue.userUno = this.login_UserID;
    if (this.userService.UserList.length == 0) {
      this.momMasterservice.getUserList(inpValue)
        .subscribe(data => {
          this.userService.UserList = data;

        },
          () => {

          }
        ).add(() => {
          //Called when operation is complete (both success and error)
        });
    }
  }
}
