import { Component, OnInit, ViewChild } from '@angular/core';
import { GridComponent, QueryCellInfoEventArgs } from '@syncfusion/ej2-angular-grids';
import { UtilityService } from './../../services/utility.service';
import { MasterMOMService } from './../../services/master-mom.service';
import { NgxSpinnerService } from "ngx-spinner";
import { GenInfoAtd, cmtObject, MeetingAttendee, AgendaDetails, AttendeesAtd } from './../../models/MOMForm';
import { FormGroup } from '@angular/forms';
import { EditSettingsModel, FilterSettingsModel, IFilter, ColumnMenuService } from '@syncfusion/ej2-angular-grids';
import { ToastrUtilsService } from './../../services/toastr-utils.service';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { EncryptDecrypt } from '../../models/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from './../../services/user.service';
import { DialogComponent, Tooltip } from '@syncfusion/ej2-angular-popups';
import SignaturePad from 'signature_pad';
import pdfMake from 'pdfmake/build/pdfmake.js';
import pdfFonts from 'pdfmake/build/vfs_fonts.js';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { formatDate } from '@angular/common';
@Component({
  selector: 'app-meeting-list',
  templateUrl: './meeting-list.component.html',
  providers: [ToastrUtilsService, ColumnMenuService]
})
export class MeetingListComponent implements OnInit {
  public data: any;
  generalInfoAtd: FormGroup;
  public getvalue: any;
  commentText = '';
  GeneralInfoID = 0;
  public user_data: any;
  signText = "";
  recordType = 0;
  public generalInfoData: GenInfoAtd = new GenInfoAtd();
 
  public attendeesList: any = [];
  public commentList: any = [];
  public agendaList: any = [];
  public filterOptions: FilterSettingsModel;
  public filter: IFilter;
  public isApproved: boolean = false;
  public isGrid: boolean = true;
  public toolbar: string[];
  public pageSettings: Object;
  public meetingTypeUno_data: any;
  public locationUno_data: any
  public meetingRoomUno_data: any;
  public submitClicked: boolean = false;
  public pageAction: string = 'add';
  public formatoptions: Object;
  @ViewChild('signatureLastPad', { static: false }) sLastPad;
  @ViewChild('SignatureMainDialog', { static: false }) SignatureMainDialog: DialogComponent;
  signLastForm: FormGroup;
  signatureLastPad: any;
  public editSettings: EditSettingsModel; 
  @ViewChild('grid', { static: false }) public grid: GridComponent;
  @ViewChild('commentDialog', { static: false }) commentDialog: DialogComponent;
  @ViewChild("imageUrlMain", { static: false } as any) imageLastFile: any;

  public permission: number = 1;
  user: any = {};
  login_UserID: number; // number
  public show: boolean = true;
  public popoverTitle: string = 'Popover title';
  public popoverMessage: string = 'Popover description';
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;
  objmodel: any;
  public filterParams: object;
  public genInfoAtdUno: any;
  constructor(public utilityService: UtilityService,
    private toastr: ToastrUtilsService,
    public momMasterservice: MasterMOMService,
    private coolDialogs: NgxCoolDialogsService,
    private spinner: NgxSpinnerService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private userService: UserService) {

  }

  ngOnInit() {
    var temthis = this;
    var user = this.userService.getUserInfo();
    this.login_UserID = user.id;
    this.pageSettings = { pageSize: 50, pageSizes: [10, 50, 100, 500, 1000] };
    this.filterOptions = {
      type: 'Excel',
      
    };
   
    this.filter = {
      type: 'CheckBox'
    };
    setTimeout(function () {
      temthis.signatureLastPad = new SignaturePad(temthis.sLastPad.nativeElement);
    }, 500);


  }

  AddComment() {
    if (this.commentText == '') {
      this.toastr.ErrorCustomMessage('Please enter the comment.');
      return;
    }
    let inpValue = new cmtObject();
    inpValue.userId = this.login_UserID;
    inpValue.genralInfoId = this.GeneralInfoID;
    inpValue.cmdId = 0;
    inpValue.actionType = "MEETING";
    inpValue.cmdText = this.commentText;
    this.momMasterservice.insertAtdComment(inpValue)
      .subscribe(data => {
        this.spinner.hide();
        this.toastr.SaveMessage();
        this.loadAtdComment(this.GeneralInfoID);
        this.commentText = '';
      },
        () => {
          this.spinner.hide();
          this.toastr.ErrorMessage();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }
  delAtdComment(id: any) {
    let inpValue = new cmtObject();
    inpValue.userId = this.login_UserID;

    inpValue.genralInfoId = this.GeneralInfoID;
    inpValue.cmdId = id;
    inpValue.actionType = 'MEETING';
    
    this.coolDialogs.confirm('Please confirm to delete?')
      .subscribe(res => {
        if (res) {
          this.momMasterservice.delAtdComment(inpValue)
            .subscribe(data => {
              this.spinner.hide();
              this.toastr.DeleteMessage();
              this.loadAtdComment(this.GeneralInfoID);
            },
              () => {
                this.spinner.hide();
                this.toastr.ErrorMessage();
              }
            ).add(() => {
              //Called when operation is complete (both success and error)
              this.spinner.hide();
            });
        }
      });


  }

  loadAtdComment(id) {
    
    this.GeneralInfoID = id;
    this.spinner.show();
    let genfoList = this.data.filter(item => item.genInfoAtdUno == id);
    this.isApproved = genfoList[0].approvalStatus != 'Published' ? false : true;
    let inpValue = new cmtObject();
    inpValue.userId = this.login_UserID;
    inpValue.genralInfoId = this.GeneralInfoID;
    inpValue.cmdId = 0;
    inpValue.startIndex = 1;
    inpValue.maxCount = 100;
    inpValue.actionType = "MEETING";
    this.momMasterservice.getAtdComment(inpValue)
      .subscribe((data: any) => {
        this.commentList = data;
        this.commentDialog.show();
      },
        (error) => {
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }
  ngAfterViewInit(): void {
    
    var objEncryptDecrypt = new EncryptDecrypt();
    this._activatedRoute.queryParams.subscribe((params: Params) => {
      let QMId = params['QMId'];
      this.recordType = (QMId != null && QMId != '') ? Number(objEncryptDecrypt.DecryptText(QMId)) : 0;
      this.loadGeneralInfo();
    });
  }
  actionComplete(args) {
    if ((args.requestType === 'beginEdit' || args.requestType === 'add')) {
      let dialog = args.dialog;
      // change the header of the dialog
      dialog.header = args.requestType === 'beginEdit' ? 'Record of ' + args.rowData['genInfoAtdUno'] : 'NewGenInfoAtd';
    }
  }

  loadUpdate(id: any) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/add-update-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QType: objEncryptDecrypt.EncryptText('MEETING') } });
  }

  loadAdd() {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/add-update-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(0), QType: objEncryptDecrypt.EncryptText('MEETING') } });
  }

  loadViewAtd(id: any) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/view-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QMType: objEncryptDecrypt.EncryptText('MEETING') } });
  }


  delAtt(id: any) {

    let inpValue = new GenInfoAtd();
    inpValue.userUno = this.login_UserID;
    inpValue.genInfoAtdUno = id;
    inpValue.actionType = "MEETING";
    
    this.coolDialogs.confirm('Please confirm to delete?')
      .subscribe(res => {
        if (res) {
          this.momMasterservice.delGeneralInfoAtd(inpValue)
            .subscribe(data => {
              this.spinner.hide();
              this.toastr.DeleteMessage();
              this.loadGeneralInfo();
            },
              () => {
                this.toastr.ErrorMessage();
                this.spinner.hide();
              }
            ).add(() => {
              this.spinner.hide();
            });
        }
      });


  }

  followupMeeting(id: any) {

    let inpValue = new GenInfoAtd();
    inpValue.userUno = this.login_UserID;
    inpValue.genInfoAtdUno = id;
    inpValue.actionType = "MEETING";
    
    this.coolDialogs.confirm('Please confirm to create followup meeting?')
      .subscribe(res => {
        if (res) {
          this.momMasterservice.createFollowupMeeting(inpValue)
            .subscribe((data: any) => {
              this.spinner.hide();
              this.toastr.SuccessCustomMessage('Successfully Followup Created');
              var objEncryptDecrypt = new EncryptDecrypt();
              this._router.navigate(['/add-update-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(data.genInfoAtdUno), QType: objEncryptDecrypt.EncryptText('MEETING') } });
            },
              () => {
                this.toastr.ErrorMessage();
                this.spinner.hide();
              }
            ).add(() => {
              this.spinner.hide();
            });
        }
      });


  }




  // Get GeneralInfo list
  loadGeneralInfo() {
    let inpValue = new GenInfoAtd();
    inpValue.userUno = this.login_UserID;
    inpValue.actionType = "MEETING";
    inpValue.genInfoAtdUno = 0;
    inpValue.recordType = this.recordType;
    
    this.momMasterservice.getGeneralInfoAtd(inpValue)
      .subscribe((data: any) => {
        this.data = data;
        this.spinner.hide();
      },
        () => {
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }
  clickSign(data) {

    this.signText = "";
    this.generalInfoData = data;
    if (this.login_UserID == data.reviewerId) {
      this.signText = "Reviewer";
      this.signatureLastPad.fromDataURL(this.generalInfoData.reviewerSign);
      this.signatureLastPad.points.push({ time: 0, x: 0, y: 0 })
    }
    if (this.login_UserID == data.approverId) {
      this.signText = "Approver";
      this.signatureLastPad.fromDataURL(this.generalInfoData.approverSign);
      this.signatureLastPad.points.push({ time: 0, x: 0, y: 0 })
    }
    this.signatureLastPad.clear()
    this.SignatureMainDialog.show();
    this.loadAgendaDetails();
    this.loadAttendesAtd();
  }

  ClearLastSignature() {
    this.signatureLastPad.clear();
  }
  handleMainChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    let videoFile = e.target.files[0];
    var videosize = Math.round((videoFile.size / 1024));
    if (videosize > 20485760) {
      this.toastr.WarnMessage("Signature Image is too big", 1500);
      return;
    }
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      this.toastr.ErrorCustomMessage('invalid format');
      return;
    }
    reader.onload = this._handleMainReaderLoaded.bind(this);
    reader.readAsDataURL(file);

  }
  _handleMainReaderLoaded(e) {
    let reader = e.target;
    this.ClearLastSignature();
    this.signatureLastPad.fromDataURL(reader.result);
    this.signatureLastPad.points.push({ time: 0, x: 0, y: 0 })
    this.imageLastFile.nativeElement.value = '';
  }


  saveApprovalSignature() {
    let temp = this;
    const dataURL = this.signatureLastPad.toDataURL('image/png');
    if (this.signatureLastPad.points.length == 0) {
      this.toastr.ErrorCustomMessage('Invalid signature');
      return;
    }
    var genInfo = this.generalInfoData;
    genInfo.actionType = 'MEETING';
    genInfo.meetingDateAtd = new Date()
    genInfo.startTimeAtd = new Date()
    genInfo.endTimeAtd = new Date()
    if (this.login_UserID == this.generalInfoData.reviewerId) {
      genInfo.callFrom = "REVIEW";
      genInfo.reviewerSign = dataURL;
    }
    if (this.login_UserID == this.generalInfoData.approverId) {
      genInfo.callFrom = "APPROVE";
      genInfo.approverSign = dataURL;
    }

    genInfo.userUno = this.login_UserID;
    this.spinner.show();
    pdfMake.fonts = {
      DroidKufi: {
        normal: 'DroidKufi-Regular.ttf',
        bold: ' DroidKufi-Regular.ttf',
        italics: 'DroidKufi-Regular.ttf',
        bolditalics: 'DroidKufi-Regular.ttf'
      },
      NotoKufiArabic: {
        normal: 'NotoKufiArabic-Regular.ttf',
        bold: ' NotoKufiArabic-Regular.ttf',
        italics: 'NotoKufiArabic-Regular.ttf',
        bolditalics: 'NotoKufiArabic-Regular.ttf'

      },
      Roboto: {
        normal: 'Roboto-Regular.ttf',
        bold: 'Roboto-Medium.ttf',
        italics: 'Roboto-Italic.ttf',
        bolditalics: 'Roboto-Italic.ttf'
      },
    };
    pdfMake.createPdf(temp.getMeetingDefinition()).getBase64(function (encodedString) {
      genInfo.pdfContent = encodedString;
      temp.momMasterservice.addEditGeneralInfoAtd(genInfo)
        .subscribe(data => {
          temp.toastr.SaveMessage();
          temp.loadGeneralInfo();
          temp.SignatureMainDialog.hide();
          temp.signatureLastPad.clear();
          temp.spinner.hide();
        },
          () => {
            temp.spinner.hide();
            temp.toastr.ErrorMessage();
          }
        )

    });

   
  }

  rowSelected(args) {
    
    this.loadViewAtd(args.rowData.genInfoAtdUno);
  }

  tooltip(args: QueryCellInfoEventArgs) {

    if (args.data[args.column.field] && args.data[args.column.field] != '') {
      const tooltip: Tooltip = new Tooltip({
        content: args.data[args.column.field] ? args.data[args.column.field].toString() : ''
      }, args.cell as HTMLTableCellElement);
    }
  }

  findMainAgenda() {
    return this.agendaList.filter(item => item.agendaParentUno == 0);
  }


  findsubDetails(val: any) {
    return this.agendaList.filter(item => item.agendaParentUno == val.agendaUno);
  }

  loadAgendaDetails() {
    let inpValue = new AgendaDetails();
    inpValue.genInfoAtdUno = this.generalInfoData.genInfoAtdUno;
    inpValue.userID = this.login_UserID;
    inpValue.startIndex = 1;
    inpValue.maxCount = 100;
    inpValue.actionType = 'MEETING';
    this.spinner.show();
    //
    this.momMasterservice.getAgendaDetails(inpValue)
      .subscribe(data => {

        this.agendaList = data;
        this.spinner.hide();
      },
        () => {
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });
  }

  loadAttendesAtd() {
    this.spinner.show();
    let inpValue = new AttendeesAtd();
    inpValue.userUno = this.login_UserID;
    inpValue.genInfoAtdUno = this.generalInfoData.genInfoAtdUno;
    inpValue.startIndex = 1;
    inpValue.maxCount = 1000;
    inpValue.actionType ='MEETING';

    this.momMasterservice.getAttendeesAtd(inpValue)
      .subscribe(data => {

        this.attendeesList = data;


        this.spinner.hide();
      },
        () => {
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }

  DayAsString(dayIndex) {
    var weekdays = new Array(7);
    weekdays[0] = "Sunday";
    weekdays[1] = "Monday";
    weekdays[2] = "Tuesday";
    weekdays[3] = "Wednesday";
    weekdays[4] = "Thursday";
    weekdays[5] = "Friday";
    weekdays[6] = "Saturday";

    return weekdays[dayIndex];
  }

  CoverttoTime(time24h) {
    time24h = new Date(time24h).toLocaleTimeString();
    if (time24h == undefined || time24h == null) { return time24h }
    else {
      var timex = time24h.split(':');
      timex[0] = timex[0].split(' ').length > 1 ? timex[0].split(' ')[1] : timex[0];
      if (timex[0] !== undefined && timex[1] !== undefined) {
        var hor = parseInt(timex[0]) > 12 ? (parseInt(timex[0]) - 12) : timex[0];
        var minu = timex[1];
        var merid = parseInt(timex[0]) < 12 ? 'AM' : 'PM';
        return hor + ':' + minu + ' ' + merid;


      }
    }
  }


  buildAgendaTable(data, columns) {
    var body = [];
    var temp = this;
    body.push([{ text: 'S.N.', style: 'tableHeader', bold: true, fillColor: '#dddddd', border: [true, true, true, true] }, { text: 'Discussion Points', style: 'tableHeader', bold: true, fillColor: '#dddddd', border: [true, true, true, true] }, { text: 'Responsibility', style: 'tableHeader', bold: true, fillColor: '#dddddd', border: [true, true, true, true] },
    { text: 'Due Date ', style: 'tableHeader', bold: true, fillColor: '#dddddd', border: [true, true, true, true] }]);
    if (data && data.length == 0) {
      body.push([{ text: '', style: 'tableHeader' }, { text: 'No Data Found', style: 'tableHeader' }, { text: '', style: 'tableHeader' }, { text: '', style: 'tableHeader' }]);

    }
    data.forEach(function (row, idx) {
      var dataRow = [];
      dataRow.push((idx + 1).toString());
      body.push([{ text: (idx + 1).toString() }, { text: row[columns[0]].toString(), style: temp.getLanguageStyle(row[columns[0]].toString()) },
      { text: row[columns[1]].toString(), style: temp.getLanguageStyle(row[columns[1]].toString()) }, { text: formatDate(row[columns[2]].toString(), 'dd/MM/yyyy', 'en-US') }]);


      temp.findsubDetails(row).forEach(function (subrow, subidx) {
        var dataRow = [];
        dataRow.push(((idx + 1) + '.' + (subidx + 1)).toString());
        body.push([{ text: ((idx + 1) + '.' + (subidx + 1)).toString() }, { text: subrow[columns[0]].toString(), style: temp.getLanguageStyle(subrow[columns[0]].toString()) },
        { text: subrow[columns[1]].toString(), style: temp.getLanguageStyle(subrow[columns[1]].toString()) }, { text: formatDate(row[columns[2]].toString(), 'dd/MM/yyyy', 'en-US') }]);

      });
    });

    return body;
  }


  getLanguageStyle(value: string) {
    let strLanguage = 'en';
    if (value !== null && value !== '') {
      let regx = /^[A-Za-z0-9 .,_-]+$/;
      value = value.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '')
      value = value.replace(/\n/g, "");
      value = value.replace(/\r/g, "");
      if (regx.test(value.replace(/[!@#?=$%^&*(){}|\\[\]]/g, ""))) {
        strLanguage = "en";
      } else {
        strLanguage = "ar";
      }
    } else {
      strLanguage = "en";
    }

    return (strLanguage == 'en') ? 'en_lang' : 'ar_lang';
  }

  buildAttendeeTable(data, columns) {
    var body = [];
    var temp = this;
    body.push([{ text: 'Name', style: 'tableHeader', bold: true, fillColor: '#dddddd', border: [true, true, true, true] }, { text: 'Attendance', style: 'tableHeader', bold: true, fillColor: '#dddddd', border: [true, true, true, true] }]);
    if (data && data.length == 0) {
      body.push([{ text: 'No Data Found', style: 'tableHeader' }, { text: '', style: 'tableHeader' }]);

    }
    data.forEach(function (row) {
      body.push([{ text: row[columns[0]].toString(), style: temp.getLanguageStyle(row[columns[0]].toString()) }, { text: row[columns[1]].toString(), style: temp.getLanguageStyle(row[columns[1]].toString()) }]);


    });

    return body;
  }

  getMeetingDefinition() {
    let temp = this;
    return {
      pageMargins: [30, 100, 30, 60],

      footer: function (currentPage, pageCount) {
        return [
          {
            text: 'Page ' + currentPage.toString() + ' of ' + pageCount, alignment: 'right', fontSize: 8, color: 'grey', margin: [30, 20, 30, -35],
          },

          { text: 'Created Date: ' + formatDate(temp.generalInfoData.createDate, 'dd/MM/yyyy', 'en-US'), alignment: 'left', margin: [30, 20, 30, 20], color: 'grey', fontSize: 8, },
          { canvas: [{ type: 'line', x1: 0, y1: 0, x2: 600, y2: 0, lineWidth: 0.5, color: 'grey', }] },
          {
            text: '© 2020 Dubai Electricity and Water Authority. All Rights Reserved.', alignment: 'center', fontSize: 8, color: 'grey', border: [false, true, false, false], margin: [0, 5, 0, 10], width: ['*']
          }];

      },

      header: {
        style: 'tableExample',
        margin: [30, 20, 30, 20],

        table: {
          widths: ['*', '*', '*'],

          body: [
            [
              {
                border: [false, false, false, false],
                image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOgAAABkCAMAAAC7BYzpAAADAFBMVEUAAADMMzPcOUDbOkDcOkDbMT3SLTzaOkDVMzzWMz3cO0HcO0HbOj+AAADbO0DbOkDbOT/cOj/bOkDbOkDbO0DGOTnbO0HbOkHbOkHcO0DROjrbO0DcOkHWOkCqAADXNjbVNUDbOkHbOkDcO0HYOj7aOj/YNj7RLi7bOkHbOj/PMEDcOkDbOkDcO0DbO0DbOjrbOT7bOkHbOkDcOkHcOkDcOkDbOkDbN0DaOUDcOkDbOj/bOj8AAADcO0DbOD7bOkDbO0C/AEDZOkDcNT7bO0HbOkHXOEDcOkDbO0DbO0DcOUDbO0HbOkHMMzPbN0DaNTzbJCTbOUHaOkHcOkHaOkHcOUDbOUDbOUHZOT3bO0DMMzPbOkDcOUDbO0HZM0DbOkHbO0HYOzvaOD7aOT/ZOz/bO0DcO0DaO0DbOUDbOkDcOkDbO0DaOz/aOT7bO0HbOkHXOT7bOT/cO0DaOEDcOkDcO0HbOkDbOj7bO0DcOz/bOz7cOkHcOkHcOkDbOT7bOkDbOkDbOD3cO0HbOkDbOkHcO0DcO0DcO0DbO0HbOkDbO0DcO0DbOkHbOkDcOkHZOUDcOUHbO0HcOkDbO0HaOD6/IEDbOkDaOkHaOkDbO0DbO0HbOz/cOkHcOUDbO0DcOkDaNz/bO0DcOT7aOT/VK0DcO0HbO0DcOkDaOkDcOj/bOj/cOUDbOkHVOTnVKyvcOkDYOzvbOkDbOj/YOEHbOkHbOkHcOkDcO0HcOkDcO0DaO0DcOkHYNz3bOz/aOkHbNzfbOUHZOz7YO0DcOkDbOkDbOkDbOkHbOkHYOzvbOkHbO0DbOkDaOj7cOkDZOT/TNzfbOkDbOEDZOEDWOjrZNjzWNz7bO0DZOTnaOUDcN0HZOUHcOkHcOj/cOkHbOkHbOkDZOD/bOkHcOkHbOkHbOkDbOkDaOkDbOD/cO0DaOEHZOj/bOUDcOj/bOkDaOkHbOUDbO0DXNjzcOkHbO0HbNz3bNTvbOkHaOUDZN0DcOT7cO0DcO0HbO0HbO0HbOkDcO0AlzwCgAAABAHRSTlMABXTXvBURYB4ZzP+NAlukVWW4+scJ1XK5rha//SwDExhqvvxCYSEL+ZUQ2uO2ZCNimsbp/t93HG/ChTkB91aMyARYHaXkIHPeh1D48Qo4IgdHkthLJHiONpQPsGzQFLH0Jyl9PbrvaGvAypx1WqnlLYHuRLTh20ayQU69Zqcxo4gy2fKWmMTmoVyTw0+v9SiK3faqUgibdoT7hnnol+qQRY86WQyt89IwkWlIyRIGgxqAXTvBzcvRe+eLpi5xbg4/SjTTz9ae3A3s4qw+n1EXf0BfHy8l6xtnM0PgbZmdVEnttaKrqHxNVzc1cInOU2O3JvB+KivFTDxeu4J61LOgzm6e7QAAFXhJREFUeAHt2wFcTNn/N/BPSPhIywDMVisSWFYDMkkEJEKArbJZACgAAGShAATZILAUQLBlFwEAAL8VANjdBWB3n989Z2ZmpmYCeJ7///FewN77Ovdzzveee1/3nosP7TObTJmz2OJ/n6x22WAqew6SOWGZfS6HL/A/kW3uPKq8MJUvf94CBVkIFhUuQqq/xIfg6OSUD5+I81dFXUgWg6niLq4o4VYSlpQqXaasG7/GeypX/psK7hqSFSvh46tcpaqaiiww40H3alrP6rDEq4Y3vmZNvIda5X1q06gYPq46dfNqKNSrnw1mGpC+Hg39YEkNemgbsTHekX+Tps1oLgAfUfMWOSjVbtkK6bQmmbkNLGlLFvo2EO8kqHhwCNNph4/lu9BASu4l22thSRU38nvLO3cgNfWRRoGOjngN506dXWjk2aVrt+52PcieWnwUvXrnpxTWNdwRVmj79GVPWNSrH6nqD53sAwYCGEQORkayDimipkHPoZ2GeUMYzhH4CEZ6jKI0eniADazSjhlr8/04WNYmghwPnR9E6AkTJgbDKptJk92oF9mnUxSMMrm54gPTtpoyldK06TNmIgOOs/J4I/tsWDZ2zqDoudCZR84vhQVwrGP1RMkZQ72KC3/0N++D/LEfOOWiln0puQUvLoEM5SvUZQmssR87XYvQ6tDRNlkaF4o5sMK/+0TqLeu9HGl1cy+FD8e+/YralKLz2JWC3spVsMT5p8DVsGZNj84zAX9vGOXui3hYlBDvTp1ma6MsbRC3Dh+Kdn3TnpTUnTsGwcQG1Q9Ir1XtjUGwxqnqpjZIQ7t5/RZYsNxBRSlx63JYtK2CVnYf3tf2KX2pk7dKAsw4uVCzA2n97FY0Cdb0mroxK9JZN6s40kkuSMm3x05HWJas/kUciINLW7yPWpV+pc6uSuknix9Ici7MOPdha29Ykytkg6VOyBeRjLRiKe2ukhVW1dgj2kyZ6staeFd7ixfRUCpt8fzAdCpKwkT3fft3wpq9e7jHH5YcMC9nLRAVLQbT5yAyEBCSHQAOeeAwC+Od2P/oEElpX8lG1sYnjty3p0dj6B2ZHHnUD9Ycq9fhOCw7AaPG8w7ZAG1JqsYkIENdQqHY5dn9ZD+8i1MeZSj5Lp1kA6viSZVJRdc8Pf4MrFkyxqWrM17nzI6f6PslcEZDl/GuyFiTCNmrc8i8R/HWlpQfR51xZ7MiI1Ek80DH9kDnc7Dq/IWiy/Eatis3aMgOFwF0Iutlg5mZotKWVjA2UmQlBPtLpLhwv41F/wmh1O9oObxOBZLrIPxWdRAyGM7Mg5CxUnYFo8nR4y+LCspCskwUTDjHOfgDAWRkPkjnUmygc8iTg/AWbDNlppS6oBrewA8k99kCcPIa4girkmt08kZGStRs60ZGX6kpQ8igPG1aIU3Iq0Co3ZCh0OlaHDqHD50pcw5vyr7JNRUFt0KH1+CNOG4m6QMErfKHVX5ze+9FBtZMuh5JMnDHERg4pZDk6GMwOJhIXoaXFvaQvJfZQMqVxR/t1+DNJNyoSKnCytV4Y8kk2R86WWFB8/KFkYFhrVNJurc8CTNRHUjS86ZJfxzPMw7xMPglE6TZOdrgjd1yoTC1d3O8lekkdztCCi5y20KlwLo2X+0iqZo8yBtp/RhNktGLYXQm7oQHDO74QeF/aJcT3liJniQZNnY93taJaaRhIqhHDs2GN3ZnTyTJzXdtYUluKtRfw6h+p3tIo939zm3w5uqTqrYP/PEOjpKcoOsuDclpa/PhTfh9vZFk6oSRsGYuFZrfTYb0jxswNyRkhQ3eXHPfGp0yHn+t1QLMV5G8ohshCinOeK0/F/5FqvNU94d12mAq1LdgkOUWTFUuuzkZbyP5FDLSLsaFVId1iT8CC26SGyDkIklVsdXeeA3bghoyZUtlZCxpFBWqndCrmx0m7EbltseHlIU67haTbmAWSFFlSKYuhxWVkyDVZeT4ZC1e68tIKqYtgs4aGCVcm7MXH9Ri6rn7wYKH9IHOMbWS9BjSOAnhWsX2ELoOSYKJgzawojiF1O+QTveWlfFhJSVSbzEsOLmJBcymJkbPc4Sp9prYmQCWa6iuvwbpJMyFVeMp9E2AudWLv8SHNvLonK0T9hS6T25DOvb9gzVU1TL+PQ8VeXuZ1zbzDwMGkOSuKKR1qTGsKjGKwv0SMOONj6XNbC4zb2z7nWIdx88nyVgYlVpGRXRsVuidJ0lVpVYUDiIN58kZdrMbhQH2eCs2eCfLL4WwpytMNaHeppkwEbSJQmq3fJAqUHG9zk8kWRRp1a2JjHSj5IV0tEnW44gnsm/Jzy4v6VY0TYFtok7VrGmqrRCliLsyaiYVSfUj2GzVWBhQbNiLjGgHU8oFcweXRpJlBlTJBksa5/HHW/ql6YifJz22gbkG1Jlf0zyoTCa5ZxKD3Uq5NQSAy6fTD6h9Qy3skIFsf1GYNgwm1nSljqbzA0tl3bVPPnwIm2ngUiM+WU6m7UpB2H6fOiniLr/NdLW8QCT0QlqlWPxETmRkHaV+e2GgLUQTpXOnn8yT+tYIwvuo5QyFa+8ImogQz1eL7D8GwbFTGCXfSVC0hzVad05zRYaWUhoKgyE0V7tAKaRRTFO6HN6d/SXo5OsYSKML/kAxUrNV17dZPeIohBRGxr4oXROC9vjgHJ1gge1pSuug41+GHDV84dg8qdQLuXQb5mLp/gveWa5cMFqesy/1FgHjSHLzn5CCbsgx/xlvZu8fJDkbFvxG0o1k6hFIg6gqrxX9PvCamnrNem+HiZm7GbIK7yqLDcx8ueNKCBXDsIoKdRT0bG7lITkeaWy/cVGLdPw3UOGZDRZcI/sNJnkA0gKuhN6fs3xpsH/WD8Z6PaeiqiPeTbWWSMd/Rj3S3RHZnpCkD0wNTOQfMJOwlOS3SMtxLKXW0Fvz9KuB9hCOpJIBkSS/0F+aS8DIOf40TYwOfAAplGSPJLyLe8dgQTmK9+vec9WmAyo8djEP+iyGJPctgbl2EaTLbhXJhjMhtb9AMrAXhOfkbxdJhskBv8BnMDWz/H0auR2B5FiDZMXLeAfjHiFAizRO/EG3M1CEJ/rATKlgDje7KMdRqJRms1Fk5ub47jTJpxDWe8rhqQbhBTtqX5Dc6AcApfkS5pJiXag3FnrNQ0jyegLe2oVX3uNhzu9hJLkAUuETMDVyqvn0sncqpdQgmHDMQzJsEVCeZFNZrKcpna4FxTDmRsI+kmVtABxgF7MBnXHAkwbRrjA4S0Wkx1vX7xOGfWFeNBcTSYYkwAJtFTeSUTBqSU6d+8OvJNtqYZSTCpdLrv5h5EYoulKvs9jSKXoSMIgk/1gCFKDmCHQcfzzUkJLbxK73vL7ZAhMDKOy70QZv5cfRY2Ei67xEcYjdYUFCQZIsBKNy0dHdHIFFGpLfOEIvlDrqP0qTvkqwIF+qVja2q02S5QHnVzF0lR1Fbn6Ec8q/KpYMmhWjTzm5+hKkY9uPUsMptfCufpk1jYq4w0hP+3sYSe7qA6PxXGdYV8YNjyA4+ZChHWmwX/esc7pymF1IxpzN40L+LS5Y4lLtOdc2lW3hN3DtBhV18u9wgkVZD1DHZfJlR7y9hLqbKakeI73vNlDR9lFbGJxRXYfg97dcEbFjZ/LhCX+RnTFz3jRKYv5GU5K7fbw8llKvuGw1goppnvTMEU09X4dnsK56BPV6et3W4m24VpmoprTv4XSkk8/DjSSjW2irBcPgpaY5pKgwmohejgUlThWhECLupUcwje+1uioazbSmzg5ChvbOnUaD/bHh+fBGSqzyyk+9hi9LXXyAtG7WoyKlERDeGXra/WWhVy2ERrPx3QAAg7qQZDcoFtGM5zdJ0Gl0mqZUS5to8VoJKzxp5Nv5aIAzMqI90WBrBTca7O9WCijiB3ON5UynWVACQLHM0GvEABicvEC9eKBrRyju1B8Qr4Vwj0YRr4JglO0FDQLrFsabKfwyhmZ6Fmxd5ekwZ0eYcrTd3uTrOds2xZn3pVgV07wszPjFe8qCOi/TRUJva217GK1e4EvFssuA3+gTSOu4LJv5u8jnMPf0+2iS84MztcNb8M/VWcN0XFIrbq6wYfD3RavW+PtCBw3T0jzRvxrK9BCmau6X+2/1g/Anzxgq1wtmnGqGvvxKTGSH+8GCNsPufJcEbTPeRlp7qy3gary1xqH5+TZURb9NgN6AdTBq9YTCxnOGQ+JlSAfZCFZ80xVWLQmM9kN6t+LwTh7Nq6HmG0kZM2kJTJQxBm08VEPFX986wmDfS0iHrC8gzjwE1vht4CZYcPAfvCunW/9JYYbcNrXuXhnm1mi8IGVboKLgcAQmym6EsHraWFiRpDkIK0o9IdfCgqDmeB8J1adcGRXJNDx73r9yaV71XjZILztjxMlye7yKwq72MFOXi6C4wZ2wYhH/hGWVl4nFIB9LkuvJRsUCbt58GhC+fmRU9pnIQALZ7+WrQxcouad7kddYU0P5p9VhHdbAiud0hUWtEknNJPw/wVtNo2kee5HOC8YD2uu8BGta8DEsWelLch7+H1GBetGXzsCCcHLoz39Q0wvWzLZY1UFLSfIlPjH7E7BsEqXIMa6wbDIV/8KqTuyNdGrOJ6mugk+mHITuceVhmV0qqenycylYk3UUyfttYNUtpjjCXK8XJNnwKT4Z/zJ9jgBwLE0WCoJF+dYftEVGnFq+OLoE1vUiC8BUu+Fqkvz1T3w635IxkwA7kkxchY/CpiGj7UyWCrdVUzHLD59OvgjRZJupVMTi49hGMs/NvUp7A3PupyCeTX06O6jYV3g4SXpmx8exiAqNe8r+aOq8qIxPatVpktwC2IWQK/CxXKO5DkO0+MQSipAdkgC0C/RNwMeS9CtNqFoXxqdnH+pyFIqZN/HxtJlOPfcRtfB/x7NS+ATurNgUE1Yvi8dAR3z22WefffbZZ5999tlnnwnD1v7z064ircO1EJJjB+964pA7CUD73qFT/CH0HoKDOT0ebrnR28sZ6//77xMmeCyeCSD72kr2kNaeBeAYnxs67Y8+A1B3yssbWx7O+Rl4ujbc8AHCOTzf+jL01atXofE5q0FPe7lk0cCJ/9r5QRF/T9nT43cISqv1F7b2evWbLYQ5oR6uEG7E+wMFQnvfW9F0yx1I/S8lQ1j+MlQ5LjQeTA7u9tXCRAaLp5njWHv2zbNPGLcSyOZCToKiEavga9I3MYR0RXmydOwLcvdq4BRZElJqWQCujD4BaRPnAphIpu5zYxbgHqeth3CYM9BV03A+qU6M4w/QObmZU3c87XSfqd0BIIWM2afidUh2ZMWmc2ftp0sP8WT8L7IdhDJqLZCXLLS1H9kUwmC2hXCTVI6rVxinNQCAvZO3AngUxvxBALRjyIdAW9IBihVxSVhMXgEcV9vgFjkZaEp6AZVpeL9VezqAcEPw83LFY0GyOjBzNXCU7NALiiYMRxtvzCQTAf8SkKpNY969AByHkisB7CKrAX5ZIT2QS5Ft1pIXsgPYR2aFsLshgDxkfyyJ0KUvp2H0ESgGkgcA/xTDm3L/OoB3M6qXQ2HTjOph6E+GKIfh7d4aaEB95w4S+waQRYHCJNkJioqHAAwxPDR7QY4DEEw+hfCKZERjiN64DSURGQE95SDdZC2WSKRbc6ACOQxGAfpe/1d+chND2kDI4S5fACQDPmR//ZqHulAsEv29kgyEkR1ZFFI3cji0/SgK6yajRENXIawipwOTxK+23EWqa4pSG6NrIScAnKMLy8gHXf0hbEntS6YcEYPdK33QFuR0SDnJFaLov4RRE33QU6TbXiWoBlKNqQAOkAeBK1Qp3bwmZroLd+vOOo4Xvb4FRn3IG5COkaeBumRBpbcGA7gsg3pr0UQU0UKRwJkDh5KqVUpQJeCBySpG2gIo9M8oqm2AoWS4/MLmaOY/55OBbZSgzdMH/YnsaKzSzMAG8rHcU/hRHxRlRE3HMBrSuF8BTCeLoX90dHmxMFzTfAC5SBf0XyCRvAmjqeRiSI9J5oOTL9UJyB79QAb1PP2XG2eiP3ml0cvoxC8AVGZ7m3/IyPVAigeA+7NnkfeA5ZpWnUlXEbRDapx6OjAnP052IKvOxHmKx7TeZkH/IldBOkiGiKD7UkPUsZDCDUE3kQFAGH0hVdglg27erLl/TkYviJvkf4xBVWQyhGyFs3mjNnkY0jmSS0TNz8aWCG8ZdNO8eK+SImgEeVpMIq5sj5lFyL+WIyUUQFz3xtEMCUJwHmwjj4mgl0Lnxp4FpuSXK6cGeJ/nkfQj6kkONA6eJ1CV9HpYf2H3dEE3iy0bUgXp/iYZ9F6ncWxYHMB23oJ3DEP2GoJGkDMg9FVybSSLQ5pExsgK3m1fcQ5k0K4Q+pMOwbp3so/YHtjbhXQ/lTIbqMVk/EtOqcZF8CLXiaDJEFbkBxDuS/r8SCfdiCbC4AI5CdLvZH4R9BEk86CpZC2goWEymlpEBk2Gd2Zq1gOXXBbPmFGU/MoQ9BqZE8Lf1AAryFhIW3UjX5p8qBZXxp3kLMNkNLS5G6eVA3BHCQqnZmTf03eB/jyFUy6M3DRYnOCvxAG0h/CNEhQz1GQOuSpojVnQ4WJzYTw5QgS1OBk9JpcB6Ek2h8LGbZthMlpItkSpyAs/Taxxn/zVMBklkzF7oQikLzBMwwv2cq53p0q8patEqsvqx3iP4Ro8HVvJolpgoIxRJ0J+6XGXq0W9sxiwTvTMZENF+oig6ERSBvUjT8PgGHkfgm0I45yBJ+aXl536oLPI3wAUJe2gKMYqAAqJNu+RXigge2Aj+QxYJHebQl4TBbCLIXKlbygAaA/pp0DnaP23CLcMl5ffyGAkuYtNJukKc3lD8iKwR20PPNYwr6z1wUBZsgmEf0ZB8NAHzUqOhtEeUtysOQZTozSZg6wGowbyBgRDdKvku5PLZiqbbwixBXCF7A/7+2SAtt9EKAqQPYBk8Ssw25e/Ft/+eKc7pykBt6p56FjUzSwMsYMUzA7+UJQn9wf/8aTZGd3ldggZdwJ2+hjhKiVojlT5UdVTWTRlxNxZ4Z8XOa4C4/pCOqQLWotUa2HgfYnqks+iHlRg2GHdWRMOo+dkhadPVxal70O501gy8Nubz4u4VRcTLfnywWTyXzzlz3J+1Si3owHkFSiyZwpuNj8xf5/eAVC0i89Sz73fHwVWGyqqx10Iq3wc/mvoAVs08XGYC2gXODhcRMC2RZAO99gJ7FkoziJxF3jmusN1G8xR9vK5fhTI2RWSY+z0JQCQMNShxxqYOHmvc0X30v902gvFAodtv8Cov4/P9QNtC81amR06xQ5tjui5cUUUFFMctk1ve61lOFB3aGUIsQ4+i1HNx+Eh/g9wUo8Ll/uG4QAAAABJRU5ErkJggg==',
                fit: [100, 100],

              },
              {
                border: [false, false, false, false],

                text: 'Minutes Of Meeting',
                bold: true,
                fontSize: 15,
                alignment: 'center',
                margin: [0, 30],


              },


              {
                border: [false, false, false, false],
                //border: [false, true, true, true],
                image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATIAAABOCAIAAAAsMw2cAAAAAXNSR0IArs4c6QAAAAlwSFlzAAASdAAAEnQB3mYfeAAAUUBJREFUeF7tnQecFdXVwGfm9fe2s42qFAFRQWzYG4pd7FFjSdQkajSJUROxlyQaTaKmWGJM0WgUxRZbRKyIiApKL9Lbsr28Om/K979zdicrzQUhId9vR37Pt/Pu3HLu6efcM7rrulr31Q2B/6cQcDVHVubyn6bp/Kc+NS2f1gxDMwLctl3DDoTy3NO0IlvTaaRaqHY8k7dpoIWDNQGtWMvFVH/8anifjuZEGME1dP7u9Ihjh+n5a1x6N1l+Deh1P7rjQ0CRZTuSe9TG35AohJgPaKZmGZoZcxyDexAhBOmGXFenjcOnDuUqwuWxeidUZGgRnrZSmpY2XcMNFVpaNC7ECPm6rm3bRjAATZu2FQkEvw5ousny60Cv+9kdHgK2pWSfofBckZem2ZrrOE5EhyRDmhXQQkHbozzD1AzIMdKm6UFXC9paiOYBRcW25uRdPc6flqW5QY9MNSfk6hCuetaxg57UzZumHjACQR53DU8kb/XVTZZbDbruB/8HIODYeSOg9ElIURFmwFASUXMzWsBS07fimh2yPQEadJOaHrdNTQ85ethVpKwINWCjquZTMSMBpVlK5bWDoWxAz7taQNdCSn7a4WBQ0aEnM73nvh5RKvHbbVv+D2BX9xS3EgKolrqnu6JhKpPQI1F15VBJnYxmIfxCZtByA7PS5vSWxmnLi9qS6drGlqbmNjNnhUKhRCIRi0YHFeq7VRWNrI72K9TLewQjUE4+p2XNXGEhPbquE1D0qCxMCFzJ3q9HmN1kuZX73f3Y/wQELA0LUTloHNsO4OOBYvJWPpcLJdBZC1fZ4UnLzPGfLJw+f02mIae3mYNK0yWlRVW9epZWV4cLCzOu1pTNpXPm0mQ+l2xON9fZVm5In8qjB/c7tk+PvaqijW6qIJKAMm3TDOhQfcDxfEsYpV8HPt1k+XWg1/3sjg6BnIaQRIq5SDPlhrVdM5lsbW6ZOj/02Ny5L9W3WFpsH007PWEdX5kZEEsakRh2oRt2jUQ0UFoU6FGu96jUEgWuVqC7RsbSZtfbk5Y2T16wpqE5VV1eccO+mQE77VwcK1AWppKRuuM6yGcR0Vt9dZPlVoOu+8EdCQJp24zrxDJiSm013LQZ1GJaDEp02rJJNxoP2MnI5HedT2c9Odd4sHHgwsqiQ4b3OuvwnQ/dtaBCzwbxvGoJWzOimglhadm8k8y4bW1uJq1l6TobqS7RCsq10gr02oyWs7XImpw9Zcb8t+aXaE1Lzhsz5NBhxQV4cPMxlxBLWDl8bc0MBLEzA65rOBYkmzZCOJAKugK1brLsCpS62+zwEHC1WjNZGSiwHDsdVu6ZjOY0aZm+yRBUsvaJZyvffGmZHfqBO3jykOF7HVj5wFF7l0a0MkMLuznNzGmESHjI0HMhvK9QEuTsLdkxNQeJm0/XrDHC+Ib0RKJUi5UpHy3xFfyvlvbRquQLU+dZ2cCFY/bas4/lOhnLLYwoGzafM5OhYCygR6FVJayNPP6kroCymyy7AqXuNjs6BCzNyrlGzDEMS8tHcI7mY4qostaf38g9MSHRs+q5vUb/sDbef2ivu08bum8phGeqJZErwBUOIcvyHgk2W3OJOmaz2VSmLWelTSdl2mnLMiN2dThs6bl0RaxqUMXIYLCXCoIEUVgJiWhpTXtrbtOkGWt27tPrvMNKe2grG7PlhVGCmCbk7uYM8hGMsJ7KuYlol5TbbrLc0RGue35dgUCzli7Jx8kPIOSPHeno6cCUuc0/eSicaDFvvOzqVK/X5mpXH7Hr90doUTebD6QNsywQUmk9pp4xtVRzDufrmrTZptutlm1nrGzGyuTRZfnnpvNOrtUNWPkWM9cQc6O9igYPrBzVq2RY0C1oiX4xWBsaSJZq4cDqsPbc+wva1tZ9+8SDi+Ia+QS6lQkHDNRZAp6BaMSbXZeubrLsEpi6G+3oEMhYdbFgBZpnKt9SmLF++w/z9qd6/vTCuT8++5ZHF+UDxrjTdx5V4tpmxgqFg04BMszUsi2ZutZsrekQC2lzrUwoECRtQCUQuJaJa9VOZ8y2tkxTJpNMB5vazCY3mjGN1rZ0qjTUe9eqQ/qV7lG7fGGbme7TZ9juVfsV2mVEYaYuafnXzJpTR/Xt36ekMKhZ2XQEjVbXcniCjDBWZ1eubrLsCpS62+zwEEjn0vFw1syUmKn0hXdaM5fFn7k9M2DAxU9ZI8rmX3L04J7FRQRGgnpMaauR1jXJNZaTz9kZNN1IMJYIFBWEimMRCDpEaJOcA7kczTStjGmmV7R+uLp2xYqWL2qsBW2BGuRggV5ZEet35ICLVrXMnL7wnUgksdeA0SP6nhDTKpsbtPemfzqgX99dB1TGSSWy0iHl+wmajhsxukSY3WS5wyNc9wS7AAGyBYgZaqtrW04d12K39HvnL6vtwh9MfPegRPU3jxpSFbYymYAGbQS0jL66ycyGs6QZBEKReCJSHNLiKJwEHCEd27C82IZBVpCKcmIVqkx1O6mZES2Q0ZoWrP1sbv2MFc1zmlIrYxHNCJefvMclbrPxzsKn5tnTduq37+iSCw8rPXZJJrd6+dqieGLQoIoIhquVigbDKimoa1c3WXYNTt2tdmwINGpa2fRVjWeMM0f2qH7qpnVW/L4XZmo99r7zSM0OBtURED3rasn61rSZSRfE8sWJ/q5t6KGoyjWA7BwzAAW6eTfg3VFkqRIRdJVIR9qe5eiRqJceS+NcOLs49fnUL55dUvNBSygQzZYeufdpiYKyidOeXdc2H8E7rO+B5w6/vaE5t3zZ2h5lRQP74fG1Axw0cYLK4uzC1U2WXQBSd5MdBgJWXsuF8om2rFYYS7nBhEs0kCw4q7TRajzw7NBuuxf+45dtYe3BV+bVGPo9YwaSWmcEoaZMq9mAfzUWjBdGSoNGbMtz4xCf+HlV7o46KWIY9FZfXz9+0T0L7OnkxZ4w7JvliX5Pfvh4q1YTMwPlBbuccfBZQ8PHfvLumj1G9CouxbYkQJKLd83pE7j11lt3GJh3T6QbAl8BAaIRYcuyYuGsFkwkbSuqzm4kmjJNJ16T7jOg4u8/zQbtdz9rXdZYf9uY4QZ2okGwJJvOp0hUj4aikVA8RKSCmGOX4hSdJ+MdQenI3YEsSZcNh8N7Dt7dygeWrFu5umV5v9Ke+5bvvXDukkyl3ZpsW7hwVihiDBq4Z0trMBZDTKaiegDNuSt73E2WXYFSd5sdBwJ54opOIIS+SaA+2dQaiRnaFffHPl1eMOHOfGXBvFTmlakrfnTgXtGCtpAVsYOpTDaT55BHKB4PF4aNiDrl1XEWuuurIhmdxn7Wu4jNYDAY0Qt3KtitqLjHjNUfrlm25PD+hxfGKz+vW6AVZJqSyRW184MJIx7dOZNye5eEdWIuoS6Zl18robbrq+pu2Q2BbQMB0thCYVwtcctIEgYsTWjPT1702rv6kzdaO0WTbmj61NpjRw0noh8ORdOBxky2lYOQfE+EOeqBFzTYfuxyy2ejxKV33AriRGArpdZxCJgUGWWj+5x85t4Xm0bgyU/G9xxQNTI+MpNKBwvL17mpZ6c+OD/1RoOeqU8GNf/8yleN3i0tvwpC3b/vSBDIu2bYDeY5q6Frbbm2As0yT/pZ9PvHrf7m8RVmZuqC+qBZfsCgGPmtoXR+XagpaOnhUCQaTgR0FaNQMk9X55c9v84WXe1KLDSJqOSSLyEtmNXzjm0OLB4aKSueuGSSFcgcs8sRM5ctyeDgCUSbk2tXNS40tXhlwZCeiWgXh93SyW3RSrobd0NgG0MgSNwvZVO3A89LaSzcOO6RWDRSfM1ZAznR7BatW1U/aNeiogIt7mSbszW6FY6EC1BfoUmyUh1S0b06IHL2aosuIUIRmF86opzWnGAmH8qEc4lDKo8fe/B5cxYuWpNevX+/0U5bCnduZdVuLVbz+4uf/SIzt44kva5d3WTZNTh1t9oxIECtHS1GIJBiAnZg/irzqUnaPd/KBGJayvp0WXOiZ2W/Enw86ZZ0bTBqRNxYMFCgk45OjjnxEDyy7dV8tgbt5biW6K7+pSXw5YQiWhzHUswOj606bUTVQa/MnLJH//6lsRLLzLc1JIOhHulAy8eLX08ZHj/pwrU18+tCt91NuiGwXSDgZvKpoBbJZFvIf/35k0V7D0ydsGc0Z+fi7px882479SxUlT1qU4FskxEsjhS4bsB28X96Re5U8o4qHbIVFTlMU6W2r6fBIj9TRlM4FQsnE8EYeeut8Uz8/EMuTQbCNTUzd991r0wqG424bj6ct6NzFn+0pnF2F4HSTZZdBFR3sx0CAnosn8horbFoSX2d9q85yV9fmaBSlqEtWRsaZms9E1kr1FqfsuxctFe0MqSyCLxSWSpBQJUMIHRIiAKddksXEw63pxnwuPyTHhNasROztAIK5ZEsEHdjWiQXum7w1S+uWXF4Yf+dwtUNrpFzU2bYXKm3PLduIql/WcbOaxmbJEAnramKQjnqgH356ibLLd2g7vb/TQg4UFSAkLy1dvyr1kEDq0vKCFMYwXhbW7pHeWkwqCfNZpJyolGoSO+oEbvdJ4z4tR1buWddJxGJ9eGKVSyrbxi6y7B8zspbTlyPOrYxffbsNem6nAXvII0I+1ZVqlVlZjeIZXaT5Xbfs+4BtiEECPzhrynSsqlXPrTPHqUVFJmum7EpBpktryjOa+mW1nqOOUbDRDPJpttiqbilU5UcAy5qUqpCPqrAnqLME4YftbI11b9X/7AVzJEdlNYi4XhdNjO15rNkel2Dlg57SX8hSprYVBtZ/+omyy3diO72/00IkMHmcHZ50coK3JxHjQC3bdOuazUL44Fw2G1N1Zt2tiCWCFGNp1Ot8+04Yy8MCjEyBJTZHtJ0neGFQ7M4jfNadXEFEj6ftl0nlNbct1Z8mMo3f7xmlh5S8jLA01K+tluJ3Y6b1N31doYAKExBV/f96cWDdjJ7lOPF4dh/xtHKSiN5p7UlWUu6XTQUC+sYlV+7WmsX10I+u+OIT0hkJpUp41Zsl512zTRndirrmTcdCl7aeiBnO5+uXZQxzGkLP2/QspR+Vp5hlXO0fsCmW1p2EfbdzXYICKhCOyEtNWOes+/ggBYnbwb7DPMtFuXMckPeyobDEcOJGCQPqEimcq9s10uculRSZx4iMylL6dFa4V79hjtmvm9ZpWHrHLi0XT0WjqxKttYTU021fbhmXl5XhbcwMKky2y0tt+s2dXe+fSGg3kdg5+2mNmu/XaOof5SXI8hvk2fTlMrU23o+GivUHfWuEC5DpyrI9r3UeUzvgjJx8kr0RdGnq1Vo8XgkWlFYWBCOtgSd1nTSNXNJ01nW0lJVVj5+2sSMZlq6zcsX/v1moY7JdkvL7btt3b1vWwgoja+tjWI9zoCegZymZagDq0dCwbyVzlB4UncjoZhOdTlTwzHKOcptO/pGekNYW3hh1T+RnJKaB1uIanpxUZGbt6lEko8EHIP3EblmPrCqsalPdc+3Z32c0Uhdt0ySjzZIOtqOZCkyHa+x/8lBNfmTS35VnmX4RUczYTZyJ59XrI4JU2IQexn+o06kai7f+dd1cHMuThp3jiP732UaXDIofzKuwJc7/q/y+GYGldn6l9/bph6RV2LIrz5M/FHkJ+7LF79xZ1h1Bpq/QMk+8Vcn7f2B1J/khfJSDe+SOQNedQLfW6y0F4j5f3LTB4h88TbJMbO5vz/2+JN/f2L16tWdn+I7hpYIDenQn4ZM7Evz8X6VqzNMNgW3JIUm569NlwWjepUeyruFvD4LUnDr3Jqs0ahnI1GtKhCwrGA6EOI9BkWb2bJt8xPBjWDICKh/uhH045nkwya1yIiyQ2Mpo6y4OlvfnNWKW4KJgJn/vGVNv+Jog649v/hj3rgQc/IW5fq+fG1HshRAc/iFT8lagovwp2wM39VrIVRwNwB+cJ+9FASVbeM8Wzqdxt1MxT+kvNpm16EB32WbN3MxFrgl/XAozsdghuNZQUc5pCOJjrRXJSc6TrjKT5LSsd6zmxqU2eZycO8vYfNmKt779Na5QyAgQ8sMgYngq0xSWtJG7vuNhRT9eSpW7QEc6MmiBN2ZnvrTA77wR/oRaANYP/26tbWVoB+N1TvkFIJbfMrS+ELjdugZxllnnfXLX/7y+uuvf/rpp9va2rxQoYInjYG5QI9HmKdEDmQsWaBstHBA5STxLp/mN7O/AQRgNhcvSGgRKrpSr5mwn0X5SRywrIlyyR6Q2jfua5Y23zyObf5XvEDEJhPRGK8wARrqHQuISsd2AnpdS5MqFh0OzlowTwElEqLxer1tL7IEsmAqm8EGCO1BJ3wRZOJiS4QN80VaMnsQRYWAgkG2liseJ8mY8I5r5fN8+eNDD98w7vpMOh0Obu7QWiqVoluwREYBOYTsfToU1sDEBDl88mMOzEQwSTBMMF7uCHptajMaGxtvvPHGZ555RjGOjuMFm9k5QVDBP5/MgICgr/AsoYTO7IOnaCOkwhcf6f3jf4LZQk5Ajz8hGOkkEomoznX9448/HjNmzOGHH/7JJ5/IbOF0/JTJZPhaUFAwa9asQw899KWXXvrRj3506aWXsnCAyfYJYwVEDDHlgw/efffdDz74YOHChVdccUVhYaEvyRlIKJ9mwpplRcxBwC74wE9CyVxC9gIT+XVToAs7ei6ZSpQUkrxD5rmHzihPmRyFsKxM0Jubeo8dRM5at/yw89chxc7PqpQ/3v8VjhTEE0YwRJ6BaTs5wBII1KVag5SzDRmfLJidwihW1aL/Uy4fYZB8Cg0wY7BEaM8nReGpgp2+8JS1sffsrpxqkz/h/TfccMOjjz7qo+xGIUh7+JPPnqWNv83Qp4woctsXJtJMiBAsZGIiE2QsmbZPMxsdd+nSpb/5zW+mTJkiixJFYDN7LEqENBDdQSAjT/lyTOSJj+WiUwhOC97zlMh/mbDIHF/401gIhkuobvasWeecc84777xz2mmn7bvvvrSHJnlZDg1isZhwsdmzaTVr3rx5M2fOhIZ9USl8ilEWLVrEXrS0tnzjG9+48MIL77vvPn8hMnP/T/qH9kQbF+lNJ8zEp0wBuOw1zyaTHKJsN3A2Cj1EJDVcwXdbp9qyUr+NACWUc9l8EoFphFXMgSqx3qvoNgP+/8BPKnWHZL9gMGRbbt7h2Db/yLVz0e85/0nq3YqmurpkCzWjNwxdbi9pCayFQQpysyWCXrINwnFBAv4U8hBMAjlAAmkpOCpyQDQ3nurXr5+IwU3BVfAGEeHTs6CaILH/IJghzEJu0r80k+G4GIubiAi++GJtM5spuAsNCJnxlCxhM4/QrYzCF2YrlOxPRiYmIlHoVprxiEhLGgjb4rsIeRFHshBBbtCdJYiKCNUtWbLk2GOPXb58+UMPPXTVVVfJFqjFYh15j8iOQLcNDQ0//elPX3vtNSQqi+qsKdCGX3ffffc7br9jxIgR/fv3Lykp4Vmfx8lC5JP5AGqZm/BlLmZCh2vWrEFhbm5u9vUCWtLGn9UmQEeFOviWjg9TXiNJTo9m8M6QjEmGqZdiKmBXH/9N0vRQlMS6dl6GYNcp1IUlzxtt8b0yd7ILFjfWbtQe215kKYQnWuKyZctgvcIgmSX63sSJE5F7MF1h5FwCSrZcMOn111//1a9+9cgjj3zw/mRUEbRWjntzjDXZ2saXDR3K/hYKtwaT+PLhhx+uWrVKZiLQEdFN/6AFuq4/Q6F5btbV1a1bt66pqQk+AklwUyQ5CxFzyx9oPaqjJZ1ISyF+htsMWdItGiD4LR0KQfLI1KlTuSm8gMcFgNyXLyIY+ZV1/eEPfzj11FNRR6+++mqkNEPL0kR++h1K/0weeQ5Nrlm75v7777/ooovEiFB6AVWrPPnMs1AFD06fPv3NN9/8/e9/P378+CeeeOLxxx+nmSxN+ML+++//u9//fty4cbfffvu3vvWt7373u/TvO67ogc55hApUULWvPjAKQzAQk7/ssst22WWXsrKy8vJyvvAnLANy5VkgDw5sgiaVFAzEeZ8WNMkLeNhaJKPpvcoHJTZr8sYDRaoqGrjFxXo2OeRW/aAUaFWfBLUMSUEc1SCXFxLMO0VuMMkbh6ig59rzG9ZwkHojA/iiaZt/QSGhz5aWlsGDB8NW+cKfINBOO+3ka5Xsh6AvmyoKW21t7QknnCCSRKGLEbj15lscy+boWkWP8kEDBuKggDI3NVvROfl10qRJECd4Iwqe3/+LL7548MEH0zP947SoqanhEX6FSsEzITzsq9tuuw2soh+xVNe7hOvLJT9NmzaNB2+++Wb5E8YvmtumLpAeZftPf/qTTE8g8I9//INO7rzzTv9Z+SJkKU4sLsy5Pffck5ZIqn322Ud4yh//+EfphE+IhEWJO1RYHhxwt9124xFM9D2Hj0CDxQyGB6lH8EN43YoiAxGKPuLrDgzBr746LcSJJ5ZNOfjAg4oLiwRQnecsrPbkk0+G6hCJMm3Rg+hnwoQJF198MVuPNQ7M0aWZGOJX5rP5y3Rz9jsfmg//Mq+0QuV1sLL1sxqmPrPw7qdm3jht1StZFqTyUnlFj3IXf1V/2+13z5K0bHd6zcQTnrg0cM9hxj3f0O88Tbv9mNOevvmpxf/U7jpDG3f0ZW+Ot4i6dpgq/my2l7QENKAdw8AskT9sc1GR8la///77Rx555Ny5c9mD73znO8jDn//856KVgV5s4ZNPPomo/MEPfgDXZL9Hjx592+23IVjoql03s+3NcFNfoNEGVVaEmO9JwqZCQ/vss89AYtAC1Dz66KNF60Nngx74FVcHM7zlllv+/Oc/04OoXqLsibq10Us0Rl/dBbPXs2/XewocheBR/3xjjPYMRw8wBb53djvJs/QvmI2AYgm0AbbITPTAnXfe+Yc//CEqCTfpVsxU1g4ls7QFCxYcdthhc+bMGTZsGPQAl2QXzjzzzHPPPRcmCEM3cWR6ApZQx7e//e3q6moaA394FjMUxuRbEyKHfSHMF9/fw3dRqkXlZjnMzd8sX4GCKTz44IP77bffF198gVUCR4N7MuK9994rcGa4TcGZg8xGIgZPUJ4dIj2mxRGMAO/BU0YA+O298Ifp8d4e+fJfuhSBeQsRfsrMMIR5QyZT6ltS2YoJbVGI1mk2M0rJoT7fl6/1/xau2Y5/rvbZ9Bl3/eJOuI5Et1SACy7U8fmVxReE+kWtoluG/uY55z7y8B93GTiovEf5ld+/gp5nfT5TuVu9C9J97LHH+LzpppvY1x49elzxgyuRBc9MeDYYDuVti/cjGRgTm9ZQ/AhnLm8SucIxzYu0lWNAN7LpzPMTnsORO/6pp79zyXfuv+/+b5x5Fu4NFC3mhsI2eNAuf/rjIycef8ITj/9953473febewsLCiXUy2SEd7Ac1gIBg7sc3znllFMw1VQLxaCpSwGECUJ4OEM4St7a3YFkAgG5o/YiEECo8qcY1eJqUu/7ph/856EwX9TL1+iEbnGWWnYkHHntlVc/nDIFMfXoI39avXIV8IyEwuN+eh0vbXvrzUm0ScTiPC5MQWgJeB5yyCEEM3DkPPTHh59+Zjxzxn2KKYGwAnPpQfQXoMFMTj/9dBRLyAl6A49ixAKxkZRLXCkvMh8Ayu6zF2yKLEfMBPVycleTmTNn5TOnsfpPeTb4DEXCfI674frvfO+7a9fVfP+yy/Guswre//HhB1PoH7htxoMazDraLhXJVCyYS+UMncKvi/SaRGtt3HKaIm1L3RUo7OT22EZbWm/7L5qWlpMKAge7LW2kl+caDRLW821Bi7qU4Yriwhmt6XhTDQ1aG5LAKagCPV+61idL0V6EHc78/HMc5bBMbilsgxqd9qAf2wCL3Qz4pAfZKrZcHPE81atXL8SiQm7LEj7qhy5EC0LX5X5paamYKL179xZ+w6f4UYSvb4oJCiVAI3TCEAwNKgspRGMx5Alf0QBNPDSuu/feezMrjC46RG4ceOCBSDn6LygsRP4gq2vWrpUsZBG5iu3Z9l133QXXQPKgFv7rX/+aMWNG+xpt3vUERSnG5k9PPDqiSQo0RO8VLxTD8V2sVmhSDcQe6XqO9SLDlZe/o6sO79enn37KinCEfvOb37zjjjuYPBPALqBbiI1H1AuJPSKXPum8Z8+e+Hh+8pOfyAT4CSvummuuGThw4D//+U9MfZ938B2IVVVViY3X2V2sICZhSZINPCYl66Ir4Swyoloe1jixaC8KjbBVzjA5ZoFKBzLoRktzM7EuSPGdt96+9tprYYhqyxwHk15hGt5mb6yNXkE4FyZoIk6BLV4yS/YdFk3aMcOBAsqltybXEtaEAejKYIMsNtXNdr9vhGJwn0DYrk23NiZTTkaL6pzKzmmGvVNBjzn1a/LREG+7bDbgwV07Bu278t56661kKonKxCKAFDBNJ1PRMJU2SRnKw2I74996CxXkExcOyoz8qrQfPAee0gj0hX7UtnkbDBKACsXFxVAm2iy/QocgCv0gNumQp8Wn53sXNoQupKhewMswHtvGRaTy8zvkP+ZuLBpDqQ7DEXQdPKBzZAJSi09GV2563Dye+SS+EHpSQow3QaVSEmdDVGJuvf3222+88cbnn38+duxYYWTIN9USopLgO0F/y5JnfU7n+3ulDU9JVFCIEwhgRNM4Eo1KCE5ZHSzcE3pKJDnO2rVrc2YO7kaDAQMGAECWA9CAjAotEghJp6EKIUgZgufEWcoSxFhgpVDyHnvsgSkhvEY4Hfoz33HJ0AztXVxfAA1oADHJL2NuTEX8W+LpQVOVURS7QYNF+MNDw2E2kf6Vd1pplZ6/19sUUAjNpWdVNdvPzGF/mKA0A0+EXXaOsqy3xcqHST5NWYm2tp6vIbz1ebs5kI0GVA2fTLY24zTiWeF1sqpmwX/vsuELSqVuW9RYk+SoCG9AUbk8aWRjv6Ien65algcQ1P9R/iACPOu/L2j9qQtyiBKLJ7OkuARThO9i2OAjwfYTNq+IatOX0A/Xc889x4MwZl83VkTlcXT2Ix5TIW+FxIEA2485etJJJ9EejwjtwZh77rkHfnzEEUdILBEMYKeFi2/0El8rLRmXPgl5g7VMGGQkAo5soZgvWSng95rVq1999VVmCHbSOZT/zLPKCwJmIHYQQZAZfAGuL0KAuUG9OAyZFa5IwfIhQ4bwEysSlxL98zhimV9BTVEQOodAhEpB9+eff55fUYMBrMgZJd5xjYYUOfEs4wIiiAGalO+qH8NgRHqAHUB++E7AdTrhEdGQIYlYPC7SRiiNEWV6XLIExoLk6K2iooJfIR7hDjTeddddgTa2PSqS+GbgTSwKpQBqUTLQs/3EhYZTDU8SPBT2xB1QQubA6HT47jvv4JqCy/ztb39jquCKelAm5rp9evWaPXMm3wmNYl4qgvRkOOtVnGvT6QSeJ0cP9ax0V6/jTZYGftlAqM5sioR5aR1B3bqVjXMtFDtIW14g8l+6UPGDenpdes2M1Sst3q1gk8uBaZnbt7pvLqClWqDPsB4Kl0YTBmGTDdIeNv4OEoH+cccci8oEDim8CQbYP3ACWho+fDhMlP1gdzcluEROgmdwd/zg+FFQEVEUTx17CswYzJag/+Ili8868yzsyXBUxUXYWrwO+PHnz5+P8QYRgvp4KfDpM9aVV175wAMP0C3G0qYYKp1AYGAVOw0PJtgAFvbt2xcNCvpUcqZnL4IEFeUVoHJrW+sNeANvvJE+//KXv1x80UVVlVWQHO5iHmQyK1euZCxlo7oucptIAPYYPdMehGYJAAEVEexElULhn/HZjN69egvigpr0YFoqEkhLFiIuUwgDkNIA/Q1XB/1IVINmTz311AXnn19UWESfomKI2SkaL50AEG4yKNRSXFSswrOuU1lRyU0YCs2gNKFz9ZJi71mmoWSdpyGzCpmzJACtWIElpiicDeULd2iMcvvrX/+ambNw+txnr71xxsDLGEUtx9PJlXjXNfaRVaDxshzELO0Vb8qZfGeNa2vWRiNRth5oD+g/wOpInJLlAN50Js1Km9taWQjXipUrqquq2X0RCbgSNkpQhIl0quCsWpGbNit+ytHI4JrM/JcWPz2q16hpq95a2TJ9j8rDT9n9hoAVMQIpzU5sedWebUPHTl4zgvWTVr5zxcv/mN9MRnpRRM/n3NpbRp9t2vqdL75O3hSocfHoix4afap62ViHe0WGX58shfcLUz/rjDNfeeUVZObwESOUKqXr+PQQNbfediuAxkeqQn+b8L7A+SBjdCfkJGqeSBV8bvhRRI8SmUYzwhUXXXwxlqqf5gL9YG/graUZOWLES4QTI07h3/BmdlHSQTa88BwI5z7qqKPwssJW4PSwA+gBlMVlj/AnKMcFah533HFHHzPGD/0/9+wErDXQAqoQPxAPorOp7G7bhq8TNWFo5gyIoGoIAPwDC3GHIrU+mjqVuYn5Km2UZh5W0X9pKTTJ3OgEZ69ov0pt7kiEIlSL/wMNGQ1TkmPoSolQz7MqX/iUaKpQKavgOxuBNS5xETHCfU8MDSSrRtw/jCVzkCglLXkJDaQlGqlk2yAGRZhjP5979jkAgU1HZgJY37zHf4P5jX3Ls8RUAI5Y4Cio7DvzxGnExuEaYMtgczzLcAzBTCQ+KVEB4UrCONga/OHtjoNN4JUiy7wWsFva3ppcuNdIq7wy4yx9+NP7Dh904KL6JbNWTyxPVF5wwN0ldn+OYRoGpzi2DZltcS8ONu4Xj8567cY332xO4j8uJL80qDU9d+n1457/x+efr4sUEWbN3HPqdVcfeiRqLGz0S0OISdD5EjBx58E/PMCicFfiHOMfAgdeOG3qR9y8+cabVFEhbm7iYnelE2HS4s4BaejBM/1VfjSP82cuk1W50l5jiE28I1yIAvnCT9KJ9COYt0n12Ztke8zKcbFhZCyMYRlXVtHehkxFr2dQROX9eL8yq5mffY5LcOxJJ8uDXAwnwQmZgDzFHfmifvVWIT3Il/YhOq2dlpLtRIc+WFTvHXHLE088Edi++cZEFXBzXL9P9SfOcDMv5zZkdSp4K6E5D5IyaOdxfej5iVNMXtKbZC/8+TB/uc+sfFDLPKVnH1ztq/PigdKDcCj/z38DVubpz61jK2XtMgqf0j9LE2TwoSeT3PDKEpBUkM7lZ0xxp8/gq+2uvu+jy1+Yff2ra/50x4cn3/D2Ph+seybngAPm5gLHmxpgG91PObnPG9684OWr9V+M1W47Wb/llMBPxh7zl2ufWflq6R1jQ1efGxw31vj+keNnzUgRgN0gvLoRs1hco2wV2uPBBx08/pnxJAOgQOJShzuO2n9U3z59yWBGdRdPxkYvsS3RYJFIP/vZz+AEdMgd1DjlHlABXwc1S3lKPF8CX0QC+FSHFJLNbmefntwQrr+pQdV9Aiih0MQ33kDmPDdhwgUXXHDbrbcqbZBFed4macA/BuW+5LLRJ46cE44/nk9Cpujb8BwigTJtMbZFZMm6ZO+4I4JLVkHnyuXojcIXFU3xXE0SexA+gqDgDn5RrGU0ZHR1NHYywsnUQS94+eWXTzv1tNFHHaXae4alet4zxdXQnpMMV4rcwYJlIO5zUzyfSrR6Liu++MaFzF/uMBNxCog1K2qFr29LCo6StJ4JIw+K/4a7//aOMmKn3DqBg4/M4jDDmCQD6Zabbx7/9NPEJ+fNncssiVTxoNKUPPij3vOpzG/cs54DSeGG9+dmRJNyGqnDz0awqtRsaFFhTL4H9CUr5yTiJYmC0jazfv7S6Xmb1B8jZ3lhzP/Ghe91Uf2q6csXu6lckGNfjhW2tZP2O/ytaZObGhryITywZqER7lFentRIlV3/2ghZyk4rDIiEX3r5n1hf6+pqiXc98NCDr/3rdRROPqt79US121hxoPYBxPpH4hH4Rq8Ti0LdVKlRSmQrqa2caljmCq6Kcjz09bPY+A66+zelgWhi8rnRC3KnQyb8+ayZNbXrpn82Y868uV7FFPVPWSwd39WgnoYjTkU0rgWLFt5w041//NMjPXv3emPixFEH7N8+w44RhUEoIvTy0WUCMsP2zlG9ZJSONcpURVMVUkEukQ5Kkh2qASEWvKnkNpDmTsyQKOJT45+GIwBYmR6f/GNRAi5lMnQATQZqv49PMoITuB2e6qeOlHEfaJ2tcTFSJMVPovxcfrKxn64o65I9wvhXFME0ZA4dLj3pVnZK8VbHpj1BYyD/2czPa+vr2IjG5ibmLj58/x8hTS+G5ulvHUtrH2LTmicnM8ywdzqxpGeusiC4dDoOzWJ390atpq556q499y0L7ze3dvLnja8AsrhF7FeVLlCREv7xHCFNC4rdZuSqlBfNymgtKizLEOlmDl8T9Fhc//60VQtmm5YWjUWTLcF0qnj36pGJ6JNzFmtWgtpbBa1x3ksyoKK8JIcOq8LXna/1bUufuwsOgVVi/4izQVKx/Xx/CNjHzvX6hQLF60NCLB4XcT+0Y/BG6Wnb3UQUgHYIakxEfD8YXZvLsfTGFbLBD8EMYSX4NiVtWiS8iEf/c4tmKg/Sj4BR0BenC5OUFB80QEYEOLQh7CGyVy4ZSLjkpuC8RZP5DzQWhw1rxDWIvgPk4UFYmNtsaDRe0hk0O8yJqLU1yba64M4ls5qXPTvn+rJo3+H9R69YtWhxw9TqssEXjPp5ea6PHUHyA0qiVvlQgKwJIjS2517ZRrUqHa3ZrisJVtRpbRVuLKeYaNvaZPadpU888MmMqXNWaD2q9NZUOGrde9qFU1d98fikKaCAcgjljIOGjnjxqttLCei6ZlBvP+MmgNog68eTaaACGCOMFgserwDObuQe6VGdcUW86hu9eFBpca5yDPBFImbbbG8225GEUqBJSAt3rngmN/ME6xVkwoULqXBBA37OndCD6HVbQRu+gPVByli4fPA/CatCRuExxnECTar96FDR/QlvxaD/GThvdBQWJb4AFiUHL7clTSouRT1z7B0U6YBWXuUakWxzbkiPAfiR6rP1LS3Nw/qMLCssnd/00eRVrxCKyGlpEq4sjRd3qdeW2DrCTIVPthWIHC1ZYlQ06m6JHsnh/HXslKF/surVma1rP11aF4tXhVN51wjt06v/0B6lz8z4FI8rB35UgNCwTxq+n1JaNhbF2UiWjzBsEXeAFe8/wUOc5pz5IBXb98jRoehCG72UE7LDRqUTjEaabSqasq1gRD9CgaI6ghaCIpuZpzQWcSqBdTQCJixyUu77SoFQ7xZdIgYFnvI4vQEHse5AYjkdzsUdXzivN8T/EGWKH1isTTFktvG+e2YQm4YLD4dBooQ3GsTiuVivxJCs3rqw4bN0yupVuKumh96b9+qS/NSIFrbJmzV53WwUmYnlRaELSuJt0SZupjH6N2HSMjsfcsJtllVmZJ9f/M6K7NJnP53j5IMZ3S7KOQVx+8rDj7nl3Vcyq3JIxryqyaIOlxy9y/AAM0Gd2lA6rjckoPSNDbGgyFDj1AXGz+9+9zvCj0OHDhUvuSSRbOpSpnxHxSH5IokK2wocm+pHQggiqMWalZDdptqLGOSSsIE8KM8KMQhN+hbUls7f1xGkZ+FWXDJPehYfDN/F/O7cv8xkS0f877YXQAl/8c3abakoKT+aw+lSPDqQpVFcEo8VWyl7SGI/POwtwZVf1C7qV7n3oPID6lrWvPDZvc3pRtLRgkbIzXhGpro2HqvfOrg5eoK3r2db1DaV64G31sytXffJ5HX1q9cF7YQRsrV6I3nPYaNnGg3vz1lZapRRZ4t8UC2TG7nToCHlPaMo1QrNupCqLvMTVid+82OOOQYVC7UEt6HP/uXAxKYWIxJAsEq84T61b936u/6UKIdSJ0q+b0Za+pzCxyexA0WTZ+YS2uFPcdh2fRp+S55iDqLEAhP6Ea3BD0vSOfOUUOeG/f/PEacwNVG1WI54ercCbht/xFAnyiB9KsyoiuT4jeJFhh7ZrWT/qkh/XkCyPDlvVf26/pX77Fy964K1M57/9OlV2WUcdaTiObU5IALlFNx25pRNVCFjaWWR1oy5om35C5+MbwkFX/l4fjBQprU0UCD2hD336ltd8Yt//F3L4ot1YRAKFm3pcw48MhYnj98rcbJB0sNGbEvBIUFBifxyRyIBIogEt0SSbAbcEtGigZ+W5YumbbZJG3Qk9CMzF+biq1IbHVS0RyE5VidRBBFc/CQhEOEvWye42vNjvT4FqiItmYxoyHQu3iAJSHQeaFti8/aD+Jd7FjnJPeAGYKW8wzbcd2UlqtfGqrogmARZcJBoC86lfPnggn2CyYJMeN28hsmZXHpg5Z4V8f6v1Tz/8oLnlicX8e6SYJjKBsxva3wEm4JfyE01RHj3iTbXrh33zkP9eyfu/eBTJxPP2I0VRlGfXSp+eMAx10+dHF1NVCdt4/Sg6lBLa9/qnicO25eceoLoxP437HwjtqVY7UKWIBMwFXQRpi7uxK/cZdHWhCB5VvSZ/4DAFNexaE1CbJsfVGSpxEh4SuSqr74KBIRgtg69oD1BSl8s86fkJAhf87/4QnVDfrd1HOEr92j7NVB5CN6xWImLbsN9tzTOOagKzir3AFGsu+oYWCRS0qN6zz6H9YkPSubr6vX5cxbPKE/svFvvw7J9m95c+PLEGa82pGtsMELj6BnPfzUCdxU4Tq7U0N5f03zLU3f1HBi9f/b0cEMq5xYgEK0eid8eeubvP3t91nvLsmE8FlZKb1WYGQwdffBhQ8uqM5oKCAdCZBqv77MI4GXtPAMRfyI04PSK52U516aDR2IO8auPQL4BJpll/ASDFNKVn3ySFlz3Wak0I4bBcViOpSu5ofKKVVKINFN8geO53uESuvIT6EWwiJIpQtvnIP4q5I7wArEMxYrz6Q0WTj4nV2Vlpcpm5kWlnNBXXnQ6zPO/ubPnsF5J8oScRHgyB4GAAEeWINxKWJgvlmUJ7XJP2fMqL1/gpiavyi61l+ETNUScUjJVgZskqUm3AsPOnQuzEOqVycjafZkvFgQP8pPaGiqvqVM77c3UIRthPSg7/KfOQ6pvAk8Bnb8owENCLEyJ7EgZiGwcuvL/VJvu/UTEUgEwr+YjXFj2iAgZubU9SsuUYuK1bFdrvR33QhfK6eDrNZ3xZEPa4JWz6tAWcQgCBWgcODY994kdyJQWlOXb7HSb1UxBGK2urXH1sMoh5YHB67L1U1rem9k0o291755av6DFyR6oU70gWtE3r0pwLKXhKtcL8lclS0C5aFy4exUFq1ZaFmlHWMartMxhZjiCOqXmZnOBosenPv/A5F8PPGTggnnL5n+4wohW5zKpgZXmfedf8ex7H0/4aLZGRem0yUEk3iuWN/SebuzeS66qjBXE1OERTjsxtscrOl0bSyfQVN4GYCL1BO826TIkK5LNTIopp9qBIGgkQX/BdbAcjZH2lMAgpYM4FT+JJukrhCIWBLm5L6KDvFMOLnLkTyGE3U5gZLqQ70JeUc/qaopi4P7lfBk5MSRMy476dCJCzJfk4B93BBf58t577zF/X30SRVQMTlg4Cdmsi4XImSk2GS6QSqek2BQTIH+dqZKfxHEKWbIIUjoXHBIcFeKR08wsmaOYnO5/+OGHBSkVimuuRJg4CCYL50HSX3Ce8ayQN3CjZI6QpUyYGQp5CI5KtR75U2hSaN7XBcSnJdD2rQwheLaGU6N333332JNPPuzQQ884/XQgzKTllJZiWKoepRpLeBATgFeS4SArIuOcbKcJz01Q43rFwwERn5x0yeayyk7DzAuFVRYB1hvHIjqc81IYgclTg4Jz1Qzxq3vukaooTEwBjQJ2WHpeVQdZtc9fhOls0RVASbWiuw8+sKpicGFJT+rTLM7Uv7dq7sBo8eg+I/cu3nXpwmUPvP7IEwufrAmtDkQzQT1gZu2ceglIPGiAvYaT5fymZsV0LFelXuKuBStJwOU8VtCxVfnLVCSUDGlNgUytlk+TNUbrMS88cs+cN0bsXlU345OJk7/QSoek163eY3DpdRde88z0aS9NmxKDz6fS8R5FrFed9qpv+97Z5w8q663qrHqsSC1zQ+EtG+xfMHmyovgTSB100EEE8f78p0epnE3RQfH6kPzNr+LFIcNTLHtJ9aQN4IY7iptEas/4OZZ+jqt0LomUVPHgU51L8jI6OYeFmcDxIk6KUCLgoQceJEON4h3MnMCMVEkTU1Cklnz6QwjHlSkRywG55fiLsH8ZUT5Jmse9zE0rS4aWt4RshgmohTjuC89O4FwFNxkXAvZzSmXmfEodDYb26+swk0suuQQaoEYjn6Tat8/QdVasWgkiQuHyCJmupLTR2K8GiHObfH1oW+iN7B8BoIhcTplBLRC8LMRPamUmckBUuqWxT67yoPzEwZeRI/Ykw4azO1dc/v3hu++BZ1JVnOjIuZVkVOlB0ICQL4fdZI0cGAAI8AX5yU9/9ScvJZoEMuvNUKYKK2eXGaWyvILaJUxSnpXq+J1hK+1l2lt68RRom3Fz765+7Y4p37v641O+NXHMqU+Mvuq1C56cc+eLn9199fhvHPv0kQc9eei1b1359rKnydVVQ6jzvuqfl5WtlsDYSnWyyVNldiQhUweLZOkscCf9mL1XSUL5dKubvmv+28bPv9P/8W+Om3nrXg+eEL3jeO2qo7Wrjjv+lVsmLLn3rD9dr/3wSO2K0cVXjA1cfpL+g7HaxWO07x69/3WXzjRb1DrzJn1xosBLoV7/Wl9askeIDp97cQ6AQqCgGiyT01UQBnTCUUZSZOkJ/ANj0AYREZxdlOAk3JFSGmRdQ6VoL2KSsTec1Tz++OPx5ZJqywEF2Co3KZnDUSlhsQCD4i4gJWesKLRz9tln05IQvySLcJ8QP8cd+em8886jDg3nLThrIiwWXsDpew6IMASiFd7Bg9AMtXkgAOQVOMehIaowMjeSAZG9CGElr8JhZk4lOM55HnvcseT6ppJJHid3gkdYEedLWQuHKjgdAtdnsSCW1E1EojIHYXhA46OPPiKZFiiBXiS7+hJVRJAfuSXTFZiwIj8oSksSDGAirIIUYgDFQsjm5WAKyCY5cYgv1oJmwZ/MAS2D/GS2hjlQylVUX3gQMKcWHkc6/FlxtoNaIbR/7vnnyQZhaewpDThu+r3vfe/hhx7yXv2mLgQ4gAVWnF/hEYr9cGJG9Bo5wQc5ibhmOCkqieDleBqgY25//etfhaiYIUwNkAJ5mCkTgAkyCkYBICJXlkgb/XO8HnKV/gEpcKPkghD2FsnJ9sYGaoIWyIeHlu7W3+jdK1cwqLC0X2liasvy15fMqAuZo/bZ94C+Q0ps9+MVM25/5+FxL9714uI36vQmK2zaVpaa51ga6MJS1VLpUOjXaLpuIG8QftTqOdCT14M5bZURuu6LOaX3/mDchJtO2df64S5lf3l35vRVrtmkJQpLrj/r+JN7Vd7x+pyJMxdoTgI4Ja2cEQtA1aUFxVrKGnfOJf1DRUETFdpLODUCsENIc70lr0+WqCgCd4GX0CcwBdUQlQAdIuGoF3qaFHTkQgxy8Ac8Bs94ik/yPFWBmeuuO/3U0+rW1dIn3JqiddWVVST9sCVgHvKErUU8cm7I3wlxFIntR7cYxBQTwLjFyOQwO8Uph+wy+J8vvgTrra1ZR0W8ww89bNWKlQvmzd9jt93vv/c+ytgwNBdCHlKnf2ZL8p1CiA+njthjOJKfSm0cin/5pX/+5dE/p9qSt918M8QGgxg0aBDncVkj/JvCXzALie+Tf0OH5BhwqA0BK2cUuQAIB3xBZVHVQGt4E5rzyJEjUStAU7GxaUmHom0CTL4gU5iPqrVEamaW+LJdXtaDKcGMkaJDBw9hpfxbuXzF1Vf9+N5f/0aV8AmFqyoqWR01kJYvXXbQAQfefdcvW5vJw3R/+9vfotRQggyOyWExxBpZb7AABuICniLPyXZSp/69HHG2QE0jn//bY38jNVfpUJ4mz76gS9MPi4UOgSFJiDJntltsGQ7owS7ZMnpG14Vfo+QDZ8DCYVRqZ7EEqg0duP8BLzz3PNtUVFD4+N8eo0JSOpWiW/aCuQFSNvfxxx4jU0VKBLF2yiz99c9/UYZ3IOi/JaXr9In9jh5D9m5ZtHTkTns7DRp1Onrt1OugHv3q0jWPzXr1g9Xzdq8aevrgoweU7NwW1V9qfvu2d+7/ztPX/+zdhyc1Tq8JNZqBrGvkYwZlTajJ5JBA7mJ1YRBpFHaNpO36PyyZdOhbvxl479m/fPSyI4tabjri0J21yI8+XNW4aG3Mih8wfPgD5xwbCWiXPv7u53O1prwVcyJRN8R7GKw0ZVa0pnX1N5xz+eg99o6iy3pJ4FL+Asr0ysZ+6dpIfF+99qPj0INwRy7hjuI+YZ9wloilx59+FF7phJaFAFEn6B0XkcgWsqM33nQTNMyuKCXBtvmVs/+ceITZC99Vhp8ncASVkU7IxlNOHstRPeDCHawX0IXT0tA8AjNRgKdLe+nFF8ED0AIGj1m4cMHCQbvs4jkklOgAUaCHv//97+KLQhOG2cOt995nH6/Uk7po+fvf/u7E449/ZsIEFQTznO0wfIpK8RTrogfEFyIdIkdg0l4gw32mjbLHHXGTSG98hypAbogciYGFzEhCt+LdoRmkKKkLqhyJFyOhNx7kT9YI1golQDzvvvcuZIPYYXVKYfnRj2jMgRhcZRwiPeLII9HgG5oaITmOxfIsugZCFRVDNHmGY7a+8gkfkLoB3GR0IMxi2UduqkUZyvYWnwJUhx9OzN2F8xeg3hUmCii9w8ZRQhaBPHbpydi6sAbYBOIXaxteQx4Y4g41gXqC2FFMpmevXiybsdBcABfqNFWU0D5YBZa2wngPLuAVp2FoA7eSOz7WdZ0sqYtMpRze5Ipc2Ll63wED17y24F9pe+2JiUFlseDifMvCtctqVjQeOHS/MSMPH7Dyi7dWfNxst81tnfdZ4xf/mD2pIlTWL1GxU2m1UxwJh+KhYIGjhTK8yDbVuqZxXUNT/aKm1c2ZZstpHda38ND99o2EEq/PXjlt3jqS6XbbY+R3Rw2OB7W73v9g3vzlhXa4zclqsYjD+7g0N2REwpaRChgHjhz+02POCOcowqacVq7uqGM0ec9Ns0FoYyNkKd4RsdPEOld6t3rHuxJl7LdvugBrGkByEkWQZDeJcMIDVK1HK4+eyflgNCsOqmNa4EITo0W8ROJNAfq4fER5Rr5Ro43UIk4YSPEIceGAu3Lm+NlnnxVK5gu/gliYNxyExzvFjoojCiwn6VTYhPiExC9FGzlsJS4rUJNuOcahzkx5/IdDjBTd8O1V6UF0bN8zQf+CvqKaSv8g8Y9//GOUQNgNYgQ64Q7er6KSYhFc9CBKgWiMKlPKs+WEYpXR/skn8CN4h9TIRK2llBZLU7ULQqp6sipd1cEXpGoEc+OgBn0i1sRmQxDx579ZgMdJGQ4Gt3jxYhkLnggNSNIvVoawBnV0wwvhMjGpLaLudaALmj/KKn/DpFgUVRRI6+dx4CkTAwiqjIOXS8jEhu8xnCFw/6pzXh1vmvF9UTSWp5RnCx7BYTfPSmeDpI1I7y27eP9B2GrBE6MVhLWKg3c7fVkyNWnhxNeDn/Uu7z2oqFdln/JlzTWvz32jX7Rq757DLj/49C/WrZ61esXcdWvr7OYlTm5OwxqrdiYzsdFdObGsea8zsk3eThDgGFZBj317lYyp7BMwIm8taZ28dClnavYctetlA/fXIg0vzv/0XzOXBBpDkUBpJuYmtCWpDEesDDOVRWdL1TXtNmjog1feUAiO8ZZqSMnj1grC4LZlg/nreX3WJ0uvdGD7yUbJ+VZ8V7nv1VlB4cE0gLSgEAkWi3dRjH6+4yTAOKGL5sbGaCiciMU+/OADCP3dt98eMmwYw6PASOxB8EAcm2pmHRV9SCpSJZ69lwLxL6aOHVo9SokPaZxVu+sXv2A4UBAt647bbjt57Nibrr8etw+/enXf2o+D+EXTRQIzjUya+mBhVVtN1U1UHmmMPPBDWZ6gjndYSaXCeR5O0Q74IimyLI1zMMwQQ5SqIswcfRUDFTtW6E0JdsOg5i2mNdyHrrC4YA1A0/cVi64hriDIHU0JU0G9KwNqyefnz53bUFeHqBl7yilqz1x3xqef8hN4SmEfFQb3DjpS8Qr/6SMPPwxDUcdTdQ1j+/zzz4cdMBN5MYHwEb7AStggWB6yS/xzTAx5joBV3BOHqtcS3oDvQbQMpge3knij8Dj0TPaaoTlFCc8CMsph472Wgk74VEDz8odVke1stqykZOratYraaUYYiT47BW+YgBjnNFD61Gln0Aka0NzZsxW7AhkkK6gLsfHOdMuZDF6ujJTQeRl6VquMll406qKAY/5x3XPLW2qr61M7l8RHVpaY1YWrmvLvtywtfX9Fz8qKMYP3PWywtaSxflZ97ZyGulp23MZ9rfhlQHdCDh06vcvL+lSWHRto/iIYf7nZWlHfNrigZNyxh/UHDsnsn2dMmr2sPtUKRlU5oTDvR8GAtGK7hQN1umkVRmJtebN4yC4PX/Oz4eESzAkgRaAlb6h0iHQ2VRSFVCH+9Q+0rE+WyvzsiP4DcfQljlwi6GDb2PTsHKhGFARNFbUKvwu18IhhoLIeMGp/eCc1Oy447/zrrruuoqqS+/izxhx/nPJeBoyrrr0GrRX9EMWSOVEkRk7cSd0DCIrdIrpFD3fcdjv+BuHWo7yS21wgEDYbE0AxxsgRrwnWFI/J2QtUTYiE+pMQJIcnwTywCsFLiVTuo23LmTt1oW1SvwuOEA5XVldhHyKcpfgajOboMWPwxYlByGJRwvGU8gWFGR/GPb+8G6MUPoLSCDZjSklZVIgWYsMoRf0DcXmcoiqKiwVDxYnCYUN3xRLG0GIUhDyy6MGHHy6vrMThieRZ+MUX55533v4HHkgA5Nbbb29qaWHy+NXmzJ939rnn8j6AUFiVGmKZ6MZcgIUJAyLVfyyKqiwz9+UbO0gbYaBK7uk650jhF0wbGjv+uOPw2yHN9hq5F74fVHEYDU6we+68a/CAgf169xk2eAjVxh76/R/22muvwuJiddoT0gLZUHA8tiJqAg5zdFEsCyneiX+BzqkRA2TawyTwFK/Al2LfkXDvvn3w8eBjRxmmpAu1bXEpVfXsCXtFaOM/GDF8hOKJTHvLvT6UB4nwolfF2jVNeR61Eq3whwdcm5xmTZn+dm1s3dpUsHWe1TNROSBePjQWM8qz+dTq2tq66uKyQ6tLj+6Fg7B/S0vb3EA4nWozLDMRChSEjKriwrIC6u46Nc2RIcnksEojPKh4bdL6eMmax5fWrGpKxdtyTowEIpZK2cGcFtaC0YCVazbx0YTsXFPTrv0GP3jVTXuWVGHwkXKtMnnVHL2jFFF1TFKx4A2Vg416otk/QCm0AdREBwM1xcsPN8VvJu8sgK722nPkY3/9mxSzwB+z/36jcFHwCPtNM+kffGUzpD1VknHP0HjK5A9G7bsfxUL9ghfEKq69+ho86Zj+eDhA+l/dfc8r/3yZO9M/+RQnx2mnnIoU5eVkkHdJUTE/8ey6tTUXf/uishLEqYrUMzSUD/ZQTgF8gpfjdP3JNdfy1LIlS2lPJZHzv3ler+qefMeNRPF/ZgXN8LnbrsNq1qzlE4uOaUMz8loBymcp5uK6s2fOmgDOPj3+7Ulv4Wc64rDDZeGYsrQR9gEXwEGCbkxsQ4piTJ3y4fHHHgfHYYZ4rTBoLzz/ApbAAqkTzZJ96OEs4T4z4T51fXBK8TheEwibmwQ56mvrQOjS4hL+hIsxIhTLxPA/sVPIH1EaIQb5gjqNboLzs32jHRc/3O7DdqPbLxYuolwT3jimgfUIbOfPnYe7hfuHHXIo/fft3eeN1//FT6pGjBfCggvfdMONgJrveNFwPtGMx9ks3FQEtAj/fOuCC7E5ZUVtLa0UagNKjI45Kq+ZgIylzijcAc8Qq3v/3feYFcO110nZdi8pSLot901+YPSjZ454dMx+fz95/z+fMOK3h+354KGHPHbwcc+MOevVM855DQf3Oee/8e1LJ195xbQfX/H+Ld9/58Zrp/7slum/uuWTu26e8rOfTLrhsue+f8TL4/Z87Hs73Xtm+W3HR647Srv2OO0np2rXnKHffIZ+1Qnh75+Q+PEZ+o/Gapcdq196Qvzik7RLxmhnHTT6V9d90FbDOz8I6qn6KRsJhWyU/qi//mXO5EfMkEWwPXgtWM4Go8aA8VLQyVP+1Uu4lFKkq1NRSsv1zD+pxdhQX48LAYYqdaJ4RI7eIXKRq3xHF5Iiv9QCJkFLkmAU40DV9pQoGqtgpuOAanB97Jm+/frRQOrA0EAcTlC+kgDotF5dOVV3y1BZI0gPJizpIzyL8IE8+LW8ooL4Bx4j2nMhu0SwSA0hUcx69e5du25dj4pyWaZaTkMDP2HEqim6mvTA1yGDB8Oq0GaVX4qK77pOPICWMCDghgTAtctWSCEMPilbzHAKkl5lDSbAWgApUJJpiC3d1NgoNiQA5CkBEY4ZeQ1ZRWUlQGAUHlfwD6kMR9YLnBkRgCgoeXoga+cm/YD9zEQWqAq0B1QRQ1Ux3Rsmm8lAJAKx9uG8zQIU7FdVdfVqLx1KipWgZDI9ukWBUpKJIfJ5Ik9QPgtRJWTxzLe28iyglpkDLlClrLwH4zJndh/jE7hxU8WZVJqRAs4xY8Zg/qDNMpDCDc9XuQ0ukxTx/PNzXvnb5OdW6s1mTM9ZyXgsnGsjh8GmJBmV4rVgJJe3MlmV0lIcTnBg2sv4CZKsymkP8shVuqRTp+qT8I8nVAqGyvRxTF6HR8o0gRXHSZlhXRlBWRW/zZVmtOMOPOyGiy4fFCsJk0FDRCMRot75Jk9CfnmpGznkohC941XNyt/TUVBHXC/KIvd8KkLPkkoibgz+9L+L84D2cl+KTfokLXfEVSi2kAwqzWSGbC1bCDlJDRtwghHbC9V4gov9UyN6OXHqAWWDkZOh0LHzQILxMgp9SmOZmI+msi7ldVCIoi6Vz+UxIFCcR5ib2NUECYnCMUmk0AsvvMBJVAm1yxA0kwAPX+RAmUhvuhJ3CH9yMQ3PjFLzVIvqOO/Gn8oX0nEMXeIr7b/iaPTS5dRCPCeNpOmoih7eJdPzjX+Bp3xKA9/mFMDKnGViCrz59qwsRhHy4ybwJCtcEgNpKV4AD9L/LtfAiBKAlZCj1LAXShOfVvuKPEe3+J/gWXKQAC726suv0AAPOQoOYTOUahl6m2WtkrhjJp2Y9Vnbovtf/9vk1bOjlYXpbFK3C6CvJNVNKC4SjPI6bML7ZAzy3imIUBWghVy9jEmNd8LaThSzx1VGMu928HLwODpCnNON5uJ5XsMBPNLZqBug9Fcukw72KPn1KZece9TJvHUnnMaJoGtETnhtu2MWbFCpWXZnvWt9sgR24iqQHfXtfv8xiQcohJAMyQ4nkBCqcjB0fBH0EjJWRnzH2UUxToRKZY991JE2/lP+oNJSaNhnAf7QnfHSR0dBHXH2CvLxXf6UNu3c2ntYEEUYDQ/KzGWBcslPLA2nDpo8nWBwEqm7/PLLpbH82jnT3ScDH/U7Mwja+zxIqFHw3p+DDwq6BarMSsiJT34SN5sIQ4GtDCf7JZAR1iNAECB3ZriMLtViZUR+lR4EFP4eCdBkZzvPme+daf7f5N2RMyyTlGbSv4DIv89AkydPRi1nOIQnLgNSVopLSsR/u83Ikvy6iJElScBwWrSWp9//5/jp7y23k2bYDMfiGc2FMlUCIhzY1IJuIGVQWkferYNg9D6ZCqGjdARWTXaBOoEiCqby1dmUITHrG4l08Tb2FKV9M+aJ+x5yzUXfHVXQm0K4EGHYAP1M/o/xTO2l9erB+gi23peNHwkFlIJkgklCaYLZAmvZBrZf1C3ZUcEAwebOe+YPKbEQP+WdTkROclMwid4E+fweBGl81JGumA+Xr/v5QkkGFR9vuxDwIjfiMKRbmScY6R8W9eWArJRPEY/+QELG/sK5Dz4xhJQ16qwuCj2I99LXDnxlQUhLhvDlpwwqC+wsu5gnI4ri4BOqdM6dzpJZ5i/EKY7WzjyOO7RnaDlL4FOsvyM+v5M76yk1vkYjYBFSF0zw2YRMT2Ylj8ta5DuA8olT0EPm4/8q9pZ02M44vIqB69dN3RT+ftX9NFV/CGaqsLgqaZAN6J83r5ww6c0nlrxK/k0ah10iRk1BJ53TTCcapqYyDie1GlAY1ZRX4Xkp6iisGSMU5p6TxY3KNuAxVnU0tRhRx4CTzjhNbXsOGPrDb5x/4p6HFKl3b6KAqFWQhQ51t6XbyuIJ3tsDAX/VlD1g+mCV1p3d6wJQf5uFCLkEk9p1IS9pTt30KjLIligkNtQBFKWFepdghmLPXnKD5BLh4uv8AllUe7U9mjpnoHQ/ztXhA4RHGQa2H5YP3QqP97m+TEb23mcNMj1a8kUoXCG3N5xgj5yFVdig/KjtExZqkc8vaY8dhCEcByetSsvwjj5IrJXlK6mlt58gE9Enc+BSbxlTtd1U9Tql74Uj/qoFXLIWHw5io3JfIkbE5clz4os69tHBa3whKUv2mV37iB5xqiV3HJr992Q8QcQEhJIV5wXVPMiQFitcQJQIpkq36lWIbLSZa1dNvVWLY9bDXA8NTKWQyxplnj7xCwL4SoHPqX0TQ93hFfHydnCvTKlvh28raZnSzAQGnar17WgFShSiu4Ys463stNemvPvajA9Xmq14XW3wznvhWjinyvO55Mahs3onSOTVzSmVlApmmAHTVttP9ES96Sacb6LynT1y8LBLTzrz5OEH9HBpYoL90GKEYvzsWiiYNFXV/HRrSwn5d10LyW5EWoqaKtSoEKcDU30XgiC3UCBop7KlvLrjQlSCHPI/X7eRrjzYeK9k84iZjZRPSc3hi2Ch3JTtV7VSvJ32dSHp0/8ULiuDyiiy674KKg/SyB9LzkAIGqkIzZePffnY7I8rbEVNvuMtPT5p+SxG+vEnRmMRLx6bVn5RBpWnGFrQWmEkm4107YTr7eSkE8D2jAKPf6mbHTaFPxMRMr5y4cvb9QAiuyAgUhyzXRa0b5NaVAecxfAWrqqmJ2VfvMM9wn2U3uHNR7iSsDl/NxW/8/ZLQanjXU8+DIXZdVa12nHDs5kVZYqnxzcc/j1Bf2+36ovVzAEQK5jwXnmZC0EuvKhHZQsEkyF3Xqb21QUfvzxjyrw1SzMWL3qxtLCHS+3BNKiLf5ArwcVSzUrr+XQITRbdAd04FNdLyr6194FnHzFmZEnfIuiQtuoVg6yIY2OMahvY5hgmhgspq4wBpK7Hbb/y2pZ1Tb5ysO4G3RDYQSAAu4SvqCugJzV7aWvdtGXzZy9Z9GLNfKratmW8fG9YA2YXtITamFbijxOApUXFw3v3P2TnYUf0G7ZH7/4VRpfIbEtX3U2WWwqx7vb/HyDAeS3iGd4RZ+XRtilzrg5WctLSzljZ5mRbS1trMqNCgKLN9ogVEL8tLSwqjiUIcKF1qIPSXF2MeGwhzLrJcgsB1t38/wUEyGZUySdQlwo7eUtSXh7vNdcERLBBOpxbmDrotRyMRqcPo54r/ValRGL9Q5hdcuBsOcS6yXLLYdb9xP8+BHwXoKJHj9Y8wqRmgXLDqVu8I5dLiVPPwCbMQWUGLO6O0irq7SzKm7DxF/59TQh1k+XXBGD34/+TENgUWUKL4u7xDyajweKUI1SiPF5fdtjg7+QFCttj/d1kuT2g2t3njg6BTSmxnq9UvaqIL95RhHaZifsWFzNeWO9QFi7yAK58lNgvvTlk2y26myy3HSy7e/rfgcCmXD4cPhE5icD0Eny8Us+OljJUhEP+9OKzuHy8Q5DqWMG2v/4PdByZqUy368AAAAAASUVORK5CY,II='
                , fit: [160, 160],
                margin: [0, 10],

              },

            ],
          ]
        },


      },

      content: [

        {
          style: 'tableExample',
          table: {
            widths: [55, '*'],

            body: [

              [

                { text: 'Subject', fillColor: '#d9d9d9', noWrap: true, bold: true },
                { text: this.generalInfoData.subjectAtd, style: this.getLanguageStyle(this.generalInfoData.subjectAtd) }
              ],
            ]
          },
          layout: {
            hLineWidth: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 1 : 1;
            },
            vLineWidth: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? 1 : 1;
            },
            hLineColor: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
            },
            vLineColor: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
            },
          },
        },

        {
          style: 'tableExample',
          table: {
            widths: [55, 165, 48.5, 230],

            body: [
              [{ text: 'Meeting Number', fillColor: '#d9d9d9', bold: true, border: [true, false, true, true], },
              { text: this.generalInfoData.momNoAtd, border: [true, false, true, true], },
              { text: 'Meeting Room', fillColor: '#d9d9d9', bold: true, border: [true, false, true, true] },
              { text: this.generalInfoData.meetingRoomName, border: [true, false, true, true], style: this.getLanguageStyle(this.generalInfoData.meetingRoomName) },

              ],

            ],

          },
          layout: {
            hLineWidth: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 1 : 1;
            },
            vLineWidth: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? 1 : 1;
            },
            hLineColor: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
            },
            vLineColor: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
            },
          },
        },
        {
          style: 'tableExample',
          table: {
            widths: [55, 65, 20, 62, 48.5, '*'],
            body: [
              [{ text: 'Date', noWrap: true, fillColor: '#d9d9d9', bold: true, border: [true, false, true, true] },
                { text: formatDate(this.generalInfoData.fMeetingDateAtd, 'dd/MM/yyyy', 'en-US') , border: [true, false, true, true] },
              { text: 'Day', noWrap: true, fillColor: '#d9d9d9', bold: true, border: [true, false, true, true] },
              { text: this.DayAsString(new Date(this.generalInfoData.fMeetingDateAtd).getDay()), border: [true, false, true, true] },
              { text: 'Time', noWrap: true, fillColor: '#d9d9d9', bold: true, border: [true, false, true, true] },
              { text: this.CoverttoTime(this.generalInfoData.fStartTimeAtd) + ' to ' + this.CoverttoTime(this.generalInfoData.fEndTimeAtd), border: [true, false, true, true] }],
            ]
          },
          layout: {
            hLineWidth: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 1 : 1;
            },
            vLineWidth: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? 1 : 1;
            },
            hLineColor: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
            },
            vLineColor: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
            },
          },
        },

        { text: 'Attendees', ineHeight: 2, margin: [0, 10, 0, 0], style: 'subheader', bold: true },
        {
          table: {
            headerRows: 1,
            widths: ['*', 100],
            body: this.buildAttendeeTable(this.attendeesList, ['nameAtd', 'status'])
          },
          layout: {
            hLineWidth: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 1 : 1;
            },
            vLineWidth: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? 1 : 1;
            },
            hLineColor: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
            },
            vLineColor: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
            },
          },
        },


        { text: 'Meeting Discussions', ineHeight: 2, margin: [0, 10, 0, 0], style: 'subheader', bold: true },
        {
          table: {
            widths: ['auto', '*', 'auto', 'auto'],
            headerRows: 1,
            body: this.buildAgendaTable(this.findMainAgenda(), ['note', 'userName', 'dueDate'])

          },
          layout: {
            hLineWidth: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 1 : 1;
            },
            vLineWidth: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? 1 : 1;
            },
            hLineColor: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
            },
            vLineColor: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
            },
          },
        },
        {
          alignment: 'justify',
          margin: [0, 20, 0, 0],
          columns: [
            [{


              alignment: 'center',
              text: 'Initiated By ',
              bold: true,
              margin: [0, 0, 0, 0],
            },

            ],
            [


              {
                text: 'Reviewed By ',
                alignment: 'center',
                bold: true,
                margin: [0, 0, 0, 0],
              },


            ],

            [


              {
                text: 'Approved By ',
                alignment: 'center',
                bold: true,
                margin: [0, 0, 0, 0],
              },


            ]

          ],
        },
        {
          alignment: 'justify',
          margin: [0, 10, 0, 0],

          columns: [
            [{
              text: '',
              alignment: 'center',
              bold: false,
              margin: [0, 0, 0, 0],
            },


            ],
            [

              {
                alignment: 'center',
                border: [false, false, false, false],
                image: this.generalInfoData.reviewerSign ? this.generalInfoData.reviewerSign : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=',
                margin: [20, 0, 0, 0],
                fit: this.generalInfoData.reviewerSign ? [100, 100] : [0, 0],
                width: this.generalInfoData.reviewerSign ? 40 : 1,
                height: this.generalInfoData.reviewerSign ? 40 : 1,
              }


            ],

            [

              {
                alignment: 'center',
                border: [false, false, false, false],
                image: this.generalInfoData.approverSign ? this.generalInfoData.approverSign : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=',
                margin: [20, 0, 0, 0],
                fit: this.generalInfoData.approverSign ? [100, 100] : [0, 0],
                width: this.generalInfoData.approverSign ? 40 : 1,
                height: this.generalInfoData.approverSign ? 40 : 1,
              }


            ]

          ],
        },

        {
          alignment: 'justify',
          margin: [0, 10, 0, 0],
          columns: [
            [{


              alignment: 'center',
              text: this.generalInfoData.initiatedBy,
              bold: false,
              margin: [0, 0, 0, 0],
            },

            ],
            [


              {
                text: this.userService.getUserName(this.generalInfoData.reviewerId),
                alignment: 'center',
                bold: false,
                margin: [0, 0, 0, 0],
              },


            ],

            [


              {
                text: this.userService.getUserName(this.generalInfoData.approverId),
                alignment: 'center',
                bold: false,
                margin: [0, 0, 0, 0],
              },


            ]

          ],
        },
     

      ],
      styles: {
        ar_lang: {
          font: 'NotoKufiArabic',
        },
        en_lang: {
          font: 'Roboto',
        }
      },


    }



  }

}

