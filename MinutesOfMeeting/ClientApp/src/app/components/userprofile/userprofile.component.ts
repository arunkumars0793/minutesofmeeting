import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { UtilityService } from '../../services/utility.service';
import { MasterMOMService } from '../../services/master-mom.service';
import { NgxSpinnerService } from "ngx-spinner";
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { DialogEditEventArgs, SaveEventArgs, GridComponent, ExcelExportProperties } from '@syncfusion/ej2-angular-grids';
import { Query } from '@syncfusion/ej2-data';
import { ClickEventArgs } from '@syncfusion/ej2-navigations/src/toolbar';
import { usermaster, interaction, division } from '../../models/admin';
import { AutoCompleteComponent, FilteringEventArgs } from '@syncfusion/ej2-angular-dropdowns';
import { ToastrUtilsService } from './../../services/toastr-utils.service';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { UserService } from '../../services/user.service';


const styles = ({
  actions: {
    display: 'flex'
  },
  cropping: {
    maxWidth: '400px',
    height: '300px'
  },
  flex: {
    flex: 1
  },
  range: {
    textAlign: 'center',
    maxWidth: '500px',
    margin: '14px'
  }
});

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css'],
  providers: [ToastrUtilsService]
})
export class UserprofileComponent implements OnInit {
  public data: any;
  public user_data: any;
  public division_data: any;
  public editSettings: Object;
  public toolbar: string[];
  public pageSettings: Object;
  public orderData: object;
  public interactionData: object[];
  public usermasterForm: FormGroup;
  public submitClicked: boolean = false;
  public pageAction: string = 'add';
  public ntUser: string = '';
  public ntUserData: any;
  // public iseditmode: boolean = true; 
  public permission: number = 1;
  public desingation: string = '';
  public displayname: string = '';
  public user: any = {};
  login_UserID: number; // number

  todayTime = new Date().toLocaleTimeString()
  todayDate = new Date().toLocaleDateString();

  constructor(public utilityService: UtilityService,
    private toastr: ToastrUtilsService,
    public momMasterservice: MasterMOMService,
    private userService: UserService,
    private coolDialogs: NgxCoolDialogsService,
    private spinner: NgxSpinnerService,
    private router: Router) { }

  ngOnInit() {
    this.user = this.userService.getUserInfo();
    this.login_UserID = this.user.id;
    this.displayname = this.user.displayname;
    this.desingation = this.user.desingation;
    this.getDivisions();
    this.loadUserMaster();

    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, mode: 'Dialog' };
    if (this.permission == 1) {
      this.toolbar = ['Add', 'Edit', 'Delete', 'Search', 'ExcelExport'];
    }
    else
      this.toolbar = ['Search', 'ExcelExport'];


    // this.loadNTUserMaster();
  }

  loadUserMaster() {
    this.spinner.show();
    let inpValue = new usermaster();
    inpValue.languageUno = 1033;
    inpValue.condition = 8;
    inpValue.userUno = this.login_UserID;

    
    this.momMasterservice.getUserMasters(inpValue)
      .subscribe(data => {
        
        this.user_data = data[0];
        this.pageAction = 'edit';
        this.usermasterForm = this.createFormGroup(this.user_data);
        this.spinner.hide();
      },
        () => {
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });
  }

  loadNTUserMaster() {
    this.spinner.show();
    let inpValue = new usermaster();
    inpValue.languageUno = 1033;



    
    this.momMasterservice.getUserMasters(inpValue)
      .subscribe(data => {
        
        this.ntUserData = data;
        this.spinner.hide();
      },
        (error) => {
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });
  }

  getDivisions() {

    let inpValue = new division();
    //inpValue.id = 3;
    inpValue.languageUno = 1033;
    inpValue.condition = 1;

    this.momMasterservice.getDivisions(inpValue)
      .subscribe(data => {
        this.division_data = data;
      },
        (error) => {
          //Called when error
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
      });
  }



  createFormGroup(data1: any): FormGroup {
    
    return new FormGroup({
      id: new FormControl(data1.iD),
      ntUsername: new FormControl(data1.ntUsername, Validators.required),
      prNumber: new FormControl(data1.prNumber, Validators.required),
      displayNameEn: new FormControl(data1.displayNameEn, Validators.required),
      displayNameAr: new FormControl(data1.displayNameAr, Validators.required),
      firstname: new FormControl(data1.firstname),
      lastname: new FormControl(data1.lastname),
      emailId: new FormControl(data1.emailId, Validators.email),
      designationEn: new FormControl(data1.designationEn),
      designationAr: new FormControl(data1.designationAr),
      divisionUno: new FormControl(data1.divisionUno),
      divisionName: new FormControl(data1.divisionNameEn),
    });
  }
  actionBegin(args: SaveEventArgs): void {
    if (args.requestType === 'beginEdit' || args.requestType === 'add') {
      this.pageAction = (args.requestType === 'beginEdit') ? 'edit' : 'add';
      if (this.permission == 1) {
        this.submitClicked = false;
        this.usermasterForm = this.createFormGroup(args.rowData);
        
      }
      else {
        args.cancel = true;
      }
    }

    if (args.requestType === 'save') {
      this.submitClicked = true;
      //this.AddEditMeetingType(args.data)
      if (this.usermasterForm.valid) {
        args.data = this.usermasterForm.value;
        if (!this.AddEditUserMaster(args.data)) {
          this.submitClicked = false;
          args.cancel = true;
        }
        this.submitClicked = false;
      } else {
        args.cancel = true;
        //this.submitClicked = false;
      }

    }


    if (args.requestType === 'delete') {
      args.cancel = true;
      this.coolDialogs.confirm('Please confirm to delete?')
        .subscribe(res => {
          if (res) {
            this.DeleteInteraction(args.data);
          }
        });
    }
  }


  actionComplete(args: DialogEditEventArgs): void {
    // this.iseditmode = false;
    if ((args.requestType === 'beginEdit' || args.requestType === 'add')) {
      const dialog = args.dialog;
      dialog.width = 900;
      // Set initail Focus
      if (args.requestType === 'beginEdit') {
        let objdata: any = args.rowData;
        dialog.header = args.requestType === 'beginEdit' ? 'Details of ' + objdata.displayNameEn : '';
        // this.iseditmode = true;
      }

    }

  }

  RedirectToHome() {
    this.router.navigateByUrl('dashboard');
  }
  ValidateForm() {

    this.submitClicked = true;
    if (this.usermasterForm.valid) {
      let objdata = this.usermasterForm.value;
      this.AddEditUserMaster(objdata);
    } else {
      return false;
    }

  }
  AddEditUserMaster(data1: any) {
    
    if (this.usermasterForm.invalid) {
      this.toastr.ErrorCustomMessage('Something went wrong');
      return false;
    }
    if (this.DuplicateNameCheck(this.utilityService.isStringNull(data1.firstname), this.utilityService.isNumericNull(data1.id))) {
      this.toastr.ErrorCustomMessage('Duplicates not allowed');
      return false;
    }
    else {
      
      let objUser = new usermaster();
      objUser.actionType = this.pageAction;
      objUser.iD = this.login_UserID;
      objUser.ntUsername = this.utilityService.isStringNull(data1.ntUsername);
      objUser.prNumber = this.utilityService.isStringNull(data1.prNumber);
      objUser.displayNameEn = this.utilityService.isStringNull(data1.displayNameEn);
      objUser.displayNameAr = this.utilityService.isStringNull(data1.displayNameAr);
      objUser.emailId = this.utilityService.isStringNull(data1.emailId);
      objUser.firstname = this.utilityService.isStringNull(data1.firstname);
      objUser.lastname = this.utilityService.isStringNull(data1.lastname);
      objUser.designationEn = this.utilityService.isStringNull(data1.designationEn);
      objUser.designationAr = this.utilityService.isStringNull(data1.designationAr);
      objUser.divisionUno = this.utilityService.isNumericNull(data1.divisionUno);

      objUser.userUno = 1;
      
      this.momMasterservice.addEditUserMaster(objUser)
        .subscribe(data => {
          
          if (data != null && data != undefined && data != '') {
            this.toastr.ErrorCustomMessage('Something went wrong');
          }
          else {
            this.toastr.SaveMessage();
            this.loadUserMaster();

          }
          this.spinner.hide();
        },
          () => {
            //Called when error
            this.spinner.hide();
          }
        ).add(() => {
          //Called when operation is complete (both success and error)
          this.spinner.hide();
        });
      return true;
    }
  }

  DuplicateNameCheck(firstname: string, id: number) {
    if (this.data != null && this.data != undefined) {
      var rtnValue = false;
      this.data.forEach((grdData) => {

        if (grdData.firstname.toLocaleLowerCase() == firstname.toLocaleLowerCase() && id != grdData.id) {
          rtnValue = true;
        }
      });

      return rtnValue;
    }
  }

  DeleteInteraction(data1: any) {

    let objUser = new usermaster();
    objUser.actionType = this.pageAction;
    objUser.userUno = data1[0].userUno;
    // objUser.userUno = 1;

    
    this.momMasterservice.deleteUserMaster(objUser)
      .subscribe(() => {
        
        this.toastr.DeleteMessage();
        this.loadUserMaster();
        this.spinner.hide();
      },
        () => {
          this.toastr.ErrorMessage();
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {

        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }

  public focusIn(target: HTMLElement): void {
    target.parentElement.classList.add('e-input-focus');
  }

  public focusOut(target: HTMLElement): void {
    target.parentElement.classList.remove('e-input-focus');
  }
  toolbarClick(args: ClickEventArgs): void {
    
    if (args.item.id === 'grdData_excelexport') {
      const excelExportProperties: ExcelExportProperties = {
        includeHiddenColumn: true,
        fileName: 'user_' + new Date().toLocaleDateString() + '.xlsx'
      };

    }
  }

  public onFilteringNTUser: any = (e: FilteringEventArgs) => {
    // take text for highlight the character in list items.   
    let query: Query = new Query().select(['ntUsername', 'value']);
    query = (e.text !== '') ? query.where('ntUsername', 'startswith', e.text, true) : query;
    e.updateData(this.ntUserData, query);
  };

  onchangeNTUser(e) {
    
    this.ntUser = (e.itemData != null) ? e.itemData.value : 0;

  }
  get ntUsername(): AbstractControl { return this.usermasterForm.get('ntUsername'); }
  get displayNameEn(): AbstractControl { return this.usermasterForm.get('displayNameEn'); }
  get displayNameAr(): AbstractControl { return this.usermasterForm.get('displayNameAr'); }
  get emailId(): AbstractControl { return this.usermasterForm.get('emailId'); }
  get divisionUno(): AbstractControl { return this.usermasterForm.get('divisionUno'); }
}

















