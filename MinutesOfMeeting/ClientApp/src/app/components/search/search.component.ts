import { Component, OnInit, Input } from '@angular/core';

import { UtilityService } from '../../services/utility.service';
import { UserService } from '../../services/user.service';
import { MasterMOMService } from '../../services/master-mom.service';
import { SpinnerVisibilityService } from 'ng-http-loader';
import { momDashboard, momDashboard_MyMeetings, AttendeesAtd, momDashboard_PartofMeetings } from '../../models/MOMForm';
import { Params } from '@angular/router';
import { EncryptDecrypt } from './../../models/common';
import { GenInfoAtd, cmtObject } from './../../models/MOMForm';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ToastrUtilsService } from '../../services/toastr-utils.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  user: any = {};
  login_UserID: number;
  checkBoxVal: number = 1;
  public lstMOMDashboard_MyMeetings: momDashboard_MyMeetings[] = [];
  isTBT = false;
  public month: number = new Date().getMonth();
  public fullYear: number = new Date().getFullYear();
  public start: Date = new Date(this.fullYear, this.month - 1, 7);
  public end: Date = new Date(this.fullYear, this.month, 25);
  public searchText: any;
  searchForm: FormGroup;
  submitted = false;

  constructor(public momMasterservice: MasterMOMService, public utilityService: UtilityService, private _router: Router, private spinner: SpinnerVisibilityService, private _activatedRoute: ActivatedRoute,
    private userService: UserService, private formBuilder: FormBuilder, ) {

  }

  ngOnInit() {
    this.user = this.userService.getUserInfo();
    this.login_UserID = this.user.id;
    this.isTBT = this.user.isTBT == 'false' ? false : true;
    var objEncryptDecrypt = new EncryptDecrypt();
    this._activatedRoute.queryParams.subscribe((params: Params) => {
      let QText = params['QText'];
      this.searchText = (QText != null && QText != '') ? objEncryptDecrypt.DecryptText(QText) : '';
      this.searchForm = this.formBuilder.group({
        searchText: [this.searchText, Validators.required],

      });
      this.getSearchList();
    });


  }
  get t() { return this.searchForm.controls; }

  search() {
    this.submitted = true;
    var searchText = this.searchForm.value.searchText;
    if (searchText != undefined && searchText.trim() != "") {
      this.searchText = searchText;
      this.getSearchList();
    }

  }
  createFormGroup(QText): FormGroup {
    return new FormGroup({
      searchText: new FormControl(QText, Validators.required),
    });
  }

  getSearchList() {

    let inpValue = new momDashboard();
    var searchText = this.searchForm.value.searchText;
    inpValue.userUno = this.login_UserID;
    inpValue.actionType = this.checkBoxVal == 2 ? 'ATTENDANCE' : this.checkBoxVal == 3 ? 'TBD' : 'MEETING';
    inpValue.searchText = this.searchText;
    
    this.momMasterservice.getSearchList(inpValue)
      .subscribe((data: any) => {
        this.lstMOMDashboard_MyMeetings = data.lstMOMDashboard_MyMeetings;
        this.spinner.hide();
      },
        (error) => {
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }


  loadUpdateMeeting(id: any) {
    var objEncryptDecrypt = new EncryptDecrypt();
    if (this.checkBoxVal == 2)
      this._router.navigate(['/add-update-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QType: objEncryptDecrypt.EncryptText('ATTENDANCE') } });
    else if (this.checkBoxVal == 3)
      this._router.navigate(['/add-update-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QType: objEncryptDecrypt.EncryptText('TBD') } });
    else
      this._router.navigate(['/add-update-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QType: objEncryptDecrypt.EncryptText('MEETING') } });
  }

  loadAddMeeting() {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/add-update-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(0), QType: objEncryptDecrypt.EncryptText('MEETING') } });
  }

  loadViewMeeting(id: any) {
    var objEncryptDecrypt = new EncryptDecrypt();
    if (this.checkBoxVal == 2)
      this._router.navigate(['/view-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QMType: objEncryptDecrypt.EncryptText('ATTENDANCE') } });
    else if (this.checkBoxVal == 3)
      this._router.navigate(['/view-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QMType: objEncryptDecrypt.EncryptText('TBD') } });
    else
      this._router.navigate(['/view-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QMType: objEncryptDecrypt.EncryptText('MEETING') } });

   }

  loadAttList(val = 0) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/attendance-list'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(val) } });
  }

  loadMeetingList(val = 0) {
    if (this.checkBoxVal == 2)
      this.loadAttList();
    else if (this.checkBoxVal == 3)
      this.loadTBTList();
    else {
      var objEncryptDecrypt = new EncryptDecrypt();
      this._router.navigate(['/meeting-list'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(val) } });
    }
  }

  loadTBTList(val = 0) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/tbd-list'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(val) } });
  }


  TypeCheckBoxSelection(val) {
    this.checkBoxVal = val;
    this.getSearchList();
  }



}

