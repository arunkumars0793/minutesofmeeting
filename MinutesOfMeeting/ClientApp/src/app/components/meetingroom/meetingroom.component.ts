import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { GridComponent, SaveEventArgs, DialogEditEventArgs, ExcelExportProperties, FilterSettingsModel, IFilter } from '@syncfusion/ej2-angular-grids';
import { UtilityService } from '../../services/utility.service';
import { MasterMOMService } from '../../services/master-mom.service';
import { NgxSpinnerService } from "ngx-spinner";
import { MeetingRoom, MeetingLocation } from '../../models/MasterMOM';
import { ClickEventArgs } from '@syncfusion/ej2-navigations/src/toolbar';
import { UserService } from './../../services/user.service';
import { ToastrUtilsService } from './../../services/toastr-utils.service';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { Router } from '@angular/router';
@Component({
  selector: 'app-meetingroom',
  templateUrl: './meetingroom.component.html',
  providers: [ToastrUtilsService]
})
export class MeetingroomComponent implements OnInit {

  public data: any;
  public division_data: any;
  public user_data: any;
  public editSettings: Object;
  public filterOptions: FilterSettingsModel;
  public filter: IFilter;
  public toolbar: string[];
  public pageSettings: Object;
  public orderData: object;
  public locationUno_data: any
  public orderForm: FormGroup;
  public submitClicked: boolean = false;
  public pageAction: string = 'add';
  public formatoptions: Object;
  @ViewChild('grid', { static: false }) public grid: GridComponent;
  public permission: number = 1;
  user: any = {}; login_UserID: number; // number



  constructor(public utilityService: UtilityService,
    private userService: UserService,
    private toastr: ToastrUtilsService,
    public momMasterservice: MasterMOMService,
    private spinner: NgxSpinnerService,
    private _router: Router,
    private coolDialogs: NgxCoolDialogsService) { }



  ngOnInit() {
    var user = this.userService.getUserInfo();
    this.login_UserID = user.id;
    var Role = Number(user.role);
    if (Role != 1) {
      this._router.navigate(['/dashboard'])
    }
    this.loadMeetingRoom();
    this.loadMeetinglocation();
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, mode: 'Dialog' };

    if (this.permission == 1) {
      this.toolbar = ['Add', 'Edit', 'Delete', 'Search', 'ExcelExport'];
    }
    else
      this.toolbar = ['Search', 'ExcelExport'];

    this.pageSettings = { pageSize: 20, pageSizes: true };
    this.filterOptions = {
      type: 'Menu'
    };
    this.filter = {
      type: 'CheckBox'
    };
    this.formatoptions = { type: 'date', format: 'dd/MM/yyyy' };
  }

  loadMeetinglocation() {
    
    this.spinner.show();
    let inpValue = new MeetingLocation();
    inpValue.languageUno = 1033;
    inpValue.userUno = this.login_UserID;
    
    this.momMasterservice.getLocation(inpValue)
      .subscribe(data => {
        
        this.locationUno_data = data;
        //console.log(this.postData);
        this.spinner.hide();
      },
        () => {
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });
  }


  // Get MeetingRoom list
  loadMeetingRoom() {
    
    this.spinner.show();
    let inpValue = new MeetingRoom();
    inpValue.languageUno = 1033;
    inpValue.userUno = this.login_UserID;
    
    this.momMasterservice.getMeetingRoom(inpValue)
      .subscribe(data => {
        
        this.data = data;
        //console.log(this.postData);
        this.spinner.hide();
      },
        (error) => {
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });
  }
  createFormGroup(data1: any): FormGroup {
    
    return new FormGroup({
      meetingRoomUno: new FormControl(data1.meetingRoomUno),
      locationUno: new FormControl(data1.locationUno, Validators.required),
      meetingRoomCode: new FormControl(data1.meetingRoomCode, Validators.required),
      meetingRoomName: new FormControl(data1.meetingRoomName, Validators.required),
    });
  }

  actionBegin(args: SaveEventArgs): void {
    
    if (args.requestType === 'beginEdit' || args.requestType === 'add') {
      this.pageAction = (args.requestType === 'beginEdit') ? 'edit' : 'add';
      if (this.permission == 1) {
        this.submitClicked = false;
        this.orderForm = this.createFormGroup(args.rowData);
      }
      else {
        args.cancel = true;
      }
    }
    if (args.requestType === 'save') {
      this.submitClicked = true;
      //this.AddEditMeetingType(args.data)
      if (this.orderForm.valid) {
        args.data = this.orderForm.value;
        if (!this.AddEditMeetingRoom(args.data)) {
          this.submitClicked = false;
          args.cancel = true;
        }
        this.submitClicked = false;
      } else {
        args.cancel = true;
        //this.submitClicked = false;
      }
    }

    if (args.requestType === 'delete') {
      args.cancel = true;
      this.coolDialogs.confirm('Please confirm to delete?')
        .subscribe(res => {
          if (res) {
            this.DeleteMeetingRoom(args.data);
          }
        });
    }
  }
  actionComplete(args: DialogEditEventArgs): void {
    if ((args.requestType === 'beginEdit' || args.requestType === 'add')) {
      const dialog = args.dialog;
      dialog.width = 900;
      // Set initail Focus
      if (args.requestType === 'beginEdit') {
        let objdata: any = args.rowData;
        dialog.header = args.requestType === 'beginEdit' ? 'Details of ' + objdata.meetingRoomCode + ":" + objdata.meetingRoomName : '';
      }

    }

  }

  AddEditMeetingRoom(data1: any) {
    
    if (this.orderForm.invalid) {
      this.toastr.ErrorMessage();
      return false;
    }



    if (this.DuplicateNameCheck(this.utilityService.isStringNull(data1.meetingRoomCode), this.utilityService.isNumericNull(data1.meetingRoomUno))) {

      this.toastr.WarnMessage('Duplicates not allowed');
      return false;
    }
    else {
      
      let objMeetingRoom = new MeetingRoom();
      objMeetingRoom.meetingRoomUno = (this.pageAction == "add") ? 0 : this.utilityService.isNumericNull(data1.meetingRoomUno);
      objMeetingRoom.meetingRoomCode = this.utilityService.isStringNull(data1.meetingRoomCode);
      objMeetingRoom.meetingRoomName = this.utilityService.isStringNull(data1.meetingRoomName);
      objMeetingRoom.locationUno = this.utilityService.isNumericNull(data1.locationUno);

      objMeetingRoom.userUno = this.login_UserID;

      
      this.momMasterservice.addEditMeetingRoom(objMeetingRoom)
        .subscribe(data => {
          
          this.toastr.SaveMessage();
          this.loadMeetingRoom();
          this.spinner.hide();
        },
          (error) => {
            //Called when error
            this.spinner.hide();
          }
        ).add(() => {
          //Called when operation is complete (both success and error)
          this.spinner.hide();
        });
      return true;
    }
  }

  DuplicateNameCheck(meetingRoomCode: string, meetingRoomUno: number) {
    if (this.data != null && this.data != undefined) {
      var rtnValue = false;
      this.data.forEach((grdData) => {

        if (grdData.meetingRoomCode.toLocaleLowerCase() == meetingRoomCode.toLocaleLowerCase() && meetingRoomUno != grdData.meetingRoomUno) {
          rtnValue = true;
        }
      });

      return rtnValue;
    }
  }
  DeleteMeetingRoom(data1: any) {

    let objMeetingRoom = new MeetingRoom();

    objMeetingRoom.meetingRoomUno = data1[0].meetingRoomUno;
    objMeetingRoom.userUno = this.login_UserID;
    //objDeaprtment.tenantUno = 1;
    
    this.momMasterservice.deleteMeetingRoom(objMeetingRoom)
      .subscribe(data => {
        
        this.toastr.DeleteMessage();
        this.loadMeetingRoom();


        //console.log(this.postData);
        this.spinner.hide();
      },
        (error) => {
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }


  public focusIn(target: HTMLElement): void {
    target.parentElement.classList.add('e-input-focus');
  }

  public focusOut(target: HTMLElement): void {
    target.parentElement.classList.remove('e-input-focus');
  }
  toolbarClick(args: ClickEventArgs): void {
    
    if (args.item.id === 'grdData_excelexport') {
      const excelExportProperties: ExcelExportProperties = {
        includeHiddenColumn: true,
        fileName: 'meetingRoom_' + new Date().toLocaleDateString() + '.xlsx'
      };
      this.grid.excelExport(excelExportProperties);
    }
  }
  get meetingRoomUno(): AbstractControl { return this.orderForm.get('meetingRoomUno'); }
  get meetingRoomCode(): AbstractControl { return this.orderForm.get('meetingRoomCode'); }
  get meetingRoomName(): AbstractControl { return this.orderForm.get('meetingRoomName'); }
  get locationUno(): AbstractControl { return this.orderForm.get('locationUno'); }

}
