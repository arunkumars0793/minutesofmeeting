import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { alert, momDashboard, momDashboard_MyMeetings } from '../../models/MOMForm';
import { NgxSpinnerService } from "ngx-spinner";
import { MasterMOMService } from '../../services/master-mom.service';
import { Router } from '@angular/router';
import { EncryptDecrypt } from './../../models/common';
import { IAccLoadedEventArgs, AccumulationTheme } from '@syncfusion/ej2-angular-charts';
import { environment } from '../../../environments/environment';
import { defaultOptions } from 'ngx-extended-pdf-viewer';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
@Component({
  selector: 'help',
  templateUrl: './help.component.html',
  styleUrls: ['./../header/header.component.css']
})
export class HelpComponent implements OnInit {
  public data: Object[];
  public alertStartIndex: number = 1;
  login_User: '';
  safePDFURL: SafeUrl;
  PDFURL: any = environment.apiBase + '/assets/help/MOM_UserManual_TBT_Admin.pdf';
  login_UserID: 0;
  alertList: any = [];
  disableMore: boolean= false;
  isTBT = false;
  Role = 0;
  IsIE = /msie\s|trident\//i.test(window.navigator.userAgent);
  public objmomDashboard: momDashboard = new momDashboard();
  public lstMOMDashboard_TodayMeetings: momDashboard_MyMeetings[] = [];
  constructor(private userService: UserService, private _router: Router, public momMasterservice: MasterMOMService, private spinner: NgxSpinnerService, private sanitizer: DomSanitizer) {
    defaultOptions.workerSrc = environment.apiBase + '/assets/pdf.worker-es5.js';
  }

  ngOnInit() {
    var user = this.userService.getUserInfo();
    this.login_User = user.username;
    this.isTBT = user.isTBT == 'false' ? false : true;
    this.Role = Number(user.role);
    if (this.isTBT && this.Role == 1) {
      this.PDFURL = environment.apiBase + '/assets/help/MOM_UserManual_TBT_Admin.pdf';
    }
    else if (this.isTBT && this.Role == 0) {
      this.PDFURL = environment.apiBase + '/assets/help/MOM_UserManual_TBT_NoAdmin.pdf';
    }
    else if (!this.isTBT && this.Role == 1) {
      this.PDFURL = environment.apiBase + '/assets/help/MOM_UserManual_NoTBT_Admin.pdf';
    }
    else if (!this.isTBT && this.Role == 0) {
      this.PDFURL = environment.apiBase + '/assets/help/MOM_UserManual_NoTBT_NoAdmin.pdf';
    }
    this.safePDFURL= this.sanitizer.bypassSecurityTrustResourceUrl(this.PDFURL);
    this.login_UserID = user.id;
    this.loadAlerts();
    this.loadGeneralInfo();
  }

  loadGeneralInfo() {
    
    let inpValue = new momDashboard();
    inpValue.userUno = this.login_UserID;
    this.momMasterservice.getMOMDashboard(inpValue)
      .subscribe((data: any) => {
        this.objmomDashboard = data;
        this.lstMOMDashboard_TodayMeetings.push(data.lstMOMDashboard_MyMeetings[0]);
        this.data = [
          { 'x': 'Draft', y: this.objmomDashboard.noOfDraftMeeting, text: 'Draft: ' + this.objmomDashboard.noOfDraftMeeting + '%', r: '100' },
          { 'x': 'Not Published', y: this.objmomDashboard.noOfNotPublishMeeting, text: 'Not Published: ' + this.objmomDashboard.noOfNotPublishMeeting + '%', r: '100' },
          { 'x': 'Published', y: this.objmomDashboard.noOfPublishedMeeting, text: 'Published: ' + this.objmomDashboard.noOfPublishedMeeting + '%', r: '100' }
        ]
        this.spinner.hide();
      },
        (error) => {
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }

  loadValues(obj) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/view-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(obj.genInfoUno), QMType: objEncryptDecrypt.EncryptText(obj.type) } });
  }
  loadAlerts() {
    

    this.spinner.show();
    let inpValue = new alert();
    inpValue.userUno = this.login_UserID;
    this.alertStartIndex = (this.alertStartIndex == undefined || this.alertStartIndex < 1) ? 1 : this.alertStartIndex;
    inpValue.startIndex = this.alertStartIndex;
    inpValue.condition = 1;
  
    inpValue.maxCount = 10;
    this.momMasterservice.getAlertList(inpValue)
      .subscribe((data: any) => {
        if (this.alertStartIndex == 1) {
          this.alertList = data;
        }
        else {
          for (var i = 0; i < data.length; i++) {
            this.alertList.push(data[i]);
          }
        }


        this.spinner.hide();
        this.disableMore = (data.length >= 10) ? false : true;
        if (data.length > 0) {
          //this.ifcountgreaterthanzero = true;
          this.alertStartIndex = this.alertStartIndex + 1;
        }
       
        
      },
        () => {
          //Called when error
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }

  //Initializing Legend
  public legendSettings: Object = {
    visible: false
  };
  public dataLabel: Object = {
    visible: true, position: 'Outside',
    connectorStyle: { length: '10%' }, name: 'text',
  };
  public startAngle: number = 0;
  public endAngle: number = 360;
  //Initializing Tooltip
  public tooltip: Object = {
    enable: true, format: '${point.x} : <b>${point.y}%</b>'
  };
  public title: string = 'RIO Olympics Gold ';
  // custom code start
  public onLoad(args: IAccLoadedEventArgs): void {
    let selectedTheme: string = location.hash.split('/')[1];
    selectedTheme = selectedTheme ? selectedTheme : 'Material';
    args.accumulation.theme = <AccumulationTheme>(selectedTheme.charAt(0).toUpperCase() + selectedTheme.slice(1)).replace(/-dark/i, "Dark");
  }

  loadMeetingList(val = 0) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/meeting-list'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(0) } });
  }

  loadViewMeeting(id: any) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/view-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QMType: objEncryptDecrypt.EncryptText('MEETING') } });
  }

  loadUpdateMeeting(id: any) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/add-update-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QType: objEncryptDecrypt.EncryptText('MEETING') } });
  }
}
