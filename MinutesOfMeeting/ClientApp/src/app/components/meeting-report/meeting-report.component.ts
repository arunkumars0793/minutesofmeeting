import { Component, OnInit, Input, ViewChild } from '@angular/core';

import { UtilityService } from '../../services/utility.service';
import { UserService } from '../../services/user.service';
import { MasterMOMService } from '../../services/master-mom.service';
import { SpinnerVisibilityService } from 'ng-http-loader';
import { momDashboard } from '../../models/MOMForm';
import { EncryptDecrypt } from './../../models/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { IFilter, FilterSettingsModel, GroupSettingsModel, GridComponent } from '@syncfusion/ej2-angular-grids';

@Component({
  selector: 'meeting-report',
  templateUrl: './meeting-report.component.html',
  styleUrls: ['./meeting-report.component.css']
})
export class MeetingReportComponent implements OnInit {
  user: any = {};
  login_UserID: number;
  public toolbar: string[];
  public pageSettings: Object;
  public filterOptions: FilterSettingsModel;
  public filter: IFilter;
  checkBoxVal: number = 1;
  public data: any = [];
  isTBT = false;
  actionType = 'Meeting';
  public searchText: any;
  searchForm: FormGroup;
  submitted = false;
  public groupOptions: GroupSettingsModel;
  value: any;

  @ViewChild('divisionGrid', { static: false }) public divisionGrid: GridComponent;
  @ViewChild('activeUserGrid', { static: false }) public activeUserGrid: GridComponent;
  constructor(public momMasterservice: MasterMOMService, public utilityService: UtilityService, private _router: Router, private spinner: SpinnerVisibilityService, private _activatedRoute: ActivatedRoute,
    private userService: UserService, private formBuilder: FormBuilder, ) {

  }

  ngOnInit() {
    this.groupOptions = { showDropArea: false, columns: ['divisionName'] };
    this.value = [null, null];
    this.pageSettings = { pageSize: 50, pageSizes: [10, 50, 100, 500, 1000] };
    this.filterOptions = {
      type: 'Excel',

    };

    this.filter = {
      type: 'CheckBox'
    };
    this.user = this.userService.getUserInfo();
    this.login_UserID = this.user.id;
    this.isTBT = this.user.isTBT == 'false' ? false : true;
    this.getSearchList();

  }
  get t() { return this.searchForm.controls; }

  search() {
      this.getSearchList();
  }

  ExportGrid() {
    if (this.actionType == 'Active Users') {
      this.activeUserGrid.excelExport();
    }
    else {
      this.divisionGrid.excelExport();
    }
  }

  createFormGroup(QText): FormGroup {
    return new FormGroup({
      searchText: new FormControl(QText, []),
    });
  }

  getSearchList() {

    let inpValue = new momDashboard();
    
    inpValue.userUno = this.login_UserID;
    inpValue.actionType = this.checkBoxVal == 2 ? 'ATTENDANCE' : this.checkBoxVal == 3 ? 'TBD' : 'MEETING';
    if (this.checkBoxVal == 4)
     inpValue.actionType  = 'Active'
    inpValue.searchText = this.searchText;
    inpValue.startdt = this.value[0];
    inpValue.enddt = this.value[1];
    this.momMasterservice.getDivMeetingList(inpValue)
      .subscribe((data: any) => {
        this.data = data; 
      },
        (error) => {
          this.spinner.hide();
        }
      ).add(() => {
        //Called when operation is complete (both success and error)
        this.spinner.hide();
      });

  }


  loadUpdateMeeting(id: any) {
    var objEncryptDecrypt = new EncryptDecrypt();
    if (this.checkBoxVal == 2)
      this._router.navigate(['/add-update-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QType: objEncryptDecrypt.EncryptText('ATTENDANCE') } });
    else if (this.checkBoxVal == 3)
      this._router.navigate(['/add-update-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QType: objEncryptDecrypt.EncryptText('TBD') } });
    else
      this._router.navigate(['/add-update-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QType: objEncryptDecrypt.EncryptText('MEETING') } });
  }

  loadAddMeeting() {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/add-update-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(0), QType: objEncryptDecrypt.EncryptText('MEETING') } });
  }

  loadViewMeeting(id: any) {
    var objEncryptDecrypt = new EncryptDecrypt();
    if (this.checkBoxVal == 2)
      this._router.navigate(['/view-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QMType: objEncryptDecrypt.EncryptText('ATTENDANCE') } });
    else if (this.checkBoxVal == 3)
      this._router.navigate(['/view-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QMType: objEncryptDecrypt.EncryptText('TBD') } });
    else
      this._router.navigate(['/view-page'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(id), QMType: objEncryptDecrypt.EncryptText('MEETING') } });

   }

  loadAttList(val = 0) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/attendance-list'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(val) } });
  }

  loadMeetingList(val = 0) {
    if (this.checkBoxVal == 2)
      this.loadAttList();
    else if (this.checkBoxVal == 3)
      this.loadTBTList();
    else {
      var objEncryptDecrypt = new EncryptDecrypt();
      this._router.navigate(['/meeting-list'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(val) } });
    }
  }

  loadTBTList(val = 0) {
    var objEncryptDecrypt = new EncryptDecrypt();
    this._router.navigate(['/tbd-list'], { queryParams: { QMId: objEncryptDecrypt.EncryptText(val) } });
  }


  TypeCheckBoxSelection(val) {
    this.checkBoxVal = val;
    this.actionType = this.checkBoxVal == 2 ? 'Attendance' : this.checkBoxVal == 3 ? 'TBT' : 'Meeting';
    if (this.checkBoxVal == 4)
      this.actionType = 'Active Users';
    this.getSearchList();
  }



}

