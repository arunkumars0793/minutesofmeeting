import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { TimeFormatPipe } from './models/time-format.pipe';
import { TimeAgoPipe } from './models/time-ago.pipe';
import { RadioButtonModule } from '@syncfusion/ej2-angular-buttons';
import { GridModule, PageService, SortService, FilterService, ToolbarService, EditService, GroupService, ExcelExportService, CommandColumnService } from '@syncfusion/ej2-angular-grids';
import { DropDownListModule, MultiSelectModule, AutoCompleteModule,ComboBoxModule } from '@syncfusion/ej2-angular-dropdowns';
import { UserIdleModule } from 'angular-user-idle';
import { NgxCoolDialogsModule } from 'ngx-cool-dialogs';
import { ToastrModule } from 'ngx-toastr';
import { DatePickerModule, TimePickerModule, DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';
import { DialogModule,TooltipAllModule } from '@syncfusion/ej2-angular-popups';
import { UploaderModule } from '@syncfusion/ej2-angular-inputs';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { MeetingTypeComponent } from './components/meeting-type/meeting-type.component';
import { LocationComponent } from './components/location/location.component';
import { MeetingroomComponent } from './components/meetingroom/meetingroom.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { AddUpdatePageComponent } from "./components/add-update-page/add-update-page.component"
import { MeetingListComponent } from './components/meeting-list/meeting-list.component';
import { AttendanceListComponent } from './components/attendance-list/attendance-list.component';
import { PrintViewPageComponent } from './components/print-view-page/print-view-page.component';
import { TBDListComponent } from './components/tbd-list/tbd-list.component';
import { UsermasterComponent } from './components/usermaster/usermaster.component';
import { UserprofileComponent } from './components/userprofile/userprofile.component';
import { SearchComponent } from './components/search/search.component';
import { AlertComponent } from './components/alert/alert.component';
import { HelpComponent } from './components/help/help.component';
import { AccordionModule } from '@syncfusion/ej2-angular-navigations';
//import { ChartAllModule, AccumulationChartAllModule, RangeNavigatorAllModule } from '@syncfusion/ej2-angular-charts';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { MeetingReportComponent } from './components/meeting-report/meeting-report.component';

//import { MeetingCalenderComponent } from './components/meeting-calender/meeting-calender.component';


const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'meeting-type', component: MeetingTypeComponent },
  { path: 'location', component: LocationComponent },
  { path: 'meetingroom', component: MeetingroomComponent },
  { path: 'add-update-page', component: AddUpdatePageComponent },
  { path: 'meeting-list', component: MeetingListComponent },
  { path: 'attendance-list', component: AttendanceListComponent },
  { path: 'tbd-list', component: TBDListComponent },
  { path: 'view-page', component: PrintViewPageComponent },
  { path: 'usermaster', component: UsermasterComponent },
  { path: 'userprofile', component: UserprofileComponent },
  { path: 'search', component: SearchComponent },
  { path: 'alert', component: AlertComponent },
  { path: 'help', component: HelpComponent },
  { path: 'report', component: MeetingReportComponent },
  //{ path: 'meeting-calender', component: MeetingCalenderComponent},
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
];



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    MeetingTypeComponent,
    LocationComponent,
    MeetingroomComponent,
    HeaderComponent,
    FooterComponent,
    AddUpdatePageComponent,
    AlertComponent,
    MeetingListComponent,
    MeetingReportComponent,
    AttendanceListComponent,
    TimeFormatPipe,
    TimeAgoPipe,
    PrintViewPageComponent,
    UsermasterComponent,
    TBDListComponent,
    UserprofileComponent,
    SearchComponent,
    HelpComponent,
    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  
    BrowserAnimationsModule,
   
    RadioButtonModule,
    MultiSelectModule,
    GridModule,
    DropDownListModule,
    NgxSpinnerModule,
    TimePickerModule,
  
    ToastrModule.forRoot(), // ToastrModule added
 
    DatePickerModule,
    DateRangePickerModule,
    DialogModule,
    UploaderModule,
    UserIdleModule.forRoot({ idle: 1175, timeout: 25, ping: 100 }),
 

    FormsModule,
    ReactiveFormsModule,
    AutoCompleteModule,
    AccordionModule,
    TooltipAllModule,
    //ChartAllModule,
    //AccumulationChartAllModule,
    //RangeNavigatorAllModule,
    NgxExtendedPdfViewerModule,
  
    ComboBoxModule,
    NgxCoolDialogsModule.forRoot({
      theme: 'material',
      okButtonText: 'Yes',
      cancelButtonText: 'No',
      color: '#8030c3',
      titles: {
        alert: 'Alert',
        confirm: 'Confirmation',
        prompt: 'Website asks...'
      }
    }),

    AppRoutingModule,
    NgHttpLoaderModule.forRoot(),
    RouterModule.forRoot(appRoutes, { useHash: true }),
    //PdfmakeModule
  ],



  providers: [PageService, SortService, ToolbarService, FilterService, EditService, GroupService, ExcelExportService, CommandColumnService],
  bootstrap: [AppComponent]
})
export class AppModule { }
