﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MinutesOfMeeting.Models.BAL;
using MinutesOfMeeting.Models.Helpers;
using System.Net.Http.Headers;
using static MinutesOfMeeting.Models.Helpers.Admin;

namespace MinutesOfMeeting.Controllers
{

    [Route("api/MasterMOM")]
   
    public class MasterMOMController : Controller
    {
        BusinessLayer ObjBusiness = new BusinessLayer();
        private IWebHostEnvironment _env;
        public MasterMOMController(IWebHostEnvironment env)
        {
            _env = env;
            ObjBusiness = new BusinessLayer(_env);
            ObjBusiness._env = _env;
        }

        #region Meeting Type
        /// <summary>
        /// Insert/Update the MeetingType details
        /// </summary>
        /// <param name="obj">Input MeetingType object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertUpdateMeetingType")]
        public string InsertUpdateMeetingType([FromBody] MeetingType obj) => ObjBusiness.InsertUpdateMeetingType(obj);

        /// <summary>
        /// Delete the MeetingType details
        /// </summary>
        /// <param name="obj">Input MeetingType object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteMeetingType")]
        public string MeetingTypeDelete([FromBody] MeetingType obj) => ObjBusiness.DeleteMeetingType(obj);

        /// <summary>
        /// Get the MeetingType details
        /// </summary>
        /// <param name="obj">Input MeetingType object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetMeetingType")]
        public IEnumerable<MeetingType> GetMeetingType([FromBody] MeetingType obj)
        {
            List<MeetingType> List_MeetingType = ObjBusiness.GetMeetingType(obj);
            return List_MeetingType;

        }

        #endregion

        #region Location
        /// <summary>
        /// Insert/Update the Location details
        /// </summary>
        /// <param name="obj">Input Location object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertUpdateLocation")]
        public string InsertUpdateLocation([FromBody] MeetingLocation obj) => ObjBusiness.InsertUpdateLocation(obj);

        /// <summary>
        /// Delete the Location details
        /// </summary>
        /// <param name="obj">Input Location object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteLocation")]
        public string DeleteLocation([FromBody] MeetingLocation obj) => ObjBusiness.DeleteLocation(obj);

        /// <summary>
        /// Get the Location details
        /// </summary>
        /// <param name="obj">Input Loication object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetLocation")]
        public IEnumerable<MeetingLocation> GetLocation([FromBody] MeetingLocation obj)
        {
            List<MeetingLocation> List_Location = ObjBusiness.GetLocation(obj);
            return List_Location;

        }

        #endregion

        #region Meeting Room
        /// <summary>
        /// Insert/Update the MeetingRoom details
        /// </summary>
        /// <param name="obj">Input MeetingRoom object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertUpdateMeetingRoom")]
        public string InsertUpdateMeetingRoom([FromBody] MeetingRoom obj) => ObjBusiness.InsertUpdateMeetingRoom(obj);

        /// <summary>
        /// Delete the MeetingRoom details
        /// </summary>
        /// <param name="obj">Input MeetingRoom object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteMeetingRoom")]
        public string DeleteMeetingRoom([FromBody] MeetingRoom obj) => ObjBusiness.DeleteMeetingRoom(obj);

        /// <summary>
        /// Get the List_MeetingRoom details
        /// </summary>
        /// <param name="obj">Input List_MeetingRoom object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetMeetingRoom")]
        public IEnumerable<MeetingRoom> GetMeetingRoom([FromBody] MeetingRoom obj)
        {
            List<MeetingRoom> List_MeetingRoom = ObjBusiness.GetMeetingRoom(obj);
            return List_MeetingRoom;

        }


        #endregion

        #region User Master

        [HttpPost]
        [Route("GetUserMasters")]
        public IEnumerable<Admin.UserMaster> GetUserMasters([FromBody] UserMaster obj)
        {
            List<UserMaster> List_UserMaster = ObjBusiness.GetUserMasters(obj);
            return List_UserMaster;

        }

        [HttpPost]
        [Route("getUserList")]
        public IEnumerable<AttendeesAtd> getUserList([FromBody] AttendeesAtd obj)
        {
            UserMaster Usrobj = new UserMaster();
            Usrobj.ID = 0;
            Usrobj.Condition = 7;
            Usrobj.LanguageUno = 0;
            List<AttendeesAtd> List_selUser = ObjBusiness.getUserList(Usrobj);
            return List_selUser;

        }

        [HttpPost]
        [Route("InsertUpdateUserMaster")]
        public string InsertUpdateUserMaster([FromBody] UserMaster obj) => ObjBusiness.InsertUpdateUserMaster(obj);


        [HttpPost]
        [Route("DeleteUserMaster")]
        public string DeleteUserMaster([FromBody] UserMaster obj) => ObjBusiness.DeleteUserMaster(obj);
        #endregion

        #region Dashboard

        [HttpPost]
        [Route("GetMOMDashboard")]
        public MOMDashboard GetMOMDashboard([FromBody] MOMDashboard obj)
        {

            MOMDashboard cmtObj = ObjBusiness.GetMOMDashboard(obj);
            return cmtObj;

        }

        #endregion

        #region Search List && Report

        [HttpPost]
        [Route("GetSearchList")]
        public MOMDashboard getSearchList([FromBody] MOMDashboard obj)
        {

            MOMDashboard cmtObj = ObjBusiness.GetSearchList(obj);
            return cmtObj;

        }


        [HttpPost]
        [Route("GetDivMeetingList")]
        public IEnumerable<DivisionReport> GetDivMeetingList([FromBody] MOMDashboard obj) => ObjBusiness.GetDivMeetingList(obj);

        #endregion



        #region outlook

        [HttpPost]
        [Route("GetMeetings")]
        public List<OutlookMeeting> GetMeetings([FromBody] OutlookMeeting obj)
        {
            return ObjBusiness.GetMeetings(obj);

        }

        #endregion

        #region Genenral Info

        [HttpPost]
        [Route("InsertUpdateGeneralInfoAtd")]
        public string InsertUpdateGeneralInfoAtd([FromBody] GenInfoAtdInsert obj) {
            string AppPath = Request.Host.Value.ToString();
            return ObjBusiness.InsertUpdateGeneralInfo(obj, _env, AppPath);

        }

        [HttpPost]
        [Route("GetGeneralInfomation")]
        public IEnumerable<GenInfo> GetGeneralInfomation([FromBody] GenInfoAtdParam obj)
        {
            List<GenInfo> List_GenInfo = ObjBusiness.GetGeneralInfomation(obj);
            return List_GenInfo;

        }

        [HttpPost]
        [Route("DelGeneralInfoAtd")]
        public GenInfoAtdParam DelGeneralInfoAtd([FromBody] GenInfoAtdParam obj)
        {
            GenInfoAtdParam List_GenInfo = ObjBusiness.DelGeneralInfo(obj);
            return List_GenInfo;

        }


        [HttpPost]
        [Route("PublishGeneralInfo")]
        public GenInfoAtdParam PublishGeneralInfo([FromBody] GenInfoAtdParam obj)
        {
            string AppPath = Request.Host.Value.ToString();
            GenInfoAtdParam List_GenInfo = ObjBusiness.PublishGeneralInfo(obj, _env, AppPath);
            return List_GenInfo;

        }

        [HttpPost]
        [Route("CreateFollowupMeeting")]
        public GenInfoAtdParam CreateFollowupMeeting([FromBody] GenInfoAtdParam obj)
        {
            GenInfoAtdParam List_GenInfo = ObjBusiness.CreateFollowupMeeting(obj);
            return List_GenInfo;

        }

        #endregion

        #region Attendees
        /// <summary>
        /// Insert/Update the Attendees details
        /// </summary>
        /// <param name="obj">Input Attendees object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertUpdateAttendeesAtd")]
        public string InsertUpdateAttendees([FromBody] AttendeesAtd obj) => ObjBusiness.InsertUpdateAttendees(obj);


        /// <summary>
        /// Insert the Attendees from outlook
        /// </summary>
        /// <param name="obj">Input Attendees object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertOutlookAttendees")]
        public OutlookAttendeeInsert InsertOutlookAttendees([FromBody] OutlookAttendeeInsert obj)
        {
           
            return ObjBusiness.InsertOutlookAttendees(obj);
        }

        /// <summary>
        /// Delete the MeetingType details
        /// </summary>
        /// <param name="obj">Input MeetingType object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteAttendeesAtd")]
        public string DeleteAttendeesAtd([FromBody] AttendeesAtd obj) => ObjBusiness.DeleteAttendeesAtd(obj);

        /// <summary>
        /// Get the GeneralInfo details
        /// </summary>
        /// <param name="obj">Input GeneralInfo object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAttendeesAtd")]
        public IEnumerable<AttendeesAtd> GetAttendeesAtd([FromBody] AttendeesAtd obj)
        {
            List<AttendeesAtd> List_Attendees = ObjBusiness.GetAttendees(obj);
            return List_Attendees;

        }

        [HttpPost]
        [Route("AttendeColumnUpdate")]
        public string AttendeColumnUpdate([FromBody] AttendeesAtd obj)
        {
            return ObjBusiness.AttendeColumnUpdate(obj);
        }

     



        #endregion

        #region Agenda
        /// <summary>
        /// Insert/Update the AgendaDetails details
        /// </summary>
        /// <param name="obj">Input AgendaDetails object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertUpdateAgendaDetail")]
        public string InsertUpdateAgendaDetail([FromBody] AgendaDetails obj) => ObjBusiness.InsertUpdateAgendaDetail(obj);

        /// <summary>
        /// Delete the AgendaDetails details
        /// </summary>
        /// <param name="obj">Input AgendaDetails object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteAgendaDetails")]
        public string DeleteAgendaDetails([FromBody] AgendaDetails obj) => ObjBusiness.DeleteAgendaDetails(obj);

        /// <summary>
        /// Get the AgendaDetails details
        /// </summary>
        /// <param name="obj">Input AgendaDetails object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAgendaDetails")]
        public IEnumerable<AgendaDetails> GetAgendaDetails([FromBody] AgendaDetails obj)
        {
            List<AgendaDetails> List_Agenda = ObjBusiness.GetAgendaDetails(obj);
            return List_Agenda;

        }

        #endregion

        #region Comment

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DelAtdComment")]
        public string DelAtdComment([FromBody] cmtObject obj)
        {
            return ObjBusiness.DelAtdComment(obj);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAtdComment")]
        public IEnumerable<cmtObject> GetAtdComment([FromBody] cmtObject obj)
        {
            List<cmtObject> cmtObjectList = ObjBusiness.GetAtdComment(obj);
            return cmtObjectList;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertAtdComment")]
        public string InsertAtdComment([FromBody] cmtObject obj)
        {

            return ObjBusiness.InsertAtdComment(obj);


        }

        #endregion

        #region Attachement
        /// <summary>
        /// Video Upload in File path
        /// </summary>
        /// <returns>Path value</returns>
        [HttpPost, DisableRequestSizeLimit]
        [Route("UploadFiles")]
        public IActionResult UploadFiles()
        {
            try
            {
                ErrorLog.DataLogFileCreation(_env, "File Upload started");
                var id = Request.Form["id"].ToString();
                foreach (var formFile in Request.Form.Files)
                {
                    var i = 0;
                    i++;
                    var param = id.Split("-");
                    var file = formFile;

                    if (file.Length > 0)
                    {
                        byte[] fileContent;
                        using (var stream = formFile.OpenReadStream())
                        {
                            using (var reader = new BinaryReader(stream))
                            {
                                var FileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                                fileContent = reader.ReadBytes((int)stream.Length);
                                string ext = Path.GetExtension(FileName);
                                string contentType = CommonClass.GetFileContentType(ext);
                                if (contentType != String.Empty)
                                {
                                    var name = FileName.Split(".");
                                    var filename = FileName;
                                    if (name.Length > 1)
                                        filename = "Attachment - " + i + "." + name[1];
                                    ObjBusiness.fileUpload(Convert.ToInt32(param[0]), Convert.ToInt32(param[1]), Convert.ToString(param[2]), filename, contentType, fileContent);
                                }
                            }
                        }
                      
                    }
                }


                return Ok("sucess");
            }



            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
                return StatusCode(500, ex.Message);
            }
        }
        [HttpPost, DisableRequestSizeLimit]
        [Route("DownloadAttachFile")]
        public FileResult DownloadAttachFile([FromBody] FileAttachement obj)
        { //int id;
            byte[] fileContent;
            string fileName, contentType;
            string result = string.Empty;
            DataTable dt = ObjBusiness.DownloadAttachFile(obj); 
            fileName = Convert.ToString(dt.Rows[0]["FileName"]);
            contentType = Convert.ToString(dt.Rows[0]["ContentType"]);
            fileContent = (byte[])(dt.Rows[0]["FileContent"]);
            return File(fileContent, contentType, fileName);
        }

        /// <summary>
        /// Video Upload in File path
        /// </summary>
        /// <returns>Path value</returns>
        [HttpPost, DisableRequestSizeLimit]
        [Route("DeleteAttachedFile")]
        public IActionResult DeleteAttachedFile([FromBody] FileAttachement obj)
        {
            try
            {
                ObjBusiness.DeleteAttachedFile(obj);
                return Ok(new { result = true });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        [HttpPost]
        [Route("GetFileList")]
        public IEnumerable<FileAttachement> GetFileList([FromBody] FileAttachement obj)
        {
            List<FileAttachement> ListUpload = ObjBusiness.GetFileList(obj);
            return ListUpload;
        }
        #endregion attachement

        #region Division


        /// <summary>
        /// Get the Division details
        /// </summary>
        /// <param name="obj">Input Division object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetDivision")]
        public IEnumerable<Division> GetDivision([FromBody] Division obj)
        {
            List<Division> List_Division = ObjBusiness.GetDivision(obj);
            return List_Division;
        }
        #endregion

        #region Alert


        /// <summary>
        /// Get the Alert details
        /// </summary>
        /// <param name="obj">Input Alert object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAlertList")]
        public IEnumerable<Alert> GetAlertList([FromBody] Alert obj) => ObjBusiness.GetAlertList(obj);


        /// <summary>
        /// Insert/Update the Alert 
        /// </summary>
        /// <param name="obj">Input Alert object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("MarkAsVisited")]
        public string MarkAsVisited([FromBody] Alert obj) => ObjBusiness.MarkAsVisited(obj);


        #endregion












    }
}