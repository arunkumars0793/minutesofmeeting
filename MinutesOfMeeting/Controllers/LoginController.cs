﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MinutesOfMeeting.Models.BAL;
using MinutesOfMeeting.Models.Helpers;

//using System.DirectoryServices;


namespace MinutesOfMeeting.Controllers
{
  
    [Route("api/Login")]
    public class LoginController : Controller
    {
        BusinessLayer ObjBusiness;
        CommonClass ObjCommonClass = new CommonClass();

        private IWebHostEnvironment _env;
        /// <summary>
        /// Constructor to manage the host environment
        /// </summary>
        /// <param name="env"></param>
        public LoginController(IWebHostEnvironment env)
        {
            _env = env;
            ObjBusiness = new BusinessLayer(_env);
            ObjBusiness._env = _env;
        }

        public IActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Manage the erro
        /// </summary>
        /// <returns></returns>
        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }

        #region Authenticate
        /// <summary>
        /// Aunthedicate to Active directory
        /// </summary>
        /// <param name="objUsers">Input username and password details</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("AuthenticateUser")]
        public Users AuthenticateUser([FromBody] Users objUsers)
        {
            Users objrtnUsers = new Users();
            try
            {

            
            ErrorLog.LogFileCreation(_env, new Exception(objUsers.UserName +  " loggedin"));
            //To validate the user in AD and generate JWT token for the API and user validation
           
            string strUser = CommonClass.DecryptStringAES(objUsers.UserName);
            string strPassword = CommonClass.DecryptStringAES(objUsers.Password);

            ErrorLog.LogFileCreation(_env, new Exception(strUser + " loggedin"));

            string whitelist = @"^[a-zA-Z\-\.']$";
            //string manager = strUser.Trim();
            Regex pattern = new Regex(whitelist);
            if (!pattern.IsMatch(strUser.Trim()))
            {
                string rtnresult = string.Empty;
                string domain = ObjCommonClass.GetADDomain();//"salzertech";
                string domainAndUsername = CommonClass.EscapeForDN(domain + @"\" + strUser);
                string LDAPPATH = ObjCommonClass.GetADPath(); //"LDAP://salzertech.com/CN=Users,DC=salzertech,DC=com";

                    ErrorLog.LogFileCreation(_env, new Exception(LDAPPATH + " " +domain + " Domain information"));


                    try
                    {
                    using (DirectoryEntry entry = new DirectoryEntry(LDAPPATH, domainAndUsername, strPassword))
                    {
                        // Bind to the native AdsObject to force authentication.
                        Object obj = entry.NativeObject;
                        using (DirectorySearcher search = new DirectorySearcher(entry))
                        {

                            search.Filter = "(SAMAccountName=" + CommonClass.EscapeForSearchFilter(strUser) + ")";
                            search.PropertiesToLoad.Add("cn");
                            search.PropertiesToLoad.Add("title");
                            search.PropertiesToLoad.Add("displayName");
                            search.PropertiesToLoad.Add("sAMAccountName");
                            search.PropertiesToLoad.Add("userprincipalname");

                            SearchResult result = search.FindOne();
                            if (null != result)
                            {
                                string acName = (result.Properties["sAMAccountName"] != null && result.Properties["sAMAccountName"].Count > 0) ? result.Properties["sAMAccountName"][0].ToString() : string.Empty;
                                objrtnUsers.UserName = acName;
                                if (!string.IsNullOrEmpty(objrtnUsers.UserName))
                                    objrtnUsers.DisplayName = (result.Properties["displayName"] != null && result.Properties["displayName"].Count > 0) ? result.Properties["displayName"][0].ToString() : string.Empty;
                                objrtnUsers.Designation = (result.Properties["title"] != null && result.Properties["title"].Count > 0) ? result.Properties["title"][0].ToString() : string.Empty;
                                objrtnUsers.EmailId = (result.Properties["userprincipalname"] != null && result.Properties["userprincipalname"].Count > 0) ? result.Properties["userprincipalname"][0].ToString() : string.Empty;
                                objrtnUsers.Token =  ObjCommonClass.GetJWTSecurityToken(acName);
                            }
                            else
                            { objrtnUsers.Error = "invalid user"; }
                            // Update the new path to the user in the directory
                            LDAPPATH = result.Path;
                        }
                    }
                    objrtnUsers.Error = "";
                

                }
                catch (Exception ex)
                {
                    objrtnUsers.Error = "invalid user";
                    ErrorLog.LogFileCreation(_env, ex);
                    //throw new Exception("Error authenticating user." + ex.Message);
                }
            }
            else
            {
                objrtnUsers.Error = "invalid user";
            }
            }
            catch (Exception)
            {

                throw;
            }
            // objrtnUsers.UserName = "";
            //objrtnUsers.Token = "sdfdsf";
            // objrtnUsers.Error = "";
            return objrtnUsers;

            
        }

        /// <summary>
        /// Get the user information
        /// </summary>
        /// <param name="obj">Account Name</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetUserInfo_ByUserID")]
        public IEnumerable<Users> GetUserInfo_ByUserID([FromBody] Users obj)
        {
            List<Users> ListUserInfo_ByUserID = new List<Users>();
            try
            {
                string result = string.Empty;
                obj.UserName = CommonClass.DecryptStringAES(obj.UserName);
                Users objUser = ObjBusiness.GetUserInfo_ByUserID(obj);
                ListUserInfo_ByUserID.Add(ObjBusiness.GetUserInfo_ByUserID(obj));

            }
            catch (Exception ex)
            {
                ErrorLog.LogFileCreation(_env, ex);
            }
            return ListUserInfo_ByUserID;
        }

     
        #endregion
    }
}